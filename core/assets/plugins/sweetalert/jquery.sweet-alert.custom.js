
!function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples 
    SweetAlert.prototype.init = function() {
        
    //Basic
    $('#sa-basic').click(function(){
        swal("Here's a message!");
    });

    //A title with a text under
    $('#sa-title').click(function(){
        swal("Here's a message!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.")
    });

    //Success Message
    $('#sa-success').click(function(){
        swal("Good job!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.", "success")
    });

    //Warning Message
    $('#sa-warning').click(function(){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
        }, function(){   
            swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
        });
    });

    //Parameter
    $('#sa-params').click(function(){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", "Your imaginary file has been deleted.", "success");   
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
            } 
        });
    });


//Elimina Registro
    $("[name='sa-delete']").click(function(){
        swal({   
            title: "Esta Seguro Desea eliminar este registro?",   
            text: "recuerde no podra ser recuperado!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Borrar",   
            cancelButtonText: "No",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm.value) {     
                swal("Registro eliminado!", "su accion ha sido realizada", "success");   
            } else {     
                swal("Cancelado", "", "error");   
            } 
        });
    });


    //Custom Image
    $('#sa-image').click(function(){
        swal({   
            title: "Govinda!",   
            text: "Recently joined twitter",   
            imageUrl: "../assets/images/users/profile.png" 
        });
    });

    //Auto Close Timer
    $('#sa-close').click(function(){
         swal({   
            title: "Auto close alert!",   
            text: "I will close in 2 seconds.",   
            timer: 2000,   
            showConfirmButton: false 
        });
    });

    $(document).ready(function()
       {
         $('#desactivacion_exitosa').each(function() {
          Swal.fire({
            title: "¡HECHO!",
            text: "Se ha efectuado la desactivación con éxito.",
            type: "success"
          });
         })
       });

       $(document).ready(function()
       {
         $('#activacion_exitosa').each(function() {
          Swal.fire({
            title: "¡HECHO!",
            text: "Se ha efectuado la activación con éxito.",
            type: "success"
          });
         })
       });

       $(document).ready(function()
       {
         $('#registro_exitoso').each(function() {
          Swal.fire({
            title: "¡HECHO!",
            text: "Se ha efectuado el registro con éxito.",
            type: "success"
          });
         })
       });

       $(document).ready(function()
       {
         $('#login_error').each(function() {
          Swal.fire({
            title: "¡ERROR!",
            text: "Datos de autenticación erróneos, inténtelo nuevamente..",
            type: "error"
          });
         })
       });



    $(".delete-confirm").on("click", function(e) {
    var flag = $(this).parent("form");
    //alert(flag.attr('id'));
    e.preventDefault(); //to prevent submitting
    swal({
            title: "Esta Seguro Desea eliminar este registro?",   
            text: "recuerde no podra ser recuperado!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#cc3f44",
            confirmButtonText: "Borrar",
            closeOnConfirm: false,
            closeOnClickOutside: true,
            html: false
        }).then((willDelete) => {
            if (willDelete.value) {
              swal("Registro eliminado!", null, "success");
              flag.submit();   
            } else {     
              swal("Cancelado", "", "error");   
            }
        });
    });

    $(".sweet-confirm").on("click", function(e) {
        if($(this).attr("data-type")=="a"){
            var flag = $(this).attr("href");
        }
        if($(this).attr("data-type")=="form") {
            var flag = $('#'+$(this).attr('data-form'));
        }
        if($(this).attr("data-type")=="button") {
            var flag = $('#'+$(this).attr('data-target'));
        }
   // alert(flag);
    e.preventDefault(); //to prevent submitting
    swal({
            title: $(this).attr('data-title'),
            text: $(this).attr('data-text'),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#cc3f44",
            confirmButtonText: $(this).attr('data-btn'),
            closeOnConfirm: false,
            closeOnClickOutside: true,
            html: $(this).attr('data-html')
        }).then((willDelete) => {
            if (willDelete.value) {
              swal($(this).attr('data-confirm'), null, "success");
                if($(this).attr("data-type")=="form") {
                    flag.submit();
                }
                if($(this).attr("data-type")=="a") {
                    window.location.replace(flag);
                }
                if($(this).attr("data-type")=="button") {
                    $(flag).click();
                }
            } else {     
              swal("Cancelado", "", "error");   
            }
        });
    });



        $(".close-confirm").on("click", function(e) {
            var flag = $(this).attr("href");
            //alert(flag.attr('id'));
            e.preventDefault(); //to prevent submitting
            swal({
                title: $(this).attr('data-title'),
                text: $(this).attr('data-text'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#cc3f44",
                confirmButtonText: $(this).attr('data-btn'),
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnClickOutside: false,
                html: false
            }).then((willClose) => {
                if (willClose.value) {
                    swal($(this).attr('data-confirm'), null, "success");
                    window.location.replace(flag);
                } else {
                    swal("Cancelado", "", "error");
                }
            });
        });

 

       $(document).ready(function()
       {
         $('#cantidad_excedida').each(function() {
          Swal.fire({
            title: "¡ERROR!",
            text: "Se ha excedido de la cantidad del stock mínimo, por favor verificar los suministros.",
            type: "error"
          });
         })
       });

       $(document).ready(function()
       {
         $('#suministros_aprobados').each(function() {
          Swal.fire({
            title: "¡HECHO!",
            text: "Se han aprobado las solicitudes.",
            type: "success"
          });
         })
       });

       $(document).ready(function()
       {
         $('#suministros_rechazados').each(function() {
          Swal.fire({
            title: "¡HECHO!",
            text: "Se han rechazado las solicitudes.",
            type: "success"
          });
         })
       });

       $(document).ready(function()
       {
         $('#usuario_duplicado').each(function() {
          Swal.fire({
            title: "¡ERROR!",
            text: "El nombre de usuario ingresado ya se encuentra registrado.",
            type: "error"
          });
         })
       });

       $(document).ready(function()
       {
         $('#cedula_duplicada').each(function() {
          Swal.fire({
            title: "¡ERROR!",
            text: "La cédula ingresada ya se encuentra registrada.",
            type: "error"
          });
         })
       });

       $(document).ready(function()
       {
         $('#modificacion_exitosa').each(function() {
          Swal.fire({
            title: "¡HECHO!",
            text: "Se han realizado las modificaciones con éxito.",
            type: "success"
          });
         })
       });

       $(document).ready(function()
       {
         $('#sincambios_usuario').each(function() {
          Swal.fire({
            title: "¡ATENCIÓN!",
            text: "No se detectaron cambios, por lo tanto no se ha modificado.",
            type: "warning"
          });
         })
       });

       


    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.SweetAlert.init()
}(window.jQuery);