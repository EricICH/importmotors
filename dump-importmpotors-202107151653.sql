-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: importmpotors
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autos`
--

DROP TABLE IF EXISTS `autos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autos` (
  `id_auto` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `brand` int(11) NOT NULL,
  `model` int(11) NOT NULL,
  `plate` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `type` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `serial_motor` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `serial_chasy` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `transmission` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `color` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `year` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `km` int(11) DEFAULT NULL,
  `agency` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `obs` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `price` float DEFAULT NULL,
  `cap` float(2,0) DEFAULT NULL,
  `power` float DEFAULT NULL,
  `height` float DEFAULT NULL,
  `long` float DEFAULT NULL,
  `wide` float DEFAULT NULL,
  `autonomy` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `consumo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `condition` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pic` longtext COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_auto`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autos`
--

LOCK TABLES `autos` WRITE;
/*!40000 ALTER TABLE `autos` DISABLE KEYS */;
INSERT INTO `autos` VALUES (1,1,1,1,1,'0000','1','1','1','1','Blanco','2018',0,'0','0',33000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'Toyota_Camry.jpg'),(2,1,1,1,2,'0000','1','1','1','1','Blanco','2017',0,'0','0',29000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'Toyota_Priusblanco.jpg'),(3,1,1,1,2,'0000','1','1','1','1','Gris','2017',0,'0','0',27000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018_toyota_prius_prime-picjpeg.jpeg'),(4,1,1,1,3,'0000','1','1','1','1','Azul','2020',0,'0','0',33000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'toyota-corolla-2020-azul.jpg'),(5,1,1,1,3,'0000','1','1','1','1','Negro','2020',0,'0','0',36000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'negro_Corolla.png'),(6,1,1,2,4,'0000','1','1','1','1','Blanco','2015',0,'0','0',35000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'mustang_coyote.jpeg'),(7,1,1,2,5,'0000','1','1','1','1','Azul','2018',0,'0','0',18000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'mustang_750.jpeg'),(8,1,1,2,6,'0000','1','1','1','1','Blanco','2018',0,'0','0',29000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'ford_350.jpeg'),(9,1,1,3,7,'0000','1','1','1','1','rojo','2001',0,'0','0',98000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'ferrari_modena-360.jpeg'),(10,1,1,4,8,'0000','1','1','1','1','Blanco','2007',0,'0','0',29000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'porsche_boxter.jpeg'),(11,1,1,5,9,'0000','1','1','1','1','Blanco','2007',0,'0','0',30000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'Corvette_2007.jpeg'),(12,1,1,6,11,'0000','1','1','1','1','Blanco','2018',0,'0','0',35000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'hummer_h2_sub.jpeg'),(13,1,1,7,12,'0000','1','1','1','1','Blanco','2016',0,'0','0',26000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'subaru-wrx.jpg'),(14,1,1,8,13,'0000','1','1','1','1','Blanco','2020',0,'0','0',98000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'jeep_gladiador.jpeg'),(15,1,1,9,14,'0000','1','1','1','1','Blanco','2020',0,'0','0',18000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'Fiat-500-vorne2.jpg'),(16,1,1,5,10,'0000','1','1','1','1','Blanco','2007',0,'0','0',30000,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'camaro_ss8.jpeg'),(17,1,0,10,15,'12lkk12','Electrico',NULL,NULL,'Automatico','verde','2015',1200,NULL,'ddta  de prueba',NULL,5,NULL,NULL,NULL,NULL,'1000km','1.6','Nuevo',NULL);
/*!40000 ALTER TABLE `autos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id_brand` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Marca',
  `id_user` int(11) NOT NULL COMMENT 'ID Usuario',
  `brand` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Marca o Fabricante',
  PRIMARY KEY (`id_brand`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,1,'Toyota'),(2,1,'Ford'),(3,1,'Ferrari'),(4,1,'Porsche'),(5,1,'Chevrolet'),(6,1,'Hummer'),(7,1,'Subaru'),(8,1,'Jeep'),(9,1,'Fiat'),(10,1,'chery');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `models`
--

DROP TABLE IF EXISTS `models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `models` (
  `id_model` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Modelo',
  `id_brand` int(11) NOT NULL COMMENT 'ID Marca Relacionada',
  `model` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Modelo del Vehiculo',
  PRIMARY KEY (`id_model`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `models`
--

LOCK TABLES `models` WRITE;
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` VALUES (1,1,'Camry'),(2,1,'Prius'),(3,1,'Corolla'),(4,2,'Mustang Coyote'),(5,2,'Mustang'),(6,2,'350'),(7,3,'Modena 360'),(8,4,'Boxter'),(9,5,'Corvette'),(10,5,'Camarro SS8'),(11,6,'H2 SUB'),(12,7,'WRX Edicion Especial'),(13,8,'Gladiador'),(14,9,'500'),(15,10,'arauca');
/*!40000 ALTER TABLE `models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `status` int(1) NOT NULL COMMENT 'Estatus de Usuario',
  `date_in` date NOT NULL COMMENT 'Fecha de Registro',
  `name` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre y Apellido',
  `ci` varchar(20) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Cedula de Identidad',
  `cargo` varchar(30) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Cargo que ocupa',
  `email` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'Correo Electronico',
  `username` varchar(25) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Usuario',
  `id_parent` int(11) NOT NULL,
  `pass` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `picture` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC COMMENT='6';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'2021-07-11','Vendedor','12345678','Vendedor','venta@importmotors.com','ventas',0,'12345678','....');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'importmpotors'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-15 16:53:37
