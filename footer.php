      <!-- ============================================================== -->
      <!-- End Container fluid  -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- footer -->
      <!-- ============================================================== -->
      <footer class="footer">
        © 2010 - <?= date('Y')?> Prevenauto by Impormotor LLS
      </footer>
      <!-- ============================================================== -->
      <!-- End footer -->
      <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
  </div>
  <!-- ============================================================== -->
  <!-- End Wrapper -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->

  <!-- Bootstrap tether Core JavaScript -->
  <script src="<?= base_url('core/')?>assets/plugins/popper/popper.min.js"></script>
  <script src="<?= base_url('core/')?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
  <!-- slimscrollbar scrollbar JavaScript -->
  <script src="<?= base_url('core/material/')?>js/jquery.slimscroll.js"></script>
  <!--Wave Effects -->
  <script src="<?= base_url('core/material/')?>js/waves.js"></script>
  <!--Menu sidebar -->
  <script src="<?= base_url('core/material/')?>js/sidebarmenu.js"></script>
  <!--stickey kit -->
  <script src="<?= base_url('core/')?>assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
  <script src="<?= base_url('core/')?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
  <!--stickey kit -->
  <script src="<?= base_url('core/')?>assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
  <script src="<?= base_url('core/')?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
  <!--Custom JavaScript -->
  <script src="<?= base_url('core/material/')?>js/custom.js"></script>
  <!-- ============================================================== -->
  <!-- This page plugins -->
  <!-- ============================================================== -->
  <!-- chartist chart -->
    <script src="<?= base_url('core/')?>assets/plugins/echarts/echarts-init.js"></script>

  
  <script src="<?= base_url('core/')?>assets/plugins/chartist-js/dist/chartist.min.js"></script>
  <script src="<?= base_url('core/')?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>

  
    <!-- ============================================================-->


  <!-- Vector map JavaScript -->
  <script src="<?= base_url('core/')?>assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
  <script src="<?= base_url('core/')?>assets/plugins/vectormap/jquery-jvectormap-us-aea-en.js"></script>
  <script src="<?= base_url('core/material/')?>js/dashboard3.js"></script>
  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <script src="<?= base_url('core/')?>assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
  <!-- Style sweetalert -->
  <!-- ============================================================== -->
  <script src="<?= base_url('core/')?>assets/plugins/sweetalert/sweetalert.min.js"></script>
  <script src="<?= base_url('core/')?>assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>

  <!-- icheck -->
  <script src="<?= base_url('core/assets/plugins/')?>icheck/icheck.min.js"></script>
  <script src="<?= base_url('core/assets/plugins/')?>icheck/icheck.init.js"></script>

  <script src="<?= base_url('core/assets/plugins/')?>wizard/jquery.steps.min.js"></script>
  <script src="<?= base_url('core/assets/plugins/')?>wizard/jquery.validate.min.js"></script>
  <script src="<?= base_url('core/assets/plugins/')?>wizard/steps.js"></script>

  <script src="<?= base_url('core/assets/plugins/')?>datatables/datatables.min.js"></script>
  <script src="<?= base_url('core/assets/plugins/')?>datatables/dataTables.fixedHeader.min.js"></script>

  <script src="<?=base_url('core')?>/assets/plugins/dropify/dist/js/dropify.min.js"></script>

  <!-- Select2-->
  <script src="<?=base_url('core/')?>assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>

  <!-- start - This is for export functionality only -->
  <script src="<?= base_url('core/assets/plugins/')?>datatables-buttons/js/dataTables.buttons.min.js"></script>
  <script src="<?= base_url('core/assets/plugins/')?>datatables-buttons/js/buttons.flash.min.js"></script>
  <script src="<?= base_url('core/assets/plugins/')?>jszip/jszip.min.js"></script>
  <script src="<?= base_url('core/assets/plugins/')?>pdfmake/pdfmake.min.js"></script>
  <script src="<?= base_url('core/assets/plugins/')?>pdfmake/vfs_fonts.js"></script>
  <script src="<?= base_url('core/assets/plugins/')?>datatables-buttons/js/buttons.html5.min.js"></script>
  <script src="<?= base_url('core/assets/plugins/')?>datatables-buttons/js/buttons.print.min.js"></script>
  <script src="<?= base_url('core/material/')?>js/jquery.PrintArea.js" type="text/JavaScript"></script>
  <script src="<?= base_url('core/assets/plugins/')?>toast-master/js/jquery.toast.js"></script>
  <script>
  $(document).ready(function() {
    $("#print").click(function() {
      var mode = 'iframe'; //popup
      var close = mode == "popup";
      var options = {
          mode: mode,
          popClose: close
      };
      $(".noPrint").hide();
      $(".siPrint").show();
      $("div.printableArea").printArea(options);
      $(".noPrint").show();
      $(".siPrint").hide();
    });
  });

  $(document).ready(function() {
    $("#printarget").click(function() {
      var mode = 'iframe'; //popup
      var close = mode == "popup";
      var options = {
          mode: mode,
          popClose: close
      };
      $(".noPrint").hide();
      $(".siPrint").show();
      $($(this).attr('data-target')).printArea(options);
      $(".noPrint").show();
      $(".siPrint").hide();
    });
  });
  </script>
  <!-- end - This is for export functionality only -->
  <script>
    $(function() {
      $(".s2").select2();
      $(".l3").select2({
          maximumSelectionLength: 3
      });
      $('.dt2').DataTable({
        fixedHeader: {
            header: true,       
        },
        "scrollY": "300px",
        "ordering": true,
        "displayLength": 10,
        "scrollCollapse": true,
        "lengthChange": false,
        "paginate": false, 
        dom: 'Bfrtip',
        buttons: [
          {
            extend:'excel',
            text: '<i class="fa fa-file-excel"></i> Excel',
            className: 'btn btn-sm'
          },
          {
            extend:'pdf',
            text: '<i class="fa fa-file-pdf"></i> Pdf',
            className: 'btn btn-sm'
          },
          {
            extend:'print',
            text: '<i class="fa fa-print"></i>',
            className: 'btn btn-sm'
          }
        ]
      });

      $('.dt').DataTable({
        fixedHeader: {
            footer: true,
            header: true,       
        },
        "ordering": false,
        "scrollCollapse": true,
        "lengthChange": false,
        "paginate": false, 
        dom: 'Bfrtip',
        buttons: [
          {
            extend:'excel',
            text: '<i class="fa fa-file-excel"></i> Excel',
            className: 'btn btn-sm'
          },
          {
            extend:'pdf',
            text: '<i class="fa fa-file-pdf"></i> Pdf',
            className: 'btn btn-sm'
          },
          {
            extend:'print',
            text: '<i class="fa fa-print"></i>',
            className: 'btn btn-sm'
          }
        ]
      });

      $('.dt2s').DataTable({
        fixedHeader: {
         header: true,       
       },        
        "lengthChange": false,
        "ordering": false,
        "displayLength": 13,
        "searching": false
      });
      $('.dtS').DataTable({
        "lengthChange": false,
        "ordering": true,
        "displayLength": 8,
        "searching": false

      });
      $('.dtm').DataTable({
        fixedHeader: {
         header: true,       
       },

       "ordering": true,
       "displayLength": 10,
       "scrollCollapse": true,
       "lengthChange": false, 
       dom: 'Bfrtip',
       "order": [[ 0, "desc" ]],
       buttons: [
       {
         extend:'excel',
         text: '<i class="fa fa-file-excel"></i> Excel',
         className: 'btn btn-sm'
       },
       {
         extend:'pdf',
         text: '<i class="fa fa-file-pdf"></i> Pdf',
         className: 'btn btn-sm'
       },
       {
         extend:'print',
         text: '<i class="fa fa-print"></i>',
         className: 'btn btn-sm'
       }
       ]
     });
       $('.dtm2').DataTable({
         fixedHeader: {
          header: true,       
        },
        "scrollY": "200px",
        "ordering": false,
        "displayLength": 50,
        "scrollCollapse": true,
        "lengthChange": false
      });
        
        $('.dtm3').DataTable({
         fixedHeader: {
          header: true,       
        },
        "scrollY": "200px",
        "ordering": false,
        "scrollCollapse": true,
        "lengthChange": false,
        "paginate": false
      });
        

        $('.dtmS').DataTable({
        fixedHeader: {
         header: true,       
       },
       "ordering": false,
       "displayLength": 10,
       "scrollCollapse": true,
       "lengthChange": false,
       "paginate": false,
       "searching": false, 
       dom: 'Bfrtip',
       buttons: [
       {
         extend:'excel',
         text: '<i class="fa fa-file-excel"></i> Excel',
         className: 'btn btn-sm'
       },
       {
         extend:'pdf',
         text: '<i class="fa fa-file-pdf"></i> Pdf',
         className: 'btn btn-sm'
       },
       {
         extend:'print',
         text: '<i class="fa fa-print"></i>',
         className: 'btn btn-sm'
       }
       ]
     });
    });


    var table = $('.url');
    $('.url tbody').on('click', 'tr', function () {
      var url = $(this).attr('data'); 
      $(location).attr('href',url);
      //alert( 'You clicked on '+data[0]+'\'s row' );
    } );
    $('.modBtn').on('click',function(){
      modal=$(this).attr('data-target');
      $(modal).modal({show:true});
    });
    $(function () {
      $('[data-toggle="popover"]').popover();
      $('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="dropify"]').dropify();
      $('[data-toggle="datatable"]').DataTable({
         fixedHeader: {
          footer: true,
          header: true,       
        },
        "scrollY": "200px",
        "ordering": false,
        "displayLength": 50,
        "scrollCollapse": true,
        "lengthChange": false
      });
      load_not('#message');
        
    });
    
  </script>
</body>
</html>
<script>
  <?php 
      $m=$this->modulo->read(null,true,['group_by'=>'id_parent']);
      $campos=count($m);
      if($campos<=1): ?>
        $(document).ready(function() {
         // alert('<?=$campos?>');
        
          $('.sidebartoggler').click();
//          $('.sidebartoggler').click();
        });
      <?php endif; ?>
</script>