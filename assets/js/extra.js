var id_css = 1;

function send_form(form_sel="#form") {
  var attr = $(form_sel).attr('action');
  var after = $(form_sel).attr('after-send');
  $(form_sel).submit(function(e) {
    e.preventDefault();
    
    $.post(attr, $(this).serialize())
    .done(function() {
      swal({
        title:'Enviado!',
        text:'Por favor espere!',
        type:'success',
        timer:10000*10,
        showConfirmButton:false
      });
    })
    .fail(function() {
      alert('Error al procesar el formulario');
    })
    .always(function() {
      if (after==null) {  
        setTimeout("location.reload",10000);
      } else {
        setTimeout("location.href='"+after+"'",1500);
      }
    });
    e.stopImmediatePropagation();
  });
}

function send_form_fast(form_sel="#form") {
  var attr = $(form_sel).attr('action');
  //var after = $(form_sel).attr('after-send');
  $(form_sel).submit(function(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    $.post(attr, $(this).serialize())
    .always(function() {
    })
  });
}

function send_form_load(form_sel="form",result="#result",route) {

  var attr = $(form_sel).attr('action');
  $(form_sel).submit(function(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    $.post(attr, $(this).serialize())
    .done(function() {
    })
    .fail(function() {
      alert('Error intente nuevamente');
    })
    .always(function() {
      $(result).load(route, function(data, textStatus, xhr) {
       if (textStatus=='error') {
          alert('Error');
        }
        $(result).html(data);
      })
    })
  });
}

function send_form_load_multipart(form_sel="form",result="#result",route) {
  var attr = $(form_sel).attr('action');
  $(form_sel).submit(function(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    $.ajax({
      type: 'POST',
      enctype: 'multipart/form-data',
      url: attr,
      data: new FormData($(form_sel)[0]),
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000
    })
      .done(function() {
        Swal.fire({
          position: 'center',
          type: 'success',
          title: 'Formulario enviado correctamente',
          showConfirmButton: false,
          timer: 3000
        })
      })
      .fail(function() {
        Swal.fire({
          position: 'center',
          type: 'error',
          title: 'Error enviar el formulario, intente mas tarde',
          showConfirmButton: false,
          timer: 3000
        })
      })
      .always(function() {
      $(result).load(route, function(data, textStatus, xhr) {
       if (textStatus=='error') {
          alert('Error');
        }
        $(result).html(data);
      })
    });
  });
}

function send_form_multipart(form_sel="#form") {
  var attr = $(form_sel).attr('action');
  $(form_sel).submit(function(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    $.ajax({
      type: 'POST',
      enctype: 'multipart/form-data',
      url: attr,
      data: new FormData($(form_sel)[0]),
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000
    })
      .done(function() {
        Swal.fire({
          position: 'center',
          type: 'success',
          title: 'Formulario enviado correctamente',
          showConfirmButton: false,
          timer: 3000
        })
      })
      .fail(function() {
        Swal.fire({
          position: 'center',
          type: 'error',
          title: 'Error enviar el formulario, intente mas tarde',
          showConfirmButton: false,
          timer: 3000
        })
      })
      .always(function() {
        setTimeout("location.reload(true)",3000);
        $(form_sel)[0].reset();
      });
  });
}

function add_form(form_sel="#form",dest='#result',route) {
  //alert(form_sel);
 var attr = $(form_sel).attr('action');
  $(form_sel).submit(function(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    $.post(attr, $(this).serialize())
    .done(function() {
    })
    .fail(function() {
      alert('Error intente nuevamente');
    })
    .always(function() {
      $(dest).load(route, function(data, textStatus, xhr) {
       if (textStatus=='error') {
          alert('Error');
        }
        $(dest).html(data);
        $('form')[0].reset();
      })
    })
  });
  $(form_sel).submit();
  return false;
}


function button_submit(form_sel,btn_sel) {
  $(document).ready(function () {
    $(btn_sel).on('click', function() {
      $(form_sel).submit();
    });    
  });
}

function sweet_alert(type='success',title='Alerta',timer=1500) {
  Swal.fire({
    position: 'center',
    type: type,
    title: title,
    showConfirmButton: false,
    timer: timer
  }) 
}

function reps_fields() {

    room++;
    var objTo = document.getElementById('reps_fields')
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "form-row mb-0 removeclass" + room);
    divtest.innerHTML = '\
        <div class="col-sm-1 nopadding">\
        </div>\
        <div class="col-sm-2 p-1">\
          <div class="form-group">\
            <input type="text" class="form-control" id="codigo" name="codigo[]" value="" placeholder="Codigo">\
          </div>\
        </div>\
        <div class="col-sm-5 p-1">\
          <div class="form-group">\
            <input type="text" class="form-control" id="descripcion" name="descripcion[]" value="" placeholder="Descripcion">\
          </div>\
        </div>\
        <div class="col-sm-1 p-1">\
          <div class="form-group">\
            <input type="text" class="form-control" id="cantidad" name="cantidad[]" value="" placeholder="">\
          </div>\
        </div>\
        <div class="col-sm-2 p-1">\
          <div class="form-group">\
            <div class="input-group">\
              <div class="input-group-append">\
                <button class="btn btn-sm btn-danger" type="button" onclick="remove_reps_fields(' + room + ');"><i class="fa fa-minus"></i></button>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div class="clear"></div>\
      ';

    objTo.appendChild(divtest)
}

function remove_reps_fields(rid) {
    $('.removeclass' + rid).remove();
}

function clone_dom(origin="#origin",target="#target",value=false) {

  var element="item";
  $(origin).clone(true,true).appendTo(target);

  $(target+' '+origin).removeAttr('id').addClass(element+'-'+id_css);

  $(target+' .'+element+'-'+id_css+' .remove-btn').attr('onclick',"remove_dom('"+target+" ."+element+"-"+id_css+"');");

  $(target+' .'+element+'-'+id_css+' .remove-btn').toggleClass('btn-info btn-danger');

  $(target+' .'+element+'-'+id_css+' .remove-btn i').toggleClass('ti-plus ti-minus');

  if (value==false) {

    $(target+' .'+element+'-'+id_css+' input').val('');

  }

  id_css++;

}



function remove_dom(target="") {

  $(target).remove();

}