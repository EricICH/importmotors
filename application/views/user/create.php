<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Nuevo Usuario</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('falla') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row" id="validation">
    <div class="col-12">
      <div class="card wizard-content">
        <div class="card-body">
          <?= form_open('usuario/create',['class'=>'validation-wizard wizard-circle','id'=>'create'],['id_usuario'=> $this->session->IdUser]) ?>
            <!-- Step 1 -->
          <h6>Datos Personales</h6>
            <section>
          
                <div class="form-row">
                  
                  <div class="col-md">
                  <?= form_label('Nombre', 'nombre')?>
                    <?= form_input('nombre', '',['class'=>'form-control']);
                  ?>
                  </div>

                  <div class="col-md">
                    <?= form_label('compañia', ''); ?>
                    <?php $campos=['compania'];
                    $comp=$this->extra->read(['compania'],'usuarios',null,null,'result',['compania'=>'asc'],['group'=>'compania']);?>
                    <select name="proveedor" id="name" class="form-control">
                      <?php foreach ($comp as $item): ?>
                        <option value="<?= $item['compania'] ?>" >
                          <?= $item['compania'] ?>
                        </option>
                      <?php endforeach ?>    
                    </select>         
                  </div>
                  
                  <div class="col-md">
                    <?= form_label('otra compañia (opcional)', 'otra'); ?>
                    <?= form_input('otra_compania', '',['class'=>'form-control']);?>
                  </div>

                  <div class="w-100 mt-2"></div>
                  
                  <div class="col-md">
                    <?= form_label('Cedula', 'ci'); ?>
                    <?= form_input('ci', '',['class'=>'form-control','required'=>'true']);?>
                  </div>
                  <div class="col-md">
                    <?= form_label('RIF', 'rif'); ?>
                    <?= form_input('rif', '',['class'=>'form-control']);?>
                  </div>
                
                  <div class="w-100 mt-2"></div>
                   
            </section>

           
            <!-- Step 2 -->
           <h6>Direccion</h6>
           <section>

             <div class="form-row">
             <div class="col-md-6">
               <?= form_label('Estado', 'estado'); ?>
               <?= form_input('estado', '',['class'=>'form-control']);?>
             </div>
             <div class="col-md-6">
               <?= form_label('Ciudad', 'ciudad'); ?>
               <?= form_input('ciudad', '',['class'=>'form-control']);?>
             </div>
             <div class="col-md-12">
               <?= form_label('Direccion', 'direccion'); ?>
             <textarea name="direccion" rows="3" class="form-control"></textarea>
             </div>
             </div>
           </section>
                     <!-- Step 3 -->
            <h6>Datos de Usuario</h6>
            <section>
              <div class="form-row">
                

                <div class="col-md">
                  <?= form_label('Nombre Usuario', 'usuario'); ?>
                  <?= form_input('usuario', '',['class'=>'form-control','required'=>'true']);?>
                </div>
                <div class="col-md">
                  <?= form_label('Contraseña', 'clave_1'); ?>
                 <?=form_input(array('name'=>'clave_1','class'=>'form-control','type'=>'password'))?>;
                </div>
                <div class="col-md">
                  <?= form_label('Repita Contraseña', 'claveconf'); ?>
                  <?=form_input(array('name'=>'claveconf','class'=>'form-control','type'=>'password'))?>;
                </div>
                <div class="col-md">
                  <?= form_label('Email', 'email'); ?>
                  <?=form_input(array('name'=>'email','class'=>'form-control','type'=>'email'))?>;
                </div>

                <div class="w-100 mt-2"></div>

                <div class="col-md">
                  <?= form_label('Descripcion', 'observacion'); ?>
                  <textarea name="observacion" rows="3" class="form-control"></textarea>
                </div>
              </div>
                <div class="col-md">
                  <?= form_label('Cargo', 'cargo'); ?>
                  <?= form_input('cargo', '',['class'=>'form-control']);?>
                </div>
                <div class="col-md">
                  <?= form_label('telefono', 'telefono_o'); ?>
                  <?= form_input('telefono_o', '',['class'=>'form-control']);?>
                </div>

            </section>
            <!-- Step 4 -->
            <h6>Imagen</h6>
            <section>
             <?= form_label('Cargar Imagen','') ?>
                  <input type="file" id="picture" name="picture" class="dropify dropify-es" />
            </section>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="row" id="validation">
      <div class="card-footer">
        <?= validation_errors('<span class="text-danger">*','</span>') ?>
      </div>
</div>
<script src="<?=base_url('core')?>/assets/plugins/dropify/dist/js/dropify.min.js"></script>
<scrip>
   <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-es').dropify({
            messages: {
                default: 'Click para agregar una imagen',
                replace: 'Click para reemplazar',
                remove: 'Borrar',
                error: 'Error, archivo no valido'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })


        // Basic
        if(act!=""){ 
          $(act).trigger('click');
          $(act).focus();
          
        }

    });

      </script>
</scrip>
