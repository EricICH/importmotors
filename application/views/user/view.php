<?php $read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1)));?> 
<?php
 
  $per_radio = [
    0=>'Sin Acceso',
    1=>'Lectura',
    2=>'Edicion'
  ];
?>
<div class="container-fluid">
  <!-- Content Header (Page header) -->
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Ver</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('usuario') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <?php if($read==2){ ?>
        <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
      <?php } ?>
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
    
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                  <?php if($view['picture']==''){ $foto='assets/images/users/default-user.png'; }else{ $foto=$view['picture'];} ?>
                <img class="profile-user-img img-fluid img-circle" src="<?= base_url('core/').$foto?>" alt="User profile picture">
              </div>

              <h3 class="profile-username text-center"><?= utf8_decode(htmlentities($view['nombre'])); ?></h3>

              <p class="text-muted text-center"><?= $view['cargo'] ?></p>

              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>CI</b> <a class="float-right"><?= $view['ci'] ?></a>
                </li>
                <li class="list-group-item">
                  <b>Tel.</b> <a class="float-right"><?= $view['telefono_c'] ?></a>
                </li>
                <li class="list-group-item">
                  <b>Ofic.</b> <a class="float-right"><?= $view['telefono_o'] ?></a>
                </li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- About Me Box -->
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Informacion</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <strong><i class="fas fa-envelope mr-1"></i> Correo</strong>
              <p class="text-muted"><?= $view['email'] ?></p>
              <hr>

              <strong><i class="fas fa-map-marker-alt mr-1"></i> Ubicacion</strong>
              <p class="text-muted"><?= $view['ciudad'] ?>, <?= $view['estado'] ?></p>
              <hr>

              <strong><i class="fas fa-calendar mr-1"></i> Ingreso</strong>
              <p class="text-muted"><?= $view['fecha_in'] ?></p>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
            <?php if((($read==2)&&($this->session->IdUser!=$view['id_usuario']))||($this->session->Root===true)){ ?>
                <li class="nav-item"><a class="nav-link <?=($this->uri->segment(4)=='p')?'active':'' ?>" href="#permisos" data-toggle="tab">Permisologia</a></li>
            <?php } ?>
                <li class="nav-item"><a class="nav-link <?=($this->uri->segment(4)=='x')?'active':'' ?>" href="#personal" data-toggle="tab">Datos Personales</a></li>
                <li class="nav-item"><a class="nav-link <?=(($this->uri->segment(4)=='u')||($this->uri->segment(4)==''))?'active':'' ?>" href="#user" data-toggle="tab">Datos de Usuario</a></li>
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">
                <div class="tab-pane" id="permisos">
                  <div class="table-responsive">
                    <?=form_open("/usuario/view/{$view['id_usuario']}/p",['id'=>'update','style'=>'width: 100%;'])?>
                      <table class="table table-bordered table-hover table-sm">
                        <caption>Permisologia</caption>
                        <thead>
                          <tr>
                            <th scope="col">Modulo</th>
                            <th scope="col" colspan="3">Permisos</th>
                          </tr>
                        </thead>
                        <tbody>
                          <div id="accordionexample" class="accordion" role="tablist" aria-multiselectable="false">
                          <?php 
                          $i=0;
                          $campos=['m.id_modulo-data','m.name','m.system'];
                          $whereM=['where'=>['is_active'=>1,'id_parent'=>0]];
                          $order=['order'=>'asc'];
                          $modulos= $this->extra->read($campos,'modulo-data m',null,$whereM,'result',$order);
                          foreach ($modulos as $mod):
                            $belong=json_decode($mod['system'],true);
                            //var_dump($belong);
                            $ver=$this->extra->system_verify($belong);
                            if($ver==true): ?>
                              
                              <tr>
                                <th scope="col"><?= $mod['name'] ?></th>
                                <th scope="col" colspan="3">
                                <?php
                                  $permisos=$this->usuario->read_permiso($view['id_usuario'],$mod['id_modulo-data'],'p');
                                 // echo count($permisos);
                                 // var_dump($permisos);
                                  if($permisos==null){ echo "no"; }else{ echo "si"; }
                                  ?>
                                </th>
                              </tr>
                              <?php 
                              $whereS=['where'=>['is_active'=>1,'id_parent'=>$mod['id_modulo-data']]];
                              $seccion= $this->extra->read($campos,'modulo-data m',null,$whereS,'result',$order);
                              foreach ($seccion as $sec): ?>
                                            <tr>
                                              <th scope="col" class="pl-5"><?= $sec['name'] ?></th>
                                              <?=form_hidden('id_modulo-data[]', $sec['id_modulo-data'])?>
                                              <?=form_hidden("parent[{$sec['id_modulo-data']}]", $mod['id_modulo-data'])?>
                                              <?php  
                                              foreach ($per_radio as $key1 => $value1):  
                                                $permisos=$this->usuario->read_permiso($view['id_usuario'],$sec['id_modulo-data']);
                                                if($permisos==null){ $permisos['read']=0; }
                                                ?>
                                                <td>
                                                  <div class="radio-button">    
                                                    <input class="with-gap"  type="radio" <?= ($permisos['read']==$key1)?'checked':'' ?> name="read[<?=$sec['id_modulo-data']?>]"  value="<?= $key1 ?>" id="<?= $sec['id_modulo-data'] ?>_<?= $key1 ?>" >
                                                    <label for="<?= $sec['id_modulo-data'] ?>_<?= $key1 ?>"><?= $value1 ?></label>
                                                  </div>
                                                </td>    
                                        
                                                <?php 
                                              endforeach;
                                              $i++; ?>
                                            </tr>  
                                <?php
                              endforeach;
                            
                            endif; 
                          endforeach; ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <td colspan="4" class="pull-right text-right">
                              <button class="btn, btn-success" type="submit">Guardar</button>                          
                            </td>
                          </tr>
                        </tfoot>
                      </table>
                    <?=form_close()?>
                  </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="personal">
                      <!-- The timeline -->
                 <form class="form-horizontal" method="post" action="<?= site_url($this->uri->segment(1).'/view') ?>/<?=$view['id_usuario']?>/x">
                   <div class="form-group">
                     <label for="username" class="col-sm-2 control-label">Nombre</label>

                     <div class="col-sm-10">
                       <input type="text" class="form-control" id="nombre" name="nombre" value="<?=$view['nombre']?>">
                     </div>
                   </div>
                   <div class="form-group">
                     <label for="telefono_o" class="col-sm-2 control-label">Telefono Oficina</label>

                     <div class="col-sm-10">
                       <input type="text" class="form-control" id="telefono_o" name="telefono_o" placeholder="<?=$view['telefono_o']?>">
                     </div>
                   </div>
                                      
                   <div class="form-group">
                     <div class="col-sm-offset-2 col-sm-10">
                       <button type="submit" class="btn btn-danger">Guardar</button>
                     </div>
                   </div>
                 </form>
                </div>
                <!-- /.tab-pane -->

                <div class="active tab-pane" id="user">

                  <form class="form-horizontal" action="<?=site_url($this->uri->segment(1).'/view') ?>/<?=$view['id_usuario']?>/u" method="POST">
                    <div class="form-group">
                      <label for="username" class="col-sm-4 control-label">Nombre Usuario</label>

                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="username" name="usuario" value="<?=$view['usuario']?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="Email" class="col-sm-4 control-label">Email</label>

                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="Email" name="email" value="<?=$view['email']?>" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="clave_1" class="col-sm-4 control-label">Contraseña</label>

                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="clave_1" name="clave_1" value="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="clavecnf" class="col-sm-4 control-label">Repita Contraseña</label>

                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="clavecnf" placeholder="repita la contraseña">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="cargo" class="col-sm-4 control-label">Cargo</label>

                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="cargo" name="cargo" value="<?=$view['cargo']?>">
                      </div>
                    </div>    
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-danger">Guardar</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
 
</div>