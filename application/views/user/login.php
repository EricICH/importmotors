<?php
  $current_url = implode('/', $this->uri->segment_array());
?>
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Importmotors | Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="<?= base_url('core/')?>css/normalize.css">
  <link rel="stylesheet" href="<?= base_url('core/')?>css/main.css">

</head>
<body>
    <header>
    
    <div class="login-box" >
              <center><a href="http://impormotor.com.ve" class="backHome ini"></a></center>
                    <div id="access">
                    <center>
                    
                   <img src="<?=base_url()?>images/importmotors_logo.png" height="50px" class="registrarse-logo"><br>

                    <?= form_open($current_url,['id'=>'login']); ?>
                    
                        <div class="titulo"><center>Usuario</center></div>
                        <div class="areaCampos">
                            <input type="text" class="campo" placeholder="Usuario" name="username" />
                            <input type="password" class="campo" placeholder="Clave" name="pass" /><br>
                            <input type="submit" class="submit" value="ENTRAR" name="entrar" />
                        </div>
                    </form>
                    <br><br><br><br><br><br>
                    <div id="copy">Derechos Reservados. Copyright &copy; 2016 | Inversiones Impormotor LLS. C.A. J-40336608-0 </div>
                </center>
                </div>
                <br><br><br><br><br><br>
            </div></header>

    
    <footer>
        <p>Derechos Reservados &copy; 2010 - 2020 - Inversiones Impormotor LLS C.A. RIF J-403366080-1</p>
    </footer>


<!-- jQuery -->
<script src="<?= base_url('core/assets/')?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('core/assets/')?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>