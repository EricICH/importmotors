<div class="container-fluid">

  <!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Lista Inventario</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">


      <div class="d-flex m-t-10 justify-content-end">
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
      <div class="d-flex m-t-10 justify-content-end">

        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>

  <!-- Main content -->
 <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header text-uppercase">
          <div class="card-actions">
            <a class="btn-minimize text-primary" data-toggle="modal" data-target="#form_filter" href="">
            <i class="fa fa-filter"></i>
          </a>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-1">
              <table class="table table-sm dt2" >
                <thead>
                  <tr>
                    <th>#</th>
                    <th>nro_parte</th>
                    <!--<th>Almacen</th> -->
                    <th>ubicacion</th>
                    <th>descripcion</th>
                    <th>exist</th>
                    <th>modelos</th>
                    <th>costo</th>
                    <th>Cambio</th>
                  </tr>
                </thead>
                <tbody>
                 
                  <?php $n=1; foreach ($list as $item): ?>
                    <tr >
                      <th><?=$n?></th>
                      <td><?= $item['nro_parte'] ?></td>
                      <td><?= $item['ubicacion'] ?></td>
                      <td><?= $item['descripcion'] ?></td>
                      <td><?= $item['existencia'] ?></td>
                      <td><?= $item['modelos_asociados'] ?></td>
                      <td><?= $item['costo'] ?></td>
                      <td><?= $item['cambio'] ?></td>
                    </tr>
                  <?php $n++; endforeach; ?>
                </tbody>
              </table>
            </div>
        <!-- /.card-body -->
          <div class="card-footer d-flex justify-content-center">
         <br> <div class="font-bold p-1" style="color: #dc3545;">Total match:<?=$total_match?>/Suma matc-rows:<?=$total_match+$total_rows?>/ total: <?=$total?></div>
        </div>
      </div>
      <!-- /.card -->
    </div>
  </div>
</div>


<div id="form_filter" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Filtro</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md">
            <label>Almacen</label>
            <select class="btn btn-light form-control" onchange="location = this.options[this.selectedIndex].value;">
              <?php foreach ($this->almacen->read() as $item): ?>
                <option class="bg-light text-dark" value="<?= site_url($this->uri->segment(1).'/read/a-'.$item['id_inventario_almacen']) ?>"><?= $item['nombre'] ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="col-md-6">
            <label>Nivel Existencia</label>
            <select class="btn btn-light form-control" onchange="location = this.options[this.selectedIndex].value;">
              <option class="bg-light text-dark" value="" disabled selected>Nivel Existencia</option>
              <option class="bg-light text-dark" value="<?= site_url($this->uri->segment(1).'/read/x-a') ?>">Optima</option>
              <option class="bg-light text-dark" value="<?= site_url($this->uri->segment(1).'/read/x-m') ?>">Media</option>
              <option class="bg-light text-dark" value="<?= site_url($this->uri->segment(1).'/read/x-b') ?>">Baja</option>
              <option class="bg-light text-dark" value="<?= site_url($this->uri->segment(1).'/read/x-z') ?>">Agotado</option>

            </select>
          </div>
          <div class="col-md-6">
            <label>Estatus</label>
            <select class="btn btn-light form-control" onchange="location = this.options[this.selectedIndex].value;">
              <option class="bg-light text-dark" value="<?= site_url($this->uri->segment(1).'/read/e-1') ?>">Activo</option>
              <option class="bg-light text-dark" value="<?= site_url($this->uri->segment(1).'/read/e-0') ?>">Inactivo</option>
            </select>
          </div>
           <div class="col-md-6">
            <label>Condicion</label>
            <select class="btn btn-light form-control" onchange="location = this.options[this.selectedIndex].value;">
              <option class="bg-light text-dark" value="<?= site_url($this->uri->segment(1).'/read/e-0') ?>">Activo</option>
              <option class="bg-light text-dark" value="<?= site_url($this->uri->segment(1).'/read/e-1') ?>">Inactivo</option>
            </select>
          </div>
          <div class="col-md-12">
            <label>Personalizado </label>
            <div class="input-group">  
              <input type="text" id="search-input" class="form-control w-50" onchange="location ='<?=site_url($this->uri->segment(1).'/read/p-')?>'+$(this).val();">
              <div class="input-group-append"><button class="btn btn-success" onclick="location ='<?=site_url($this->uri->segment(1).'/read/p-')?>'+$('#search-input').val();"> <i class="fas fa-search"></i> Buscar </button>
              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="<?= site_url($this->uri->segment(1).'/read/view-all') ?>" class="btn btn-danger waves-effect" id="btn_filter">
          <i class="fa fa-circle"></i> Ver todo
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-danger waves-effect" id="btn_filter">
          <i class="fa fa-trash"></i> Limpiar filtro
        </a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->

</div>
</div>

  
      <script>
          $(function() {
              $('.dt2Inv').DataTable({
                  fixedHeader: {
                      footer: true,
                      header: true,
                  },
                  "scrollY": "300px",
                  "ordering": false,
                  "displayLength": 10,
                  "scrollCollapse": false,
                  "lengthChange": false,
                  "paginate": false,
                  "searching": false
                  dom: 'Bfrtip',
                  
                  buttons: [
                      {
                          extend: 'excel',
                          text: '<i class="fa fa-file-excel"></i> Excel',
                          className: 'btn btn-sm'
                      },
                      {
                          extend: 'pdf',
                          text: '<i class="fa fa-file-pdf"></i> Pdf',
                          className: 'btn btn-sm'
                      },
                      {
                          extend: 'print',
                          text: '<i class="fa fa-print"></i>',
                          className: 'btn btn-sm'
                      }
                  ]
              });
          });
      </script>