<div class="container-fluid">
	<div class="row page-titles">
	  <div class="col-md-5 col-8 align-self-center">
	    <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
	    <ol class="breadcrumb">
	      <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
	      <li class="breadcrumb-item active">Crear Orden</li>
	    </ol>
	  </div>
	  <div class="col-md-7 col-4 align-self-center">
	    <div class="d-flex m-t-10 justify-content-end">
	      <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
	        <i class="fa fa-home"></i>
	      </a>
	      <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
	        <i class="fa fa-plus"></i>
	      </a>
	      <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-info">
	        <i class="fa fa-list"></i>
	      </a>
	      <div class="">
	        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="row">
	  <div class="col">		
		<h3 align="center">Importar CSV</h3>
		<br />
		<?= form_open_multipart('Csv_import/'.$control); ?>
		<div class="form-row">
		    <div class="col-12">
			  <label>Seleccione el archivo CSV</label>
				<input type="file" name="csv_file" required accept=".csv" data-toggle="dropify" />
			</div>
			<?php if($control=='conteo'): ?>
			<div class="col-4">
				<label>Fecha</label>
				<input type="date" name="fecha" class="form-control" required />
			</div>
			<div class="col-4">
				<label>Hora</label>
				<input type="time" name="hora" class="form-control" required />
			</div>
			<div class="col-4">
				<label>Personal</label>
				<select id="id_us" name="id_us" class="form-control" required />
					<option selected disabled>Seleccione</option>
					<?php 
					$campos= ["*"];
					$tabla='personal_almacen pa';
					$where= ['where'=> ['pa.id_usuario'=>$this->usuario->id_user()]];
					$order=null;
					$group=null;
					$personal=$this->extra->read($campos, $tabla, null,$where, 'result',$order,$group);
					
					foreach ($personal as $person): ?>
						<option value="<?=$person['id_us']?>"><?=$person['nombre']?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<?php endif; ?>
		</div>
			    

			<div class="w-100 mt-3"></div>
			<button type="submit" name="import_csv" class="btn btn-primary">
				<i class="fa fa-upload"></i> Importar
			</button>
			<a href="<?=base_url('core/assets/files/').$file?>" class="btn btn-primary"><i class="fas fa-file-excel"></i> Descargar Plantilla</a>
		</form>
	  </div>
	</div>
</div>
<script>
	$(document).ready(function(){

		load_data();

		function load_data()
		{
			$.ajax({
				url:"<?php echo base_url(); ?>csv_import/load_data",
				method:"POST",
				success:function(data)
				{
					$('#imported_csv_data').html(data);
				}
			})
		}
	});
</script>