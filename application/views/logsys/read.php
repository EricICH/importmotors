<?php $read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1)));?> 
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Ver</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header text-uppercase">
          Bitacora de sistema
          <div class="card-actions">
            <div class="input-group">
              <label for="dateg"></label>
              <input type="date" id="dateg" name="dateg" class="form-control"   onchange="location = '<?=site_url('logsys/read/')?>'+this.value;">
              <div class="input-group-append">
                <span class="input-group-text" id="basic-addon1" onclick="$('#dateg').focus();"><i class="fa fa-calendar-alt"></i></span>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body table-responsive p-1">
          <table class="display dt2 table table-responsive table-hover table-sm table-striped table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Fecha/hora</th>
                <th>Usuario</th>
                <th>Modulo</th>
                <th>Accion</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php $n = 1 ?>
              <?php foreach ($list as $u): ?>
                <tr>
                  <th><?= $n ?></th>
                  <td><small><?= nice_date($u['date'], 'd/m/Y H:i') ?></small></td>
                  <td><?= $u['nombre'] ?></td>
                  <td><?= $u['controller'] ?></td>
                  <td><?= $u['action'] ?></td>
                  <td width="5%" class="float-right">
                    <!--<div class="btn-group btn-group-sm" hidden>
                      <a href="<?= site_url($this->uri->segment(1).'/view/'.$u['id_log']) ?>" class="btn btn-primary btn-flat">
                        <i class="fa fa-eye"></i>
                      </a>
                    </div>-->
                  </td>
                </tr>
                <?php $n++ ?>
              <?php endforeach ?>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="6">Total registros <?=$total_rows?></th>
              </tr>
            </tfoot>
          </table>
        </div>
        <div class="card-footer d-flex justify-content-center">
          <?= $this->pagination->create_links(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
