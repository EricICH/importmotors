<?php $read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1)));
  $v_tot = count($this->vehiculo->read());
  $v_dis = count($this->vehiculo->read(['estatus'=>1]));
  $v_fue = count($this->vehiculo->read(['estatus'=>2]));
 ?>
<!-- Content Wrapper. Contains page content  -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Ver</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <?php if($read==2){ ?>
        <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
      <?php } ?>
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-lg-4">
      <div class="card card-body">
        <!-- Row -->
        <div class="row">
          <!-- Column -->
          <div class="col p-r-0 align-self-center">
            <h2 class="font-light m-b-0"> <?= $v_dis ?></h2>
            <h6 class="text-muted">Disponible</h6>
          </div>
          <!-- Column -->
          <div class="col text-right align-self-center">
            <div data-label="<?= calc_percent($v_dis,$v_tot,0) ?>%" class="css-bar m-b-0 css-bar-success css-bar-<?= round_number(calc_percent($v_dis,$v_tot,0)) ?>"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="card card-body">
        <!-- Row -->
        <div class="row">
          <!-- Column -->
          <div class="col p-r-0 align-self-center">
            <h2 class="font-light m-b-0"> <?= $v_fue ?></h2>
            <h6 class="text-muted">Fuera de Servicio</h6>
          </div>
          <!-- Column -->
          <div class="col text-right align-self-center">
            <div data-label="<?= calc_percent($v_fue,$v_tot,0) ?>%" class="css-bar m-b-0 css-bar-warning css-bar-<?= round_number(calc_percent($v_fue,$v_tot,0)) ?>"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header text-uppercase">Resumen de Talleres</div>
        <div class="card-body p-1">
          <div id="talleres" style="width:100%; height:400px;"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('taller/scripts'); ?>