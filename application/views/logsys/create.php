<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Agregar</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row" id="validation">
    <div class="col-12">
      <div class="card wizard-content">
        <div class="card-body">
          <?= form_open('taller/create',['class'=>'validation-wizard wizard-circle','id'=>'create'],['id_usuario'=> $this->session->IdUser,'id_taller'=>$edit['id_taller']]) ?>
            <!-- Step 1 -->
            <h6>Informacion</h6>
            <section>
              <div class="form-row">
                <div class="col-md">
                  <label class="text-primary">
                    <i class="fa fa-store-alt"></i> Nombre
                  </label>
                  <?= form_input('nombre',$edit['nombre'],['class'=>'form-control','required'=>'true']); ?>
                </div>
                <div class="col-md-3">
                  <label class="text-primary">
                    <i class="fa fa-address-card"></i> R.I.F
                  </label>
                  <?= form_input('rif',$edit['rif'],['class'=>'form-control']); ?>
                </div>
                <div class="w-100 mt-3"></div>
                <div class="col-md-3">
                  <label class="text-primary">
                    <i class="fa fa-map-marker-alt"></i> Tipo de Taller
                  </label><br>
                  <?= form_radio('tipo',1,$edit['tipo']==1?true:false,['id'=>'interno','class'=>'radio-col-green']) ?>
                  <?= form_label('Interno','interno') ?>
                  <?= form_radio('tipo',2,$edit['tipo']==2?true:false,['id'=>'externo','class'=>'radio-col-red']) ?>
                  <?= form_label('Externo','externo') ?>
                </div>
                <div class="w-100 mb-3"></div>
              </div>
            </section>
            <!-- Step 2 -->
            <h6>Direccion</h6>
            <section>
              <div class="form-row">
                <div class="col-md">
                  <label class="text-primary">
                    <i class="fa fa-map"></i> Estado
                  </label>
                  <?= form_input('estado',$edit['estado'],['class'=>'form-control']); ?>
                </div>
                <div class="col-md-3">
                  <label class="text-primary">
                    <i class="fa fa-map-marked-alt"></i> Ciudad
                  </label>
                  <?= form_input('ciudad',$edit['ciudad'],['class'=>'form-control']); ?>
                </div>
                <div class="w-100 mt-3"></div>
                <div class="col-md">
                  <label class="text-primary">
                    <i class="fa fa-map-marker-alt"></i> Direccion
                  </label>
                  <textarea name="direccion" rows="3" class="form-control"><?=$edit['direccion']?></textarea>
                </div>
                <div class="w-100 mb-3"></div>
              </div>
            </section>
            <!-- Step 3 -->
            <h6>Contacto</h6>
            <section>
              <div class="form-row">
                <div class="col-md">
                  <label class="text-primary">
                    <i class="fa fa-user"></i> Contacto
                  </label>
                  <?= form_input('contacto',$edit['contacto'],['class'=>'form-control']); ?>
                </div>
                <div class="col-md-3">
                  <label class="text-primary">
                    <i class="fa fa-phone"></i> Tel. Oficina
                  </label>
                  <?= form_input('telefono_o',$edit['telefono_o'],['class'=>'form-control']); ?>
                </div>
                <div class="col-md-3">
                  <label class="text-primary">
                    <i class="fa fa-mobile"></i> Tel. Movil
                  </label>
                  <?= form_input('telefono_c',$edit['telefono_c'],['class'=>'form-control'])?>
                </div>
                <div class="w-100 mt-2"></div>

                <div class="col-md">
                  <label class="text-primary">
                    <i class="fa fa-globe"></i> Sitio Web
                  </label>
                  <?= form_input('website',$edit['website'],['class'=>'form-control']); ?>
                </div>
                <div class="col-md">
                  <label class="text-primary">
                    <i class="fa fa-envelope"></i> Correo
                  </label>
                  <?= form_input('email',$edit['email'],['class'=>'form-control'])?>
                </div>
                <div class="w-100 mb-3"></div>
              </div>
            </section>
            <!-- Step 4 -->
            <h6>Otros</h6>
            <section>
              <div class="form-row">
                <div class="col-md">
                  <label class="text-primary">
                    <i class="fa fa-comment-alt"></i> Observacion
                  </label>
                  <textarea name="observacion" rows="3" class="form-control"><?=$edit['observacion']?></textarea>
                </div>
              </div>
            </section>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(function() {
    var id_marca = $('select[name="marca"] option:selected').val();
    $('select[name="modelo"]').load('<?= site_url('extra/option_modelo') ?>',{id_marca: id_marca, selected: ''},function(response,data,xhr) {
      if (status=='error') {
        alert('Error');
      }
    });
    $('select[name="marca"]').on('change', function() {
      $('select[name="marca"] option:selected').each(function() {
        var id_marca = $(this).val();
        alert(id_marca);
        $.post('<?= site_url('extra/option_modelo') ?>', {id_marca: id_marca, selected: ''}, function(data, textStatus, xhr) {
          $('select[name="modelo"]').html(data);
        });
      });
    });
  });

  //send_form('#create')
</script>