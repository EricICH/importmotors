<?php $read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1)));
if ($view['tipo']==1){
  $filter['ot.id_taller']=$view['id_taller'];
  $v_where['where']['ot.id_taller'] = $view['id_taller'];
  $v_where['where']['ot.tipo !='] = 'PERDIDA TOTAL';
  $v_where['where_not_in']['ot.estatus'] = ['CERRADA','ANULADA','FACTURADA'];
}else{
  $filter['ot.taller_externo']=$view['id_taller'];
  $v_where['where']['ot.taller_externo'] = $view['id_taller'];
  $v_where['where']['ot.tipo !='] = 'PERDIDA TOTAL';
  $v_where['where_not_in']['ot.estatus'] = ['CERRADA','ANULADA','FACTURADA'];
}

$vehiculos = $this->extra->read(
  ['*'],
  'autos a',
  [
    'ordenes ot'=>'a.id_auto=ot.id_auto'
  ],
  $v_where,
  'result',
  null,
  ['a.placa']
);

$personal =$this->taller->read_personal(['id_taller'=>$view['id_taller']]);
?>
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Listado</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-arrow-left"></i>
        </a>
        <?php if($read==2){ ?>
        <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/update/'.$view['id_taller']) ?>" class="btn btn-sm btn-info">
          <i class="fa fa-edit"></i>
        </a>
      <?php } ?>
        <!-- <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div> -->
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-body printableArea">
        <h3 class="text-uppercase"><b>Taller</b> <span class="pull-right"><?= $view['nombre'] ?></span></h3>
        <hr>
        <div class="row">
          <div class="col-md">
            <div class="pull-left  text-left">
              <address>
                <p class="">
                  Nombre <b class="font-bold"><?= $view['nombre'] ?></b><br/>
                  Estado <b class="font-bold"><?= $view['estado'] ?></b><br/>
                  Contacto <b class="font-bold"><?= $view['contacto'] ?></b><br>
                  Correo <b class="font-bold"><?= $view['email'] ?></b>
                </p>
              </address>
            </div>
          </div>
          <div class="col-md">
            <div class="pull-left  text-center">
              <address>
                <p class="">
                  R.I.F <b class="font-bold"><?= $view['rif'] ?></b><br/>
                  Ciudad <b class="font-bold"><?= $view['ciudad'] ?></b><br/>
                  Tel. Oficina <b class="font-bold"><?= $view['telefono_o'] ?></b><br>
                  Sitio Web <b class="font-bold"><?= $view['website'] ?></b>
                </p>
              </address>
            </div>
          </div>
          <div class="col-md">
            <div class="pull-right text-right">
              <address>
                <p class="">
                  Taller <b class="font-bold"><?= $view['tipo']==1?'Interno':'Externo' ?></b><br/>
                  Direccion <small><b class="font-bold"><?= $view['direccion'] ?></b></small><br/>
                  Tel. Movil <b class="font-bold"><?= $view['telefono_c'] ?></b><br>
                  Observacion <small><b class="font-bold"><?= $view['observacion'] ?></b></small>
                </p>
              </address>
            </div>
          </div>
          <div class="w-100 mt-3"></div>
          <div class="col-md-6">
            <div class="table-responsive m-t-40">
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th colspan="4" class="text-uppercase text-center">Resumen</th>
                  </tr>
                </thead>
                <tbody>
                <tr class="text-uppercase">
                  <td class="font-weight-bold">Ordenes Asig.</td>
                  <td class="text-right font-weight-bold"><?= count($this->orden_trabajo->read($filter)) ?> </td>
                </tr>
                <tr class="text-uppercase">
                  <td class="font-weight-bold">Vehiculos en Taller</td>
                  <td class="text-right font-weight-bold"><?= count($vehiculos) ?> </td>
                </tr>
                <tr class="text-uppercase">
                  <td class="font-weight-bold">Personal</td>
                  <td class="text-right font-weight-bold"><?= count($personal) ?> </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div class="col-md-12">
            <div class="table-responsive m-t-40" style="clear: both;">
              <table class="table table-bordered table-striped table-sm dt2">
                <thead>
                  <tr>
                    <th colspan="4" class="text-uppercase text-center">Vehiculos En Taller</th>
                  </tr>
                  <tr>
                    <th>Vehiculo</th>
                    <th class="text-right">KM</th>
                    <th class="text-right">Tipo</th>
                    <th class="text-right">Estatus</th>
                  </tr>
                </thead>
                <tbody>

                  <?php foreach ($vehiculos as $item): ?>
                  <tr>
                    <td>Flota: <?= $item['flota'] ?> - Placa: <?= $item['placa'] ?> / <?= $item['marca'] ?> <?= $item['modelo'] ?></td>
                    <td class="text-right"><?= $item['kilometraje'] ?> </td>
                    <td class="text-right"> <?= $item['tipo'] ?> </td>
                    <td class="text-right"> <?= $item['estatus'] ?> </td>
                  </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>

          <div class="col-md-12">
            <div class="table-responsive m-t-40">
              <table class="table dt2 table-bordered  table-sm">
                <thead>
                  <tr>
                    <th colspan="2" class="text-uppercase text-center">Personal</th>
                    <th class="text-right">
                      <a href="<?= site_url('personal/create?taller='.$view['id_taller']) ?>" data-toggle="tooltip" title="Agregar Personal" class="btn btn-sm btn-primary">
                        <i class="fa fa-user-plus"></i>
                      </a>
                    </th>
                  </tr>
                  <tr>
                    <th class="text-center">CI</th>
                    <th class="text-center">Nombre y Apellido</th>
                    <th class="text-right">Cargo</th>
                    <th class="text-right"><?=nbs()?></th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($personal as $item): ?>
                  <tr>
                    <td class="text-center"><?= $item['ci'] ?></td>
                    <td class="text-center"><?= $item['nombre'] ?> <?= $item['apellido'] ?></td>
                    <td class="text-right"><?= $item['cargo'] ?></td>
                    <td class="text-right">
                      <a href="<?=site_url('personal/create/'.$item['id_taller_personal'])?>" class="btn btn-sm btn-info">
                        <i class="fa fa-edit"></i>
                      </a>
                    </td>
                  </tr>
                <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
