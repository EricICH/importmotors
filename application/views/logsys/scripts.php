<?php 
if($this->session->Parent==0){ 
  $idUser=$this->session->IdUser;
}else{
  $idUser=$this->session->Parent;
}
$campos=["t.id_taller","t.nombre","Count(o.nro_orden) as ordenes"];
$where=["where"=>['o.estatus' => "ABIERTA", 't.id_usuario'=>$idUser]];
$join=['ordenes o'=>'t.id_taller = o.id_taller'];
$group=['taller'=>'t.id_taller'];
$resumen=$this->extra->read($campos, 'talleres as t',$join,$where,'result',null,$group);
?>
<script>
  Highcharts.chart('talleres', {
   chart: {
     type: 'column'
   },
   title: {
     text: null
   },
   subtitle: {
     text: null
   },
   xAxis: {
     categories: ['Talleres'],
     crosshair: true
   },
   yAxis: {
     min: 0,
     title: {
       text: null
     }
   },
   tooltip: {
     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
     '<td style="padding:0"><b>{point.y:.0f} mm</b></td></tr>',
     footerFormat: '</table>',
     shared: false,
     useHTML: true
   },
   plotOptions: {
     column: {
       pointPadding: 0.2,
       borderWidth: 0
     },
     series: {
       point: {
         events: {
           click: function () {
             window.location.href = this.series.options.url;
           }
         }
       },
       cursor: 'pointer',
       dataLabels: {
        enabled: true
         //format: '{point.y:.0f}', // one decimal
         //y: 30, // 10 pixels down from the top
       }
     }
   },
   series: [
   <?php foreach ($resumen as $item): ?>
     {
       name: '<?= $item['nombre'] ?>',
       data: [<?=$item['ordenes']?>],
       url: '<?= site_url('orden_trabajo/read/t-'.$item['id_taller']) ?>'
     },
   <?php endforeach ?>
   ]
 });
</script>