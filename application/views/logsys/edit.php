<?php 
  $atts = array(
    'width'       => 800,
    'height'      => 600,
    'scrollbars'  => 'yes',
    'status'      => 'yes',
    'resizable'   => 'yes',
    'screenx'     => 0,
    'screeny'     => 0,
    'window_name' => '_blank'
  );
?>
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <a href="<?= site_url('vehiculo/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
        <a href="<?= site_url('vehiculo/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="card">
      <div class="card-body">
        <?= form_open(implode('/', $this->uri->segment_array()),['id'=>'edit']); ?>
        <div class="form-row">
          <div class="col-md">
            <label class="text-primary">
              <i class="fa fa-hashtag"></i> Flota
            </label>
            <?= form_input('flota',$edit['flota'],['class'=>'form-control']); ?>
          </div>
          <div class="col-md">
            <label class="text-primary">
              <i class="fa fa-list"></i> Marca
            </label>
            <select name="marca" class="form-control">
              <?php foreach ($this->db->get('marcas')->result_array() as $item): ?>
              <option <?= ($edit['id_marca']==$item['id_marca'])?'selected':'' ?> value="<?= $item['id_marca'] ?>"><?= $item['marca'] ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="col-md">
            <label class="text-primary">
              <i class="fa fa-list-ol"></i> Modelo
            </label>
            <select name="modelo" id="" class="form-control">
              <?php foreach ($this->db->get('modelos')->result_array() as $item): ?>
                <option value="<?= $item['id_modelo'] ?>"><?= $item['modelo'] ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="w-100 mt-2"></div>

          <div class="col-md-3">
            <label class="text-primary">
              <i class="fa fa-barcode"></i> Placa
            </label>
            <?= form_input('placa',$edit['placa'],['class'=>'form-control'])?>
          </div>
          <div class="col-md-3">
            <label class="text-primary">
              <i class="fa fa-tasks"></i> Tipo
            </label>
            <select name="tipo" class="form-control">
              <?php foreach ($this->db->get('autos_tipos')->result_array() as $item): ?>
                <option <?= ($edit['id_tipo']==$item['id_tipo'])?'selected':'' ?> value="<?= $item['id_tipo'] ?>"><?= $item['tipo'] ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="col-md">
            <label class="text-primary">
              <i class="fa fa-barcode"></i> Serial Motor
            </label>
            <?= form_input('serial_motor',$edit['serial_motor'],['class'=>'form-control']); ?>
          </div>
          <div class="col-md">
            <label class="text-primary">
              <i class="fa fa-barcode"></i> Serial Carroceria
            </label>
            <?= form_input('serial_carroceria',$edit['serial_carroceria'],['class'=>'form-control']); ?>
          </div>

          <div class="w-100 mt-2"></div>

          <div class="col-md-2">
            <label class="text-primary">
              <i class="fa fa-calendar"></i> Año
            </label>
            <?= form_input(['type'=>'number','name'=>'ano','value'=>$edit['ano'],'class'=>'form-control']); ?>
          </div>
          <div class="col-md">
            <label class="text-primary">
              <i class="fa fa-palette"></i> Color
            </label>
            <?= form_input('color',$edit['color'],['class'=>'form-control']); ?>
          </div>
          <div class="col-md-2">
            <label class="text-primary">
              <i class="fa fa-tachometer-alt"></i> Kilometraje
            </label>
            <?= form_input('kilometraje',$edit['kilometraje'],['class'=>'form-control']); ?>
          </div>
          <div class="col-md">
            <label class="text-primary">
              <i class="fa fa-map-marked-alt"></i> Ubicacion
            </label>
            <select name="sucursal" id="" class="form-control">
              <?php foreach ($this->sucursal->read() as $item): ?>
                <option <?=($edit['id_sucursal']==$item['id_sucursal'])?'selected':''?> value="<?= $item['id_sucursal'] ?>"><?= $item['nombre'] ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="w-100 mt-2"></div>
          <div class="col-md">
            <label class="text-primary">
              <i class="fa fa-comment-dots"></i> Observacion
            </label>
            <textarea name="observacion" rows="3" class="form-control"><?= $edit['observacion']?></textarea>
          </div>
          <div class="w-100 mt-2"></div>
          <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i> Agregar
          </button>
        </div>
        <?= form_close(); ?>
      </div>
    </div>
  </div>
</div>
<script>
  $(function() {
    var id_marca = $('select[name="marca"] option:selected').val();
    $('select[name="modelo"]').load('<?= site_url('extra/option_modelo') ?>',{id_marca: id_marca, selected: '<?= $edit['id_modelo'] ?>'},function(response,data,xhr) {
      if (status=='error') {
        alert('Error');
      }
    });
    $('select[name="marca"]').on('change', function() {
      $('select[name="marca"] option:selected').each(function() {
        var id_marca = $(this).val();
        alert(id_marca);
        $.post('<?= site_url('extra/option_modelo') ?>', {id_marca: id_marca, selected: '<?= $edit['id_modelo'] ?>'}, function(data, textStatus, xhr) {
          $('select[name="modelo"]').html(data);
        });
      });
    });
  });

  send_form('#edit')
</script>