<?php $read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1))); ?> 
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="http://ci3.prevenauto.com/">Inicio</a></li>
        <li class="breadcrumb-item"><a href="http://ci3.prevenauto.com/index.php/vehiculo/">Flota</a></li>
        <li class="breadcrumb-item active">Marcas y Modelos</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex justify-content-end">
       
        <?php if($read==2){ ?>
          <a href="<?= site_url('marca_modelo/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus" style="margin-right: 9px;"></i> <p>Crear</p>
        </a>
      <?php } ?>
      <a href="<?= site_url('marca_modelo/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list" style="margin-right: 9px;"></i> <p>Listado</p>
        </a>
        <!-- <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div> -->
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body table-responsive p-1" style="overflow-x: hidden;">
          <div class="accordion" id="accordionExample">
            <?php foreach ($marca as $ma): ?>

              <div class="card">

                <div class="card-header" id="h<?= $ma['id_marca'] ?>">
                  <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#t<?= $ma['id_marca'] ?>" aria-expanded="true" aria-controls="collapseOne">
                      <?= $ma['marca'] ?>
                    </button>
                  </h2>
                </div>

                <div id="t<?= $ma['id_marca'] ?>" class="collapse" aria-labelledby="h<?= $ma['id_marca'] ?>" data-parent="#accordionExample">

                  <div class="card-body p-1">

                    <table class="display nowrap dt2 table table-hover table-sm table-striped table-bordered">

                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Modelo</th>
                          <th><?= nbs() ?></th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php $n = 1 ?>
                        <?php foreach ($this->marca_modelo->read(['where'=>['mo.id_marca'=>$ma['id_marca']]]) as $mo): ?>
                          <tr>
                            <th><?= $n ?></th>
                            <td><?= $mo['modelo'] ?></td>
                            <td class="float-right">
                              <div class="btn-group btn-group-sm">
                                <a href="<?= site_url($this->uri->segment(1).'/view/'.$mo['id_modelo']) ?>" class="btn btn-outline-primary btn-flat">
                                  <i class="fa fa-arrow-right"></i>
                                </a>
                              </div>
                            </td>
                          </tr>
                          <?php $n++ ?>
                        <?php endforeach ?>
                      </tbody>

                    </table>
                  </div>
                </div>
                
              </div>
            <?php endforeach ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
