<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="http://ci3.prevenauto.com/">Inicio</a></li>
        <li class="breadcrumb-item"><a href="http://ci3.prevenauto.com/index.php/vehiculo/">Flota</a></li>
        <li class="breadcrumb-item"><a href="http://ci3.prevenauto.com/index.php/marca_modelo/">Marcas y Modelos</a></li>
        <li class="breadcrumb-item active">Agregar Marca y Modelo</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          Agregar Marca
        </div>
        <div class="card-body">
          <?= form_open(implode('/', $this->uri->segment_array()).'?s=marca',['id'=>'add_marca'],['id_usuario'=> $this->session->IdUser]); ?>
          <div class="form-row">
            <div class="col-md">
              <label class="text-primary">
                <i class="fa fa-list"></i> Marca
              </label>
              <?= form_input('marca',null,['class'=>'form-control']); ?>
            </div>
            <div class="w-100 mt-2"></div>
            <div class="col-md d-flex justify-content-center">
              <button type="submit" class="btn btn-primary">
                <i class="fa fa-save"></i> Agregar
              </button>
            </div>
          </div>
          <?= form_close(); ?>
        </div>
      </div>
    </div>
    <div class="col-md">
      <div class="card">
        <div class="card-header">
          Agregar Modelo
        </div>
        <div class="card-body">
          <?= form_open(implode('/', $this->uri->segment_array()).'?s=modelo',['id'=>'add_modelo'],['id_usuario'=> $this->session->IdUser]); ?>
          <div class="form-row">
            <div class="col-md">
              <label class="text-primary">
                <i class="fa fa-list"></i> Marca
              </label>
              <select name="id_marca" class="form-control">
                <?php foreach ($this->marca_modelo->read(null,true,['group_by'=>'ma.id_marca']) as $item): ?>
                  <option value="<?= $item['id_marca'] ?>"><?= $item['marca'] ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="col-md">
              <label class="text-primary">
                <i class="fa fa-list-ol"></i> Modelo
              </label>
              <?= form_input('modelo',null,['class'=>'form-control']); ?>
            </div>
            <div class="w-100 mt-2"></div>
            <div class="col-md d-flex justify-content-center">
              <button type="submit" class="btn btn-primary">
                <i class="fa fa-save"></i> Agregar
              </button>
            </div>
          </div>
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>