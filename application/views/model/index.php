<?php  
$read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1)));
$o_tot = count($this->orden_trabajo->read());
$o_abi = count($this->orden_trabajo->read(['ot.estatus'=>'ABIERTA']));
$o_cer = count($this->orden_trabajo->read(['ot.estatus'=>'CERRADA']));
$o_anu = count($this->orden_trabajo->read(['ot.estatus'=>'ANULADA']));
$o_tip = $this->orden_trabajo->read(['ot.estatus'=>'ABIERTA'],'result',null,['ot.tipo']);

?>
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Ver</li>
      </ol>
    </div>
    
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <?php if($read==2){ ?>
        <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
      <?php } ?>
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list" style="margin-right: 9px;"></i> <p>Listado</p>
        </a>
        <!-- <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div> -->
      </div>
    </div>
  </div>

  <div class="row printableArea">
    <div class="col-md-6 col-lg-4">
      <div class="card card-body">
        <!-- Row -->
        <div class="row">
          <!-- Column -->
          <div class="col p-r-0 align-self-center">
            <h2 class="font-light m-b-0"> <?= $o_abi ?></h2>
            <h6 class="text-muted">Abiertas</h6>
          </div>
          <!-- Column -->
          <div class="col text-right align-self-center">
            <div data-label="<?= calc_percent($o_abi,$o_tot,1) ?>%" class="css-bar m-b-0 css-bar-warning css-bar-<?= round_number(calc_percent($o_abi,$o_tot,1)) ?>"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="card card-body">
        <!-- Row -->
        <div class="row">
          <!-- Column -->
          <div class="col p-r-0 align-self-center">
            <h2 class="font-light m-b-0"> <?= $o_cer ?></h2>
            <h6 class="text-muted">Cerradas</h6>
          </div>
          <!-- Column -->
          <div class="col text-right align-self-center">
            <div data-label="<?= calc_percent($o_cer,$o_tot,0) ?>%" class="css-bar m-b-0 css-bar-success css-bar-<?= round_number(calc_percent($o_cer,$o_tot,1)) ?>"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="card card-body">
        <!-- Row -->
        <div class="row">
          <!-- Column -->
          <div class="col p-r-0 align-self-center">
            <h2 class="font-light m-b-0"> <?= $o_anu ?></h2>
            <h6 class="text-muted">Anuladas</h6>
          </div>
          <!-- Column -->
          <div class="col text-right align-self-center">
            <div data-label="<?= calc_percent($o_anu,$o_tot,0) ?>%" class="css-bar m-b-0 css-bar-danger css-bar-<?= round_number(calc_percent($o_anu,$o_tot,1)) ?>"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header text-uppercase">Tendencia de Disponibilidad</div>
        <div class="card-body p-1">
          <div id="o_tipo" style="width:100%; height:400px;"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var myChart = echarts.init(document.getElementById('o_tipo'));

  // specify chart configuration item and data
  option = {
    tooltip: {
      trigger: 'axis'
    },
    legend: {
      data: ['Ordenes']
    },
    toolbox: {
      show: true,
      feature: {
        magicType: { show: true, type: ['line', 'bar'] },
        restore: { show: true },
        saveAsImage: { show: true }
      }
    },
    color: ["#28a745"],
    calculable: true,
    xAxis: [{
      type: 'category',
      data: [
        <?php foreach ($o_tip as $t): ?>
        '<?= ucfirst(strtolower($t['tipo'])) ?>',
        <?php endforeach ?>
      ]
    }],
    yAxis: [{
      type: 'value'
    }],
    series: [
      {
        name: 'Ordenes',
        type: 'bar',
        data: [
          <?php foreach ($o_tip as $t): ?>
          <?= count($this->orden_trabajo->read(['ot.tipo'=>$t['tipo'],'ot.estatus'=>'ABIERTA'])) ?>,
          <?php endforeach ?>
        ],
        markPoint: {
          data: [
            { type: 'max', name: 'Max' },
            { type: 'min', name: 'Min' }
          ]
        },
        markLine: {
          data: [
            { type: 'average', name: 'Average' }
          ]
        }
      }
    ]
  };


  // use configuration item and data specified to show chart
  myChart.setOption(option, true), $(function() {
    function resize() {
      setTimeout(function() {
        myChart.resize()
      }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
  });
</script>