<?php $read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1))); 
  $atts = array(
    'width'       => 800,
    'height'      => 600,
    'scrollbars'  => 'yes',
    'status'      => 'yes',
    'resizable'   => 'yes',
    'screenx'     => 0,
    'screeny'     => 0,
    'window_name' => '_blank'
  );
?>
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Listado</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
      <div class="card"> <img class="card-img" src="<?= base_url('core/')?>assets/images/background/socialbg.jpg" alt="Card image">
        <div class="card-img-overlay card-inverse social-profile">
          <div class="text-center">
            <?php $fi = $this->vehiculo->foto(['id_auto'=>$view['id_auto']],'row',null,null,[1=>null]) ?>
            <img src="<?= base_url('uploads/auto/foto/'.$fi['foto'])?>" class="" width="100">
            <hr>
            <h4 class="card-title">Placa <?= $view['placa'] ?></h4>
            <h6 class="card-subtitle">Flota <?= $view['flota'] ?></h6>
            <p class="text-white"><?= $view['marca'] ?> <?= $view['modelo'] ?></p>
            <p class="text-white"><?= $view['observacion'] ?></p>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <small class="text-muted">Kilometraje </small>
          <h6><?= $view['kilometraje'] ?></h6>

          <small>Serial del Motor</small>
          <h6><?= $view['serial_motor'] ?></h6>

          <small>Serial de Carroceria</small>
          <h6><?= $view['serial_carroceria'] ?></h6>

          <small>Año</small>
          <h6><?= $view['ano'] ?></h6>

          <small>Ubicacion actual</small>
          <h6><?= $view['nombre'] ?></h6>

          <small>Estatus</small>
          <h6><b class="text-<?= $view['css'] ?>"><?= $view['auto'] ?></b></h6>
          <br/>

          <button class="btn btn-circle btn-primary" data-toggle="tooltip" title="Crear Orden">
            <i class="fa fa-wrench"></i>
          </button>
          <button class="btn btn-circle btn-secondary" data-toggle="tooltip" title="Reportar Falla">
            <i class="fa fa-clipboard"></i>
          </button>
          <a class="btn btn-circle btn-info" href="<?= site_url('vehiculo/edit/'.$view['id_auto']) ?>" data-toggle="tooltip" title="Editar">
            <i class="fa fa-edit"></i>
          </a>
        </div>
      </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
      <div class="card">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs profile-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#mantenimiento" role="tab">
              <i class="fa fa-wrench"></i>
              Mantenimientos
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#suministro" role="tab">
              <i class="fa fa-boxes"></i>
              Suministros
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#kilometraje" role="tab">
              <i class="fa fa-tachometer-alt"></i>
              Kilometraje
            </a>
          </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="mantenimiento" role="tabpanel">
            <div class="card-body">
              <div class="table-responsive-sm">
                <h5><b class="text-uppercase">Ordenes Generales</b></h5>
                <table class="table table-striped table-sm dt2s">
                  <thead>
                    <tr>
                      <th scope="col">Fecha</th>
                      <th scope="col">Kms</th>
                      <th scope="col">Costo</th>
                      <th><?= nbs() ?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      $total_trabajo = 0;
                      $total_suministro = 0;
                    ?>
                    <?php foreach ($this->orden->read(null,['id_auto'=>$view['id_auto']]) as $o): ?>
                      <?php 
                        $icon = ($o['estatus']==1)?'lock':'';
                        $trabajo = $this->tempario->trabajo(['where'=>['id_orden'=>$o['id_orden']]]);
                        $costo_trabajo = 0;
                        foreach ($trabajo as $t) {
                          $costo_trabajo += $t['costo'];
                        }
                        $costo_suministro = 0;
                      ?>
                      <tr>
                        <th scope="row"><?= nice_date($o['fecha_in'],'d/m/Y') ?></th>
                        <td><?= $o['kilometraje'] ?></td>
                        <td data-toggle="popover" data-title="Desglose de costos" data-html="true" data-content="Servicios: <?= $costo_trabajo ?> <br> Suministros: <?= $costo_suministro ?>">
                          <?= $costo_trabajo+$costo_suministro ?>
                        </td>
                        <td>
                          <div class="btn-group btn-group-sm float-right" role="group">
                            <button type="button" class="btn btn-link btn-flat" data-toggle="tooltip" title="Orden <?= $o['estatus'] ?>">
                              <i class="fa fa-circle"></i>
                            </button>
                            <button type="button" class="btn btn-outline-primary btn-flat" data-toggle="tooltip" title="Ver orden <?= $o['nro_orden'] ?>">
                              <i class="fa fa-arrow-right"></i>
                            </button>
                            <button type="button" class="btn btn-outline-info btn-flat" data-toggle="tooltip" title="Ver PDF">
                              <i class="fa fa-file-pdf"></i>
                            </button>
                          </div>
                        </td>
                      </tr>      
                      <?php
                        $total_trabajo += $costo_trabajo;
                      ?>                  
                    <?php endforeach ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Total</th>
                      <td></td>
                      <td><?= $total_trabajo+$total_suministro ?></td>
                      <td></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
          <!--second tab-->
          <div class="tab-pane" id="suministro" role="tabpanel">
            <div class="card-body">
              <div class="table-responsive-sm">
                <h5><b class="text-uppercase">Ordenes Generales</b></h5>
                <table class="table table-striped table-sm dt2s">
                  <thead>
                    <tr>
                      <th scope="col">Fecha</th>
                      <th scope="col">Kms</th>
                      <th scope="col">Costo</th>
                      <th><?= nbs() ?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      $total_trabajo = 0;
                      $total_suministro = 0;
                    ?>
                    <?php foreach ($this->orden->read(null,['id_auto'=>$view['id_auto']]) as $o): ?>
                      <?php 
                        $icon = ($o['estatus']==1)?'lock':'';
                        $trabajo = $this->orden->suministro(null,['su.id_orden'=>$o['id_orden']]);
                        $costo_trabajo = 0;
                        foreach ($trabajo as $t) {
                          $costo_trabajo += $t['costo'];
                        }
                        $costo_suministro = 0;
                      ?>
                      <tr>
                        <th scope="row"><?= nice_date($o['fecha_in'],'d/m/Y') ?></th>
                        <td><?= $o['kilometraje'] ?></td>
                        <td data-toggle="popover" data-title="Desglose de costos" data-html="true" data-content="Servicios: <?= $costo_trabajo ?> <br> Suministros: <?= $costo_suministro ?>">
                          <?= $costo_trabajo+$costo_suministro ?>
                        </td>
                        <td>
                          <div class="btn-group btn-group-sm float-right" role="group">
                            <button type="button" class="btn btn-link btn-flat" data-toggle="tooltip" title="Orden <?= $o['estatus'] ?>">
                              <i class="fa fa-circle"></i>
                            </button>
                            <button type="button" class="btn btn-outline-primary btn-flat" data-toggle="tooltip" title="Ver orden <?= $o['nro_orden'] ?>">
                              <i class="fa fa-arrow-right"></i>
                            </button>
                            <button type="button" class="btn btn-outline-info btn-flat" data-toggle="tooltip" title="Ver PDF">
                              <i class="fa fa-file-pdf"></i>
                            </button>
                          </div>
                        </td>
                      </tr>      
                      <?php
                        $total_trabajo += $costo_trabajo;
                      ?>                  
                    <?php endforeach ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Total</th>
                      <td></td>
                      <td><?= $total_trabajo+$total_suministro ?></td>
                      <td></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="kilometraje" role="tabpanel">
            <div class="card-body">
              <form class="form-horizontal form-material">
                <div class="form-group">
                  <label class="col-md-12">Full Name</label>
                  <div class="col-md-12">
                    <input type="text" placeholder="Johnathan Doe" class="form-control form-control-line">
                  </div>
                </div>
                <div class="form-group">
                  <label for="example-email" class="col-md-12">Email</label>
                  <div class="col-md-12">
                    <input type="email" placeholder="johnathan@admin.com" class="form-control form-control-line" name="example-email" id="example-email">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-12">Password</label>
                  <div class="col-md-12">
                    <input type="password" value="password" class="form-control form-control-line">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-12">Phone No</label>
                  <div class="col-md-12">
                    <input type="text" placeholder="123 456 7890" class="form-control form-control-line">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-12">Message</label>
                  <div class="col-md-12">
                    <textarea rows="5" class="form-control form-control-line"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-12">Select Country</label>
                  <div class="col-sm-12">
                    <select class="form-control form-control-line">
                      <option>London</option>
                      <option>India</option>
                      <option>Usa</option>
                      <option>Canada</option>
                      <option>Thailand</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <button class="btn btn-success">Update Profile</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
      <!-- Column -->
  </div>
  <!-- Default box -->
  <div class="card">
    <div class="card-body p-0">
      <div class="accordion" id="accordionExample">
        <div class="card">
          <div class="card-header bg-primary" id="headingOne">
            <h2 class="mb-0">
              <button class="btn btn-link text-white font-weight-bold text-uppercase collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                <i class="fa fa-lg fa-file"></i><?= nbs() ?>Documentacion
              </button>
            </h2>
          </div>
      
          <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body p-0">
              <ul class="list-group">
                <?php foreach ($this->vehiculo->documento(['id_auto'=>$view['id_auto']]) as $d): ?>
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                    <?= $d['nombre'] ?><?= nbs() ?><?= $d['observacion'] ?><?= nbs() ?><span class="text-muted">Cargado: <?= $d['fecha_in'] ?></span>
                    <a href="#" class="btn btn-success badge-pill">
                      <i class="fa fa-lg fa-download"></i>
                    </a>
                  </li>
                <?php endforeach ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header bg-primary" id="headingTwo">
            <h2 class="mb-0">
              <button class="btn btn-link text-white font-weight-bold text-uppercase collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                <i class="fa fa-lg fa-calendar"></i><?= nbs() ?>Planes de Mantenimiento
              </button>
            </h2>
          </div>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body p-0">
              <div class="table-responsive table-responsive-sm" style="height: 250px">
                <table class="table table-striped table-head-fixed table-sm">
                  <thead>
                    <tr>
                      <th scope="col">Plan</th>
                      <th scope="col" data-toggle="tooltip" title="Total dias / Dias transcurridos" data-placement="top">
                        Estimado (Dias)
                      </th>
                      <th scope="col" data-toggle="tooltip" title="Fecha estimada de aplicacion">Fecha</th>
                      <th scope="col" data-toggle="tooltip" title="Kms estimado de aplicacion">Kms</th>
                      <th><?= nbs() ?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($this->plan->asignado(null,['id_auto'=>$view['id_auto']]) as $pma): ?>
                      <?php 
                        $total = count_days($pma['fecha_in'],$pma['fecha_up']);
                        $trascurridos = count_days($pma['fecha_in'],date('Y-m-d'));
                        $alert = ($trascurridos >= $total)?'text-danger':'';
                      ?>
                      <tr>
                        <th scope="row" data-toggle="tooltip" title="<?= $pma['titulo'] ?>"><?= ellipsize($pma['titulo'], 3,.3); ?></th>
                        <td data-toggle="tooltip" title="Dias restantes: <?= $total-$trascurridos ?>" data-placement="top">
                          <?= $total ?> / <b class="<?= $alert ?>"><?= $trascurridos ?></b>
                        </td>
                        <td><?= nice_date($pma['fecha_up'],'d/m/Y') ?></td>
                        <td><?= $pma['kilometraje'] ?></td>
                        <td>
                          <div class="btn-group btn-group-sm float-right" role="group">
                            <?php if ($pma['observacion']!==""): ?>
                              <button type="button" class="btn btn-link text-warning" data-toggle="popover" data-title="Observacion" data-content="<?= $pma['observacion'] ?>">
                                <i class="fa fa-comment-alt"></i>
                              </button>
                            <?php endif ?>
                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#delete_plan">
                              <i class="fa fa-trash"></i>
                            </button>
                            <button type="button" class="btn btn-outline-primary">
                              <i class="fa fa-arrow-right"></i>
                            </button>
                          </div>
                        </td>
                      </tr>

                      <div class="modal" id="delete_plan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content position-static">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLongTitle">Advertencia</h5>
                            </div>
                            <div class="modal-body text-center">
                              <p class="lead"><i class="fa fa-exclamation-triangle fa-lg text-warning"></i> Desea quitar el plan asignado a esta unidad</p>
                            </div>
                            <div class="modal-footer d-flex justify-content-between">
                              <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal"><i class="fa fa-times fa-lg"></i> Cancelar</button>
                              <button type="button" class="btn btn-success btn-flat"><i class="fa fa-check fa-lg"></i> Aceptar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.card-body -->
  </div>
</div>