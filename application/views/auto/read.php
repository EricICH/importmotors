<?php $read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1)));?> 
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=site_url()?>/">Inicio</a></li>
        <li class="breadcrumb-item"><a href="<?=site_url()?>/vehiculo/">Flota</a></li>
        <li class="breadcrumb-item"><a href="<?=site_url()?>/vehiculo/">Vehiculos</a></li>
        <li class="breadcrumb-item active">Listado de Vehiculos</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex justify-content-end">
        <a href="<?= site_url('vehiculo/readKmt') ?>" class="btn btn-sm btn-info mr-1">
          <i class="fas fa-sort-amount-up" style="margin-right: 9px;"></i> <p>Mayor Km</p>
        </a>
        <a href="<?= site_url('vehiculo/readKmt/asc') ?>" class="btn btn-sm btn-info mr-1">
          <i class="fas fa-sort-amount-down" style="margin-right: 9px;"></i> <p>Menor Km</p>
        </a>
        <a href="<?= site_url('vehiculo/readDemand') ?>" class="btn btn-sm btn-info mr-1">
          <i class="fas fa-sort-amount-up" style="margin-right: 9px;"></i> <p>Mas Repuestos</p>
        </a>
        <a href="<?= site_url('vehiculo/readDemand/asc') ?>" class="btn btn-sm btn-info mr-1">
          <i class="fas fa-sort-amount-down" style="margin-right: 9px;"></i> <p>Menos Repuestos</p>
        </a>
        <?php if($this->uri->segment(2)!=='read'): ?>
        <a href="<?= site_url('vehiculo/read') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-list" style="margin-right: 9px;"></i> <p>listado</p>
        </a>
      <?php endif; ?>
        <?php if($read==2): ?>
        <a href="<?= site_url('vehiculo/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus" style="margin-right: 9px;"></i> <p>Crear</p>
        </a>
        <?php endif;?>
        <!-- <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div> -->
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="card">
      <!-- <div class="card-header text-uppercase">
          <div class="card-actions">
            <?php if($read==2){ ?>
            <a class="btn-minimize" href="<?= site_url('vehiculo/create') ?>">
              <i class="fa fa-plus"></i> Agregar Vehiculo
            </a>
          <?php } ?>
          </div>
        </div> -->
        <div class="card-body table-responsive p-1">
          <table class="display datDisplay table table-hover table-sm table-striped table-bordered url" id="tableRead">
            <thead>
              <tr>
                <th colspan="12" style="font-weight: 300; padding: 10px; width: 100%;"><i class="fas fa-car text-warning" style="margin-right: 10px!important;"></i> Fuera de Servicio | <i class="fas fa-flag text-success" style="margin: 0 10px;"></i> Disponible </th>
              </tr>
              <tr>
                <th width="5%">#</th>
                <th>Fecha</th>
                <th>Flota</th>
                <th>Clase</th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Año</th>
                <th>Placa</th>
                <th>Tipo</th>
                <th>Marca/Modelo</th>
                <th>Estatus:</th>
                <th><?=$this->uri->segment(2)=='readKmt'?'Kilometraje':'Falla'?>
                <th>Ubic.</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if($this->uri->segment(4)==""){$n = 1; }else{ $n=$this->uri->segment(4)+1;} ?>
              <?php foreach ($list as $item): ?>
                <?php
                  $this->db->select('t.nombre,o.tipo,o.estatus,o.fecha_in,o.fecha_out, o.descripcion_1 as desc, o.descripcion_2 as obs');
                  $this->db->from('ordenes o');
                  $this->db->join('talleres t', '(t.id_taller = o.id_taller) OR (t.id_taller = o.taller_externo)', 'left');
                  $this->db->where(['o.estatus'=>'ABIERTA', 'o.id_auto'=>$item['id_auto']]);
                  $this->db->limit(1);
                  $ubicacion = $this->db->get()->row_array();
                  $end_date = (!empty($ubicacion['fecha_out'])&&($ubicacion['fecha_out']>$ubicacion['fecha_in']))?$ubicacion['fecha_out']:date('Y-m-d');
                  $days = count_days(@$ubicacion['fecha_in'],$end_date);
                ?>
                <tr data="<?= site_url($this->uri->segment(1).'/view/'.$item['id_auto']) ?>" style="cursor: pointer;">
                  <th><?= $n ?></th>
                  <th><?= $ubicacion['estatus']=='ABIERTA'?nice_date($ubicacion['fecha_in'], 'd/m/Y'):'' ?></th>
                  <td><?= $item['flota'] ?></td>
                  <td><?= @$item['class'] ?></td>
                  <td><?= $item['marca'] ?></td>
                  <td><?= $item['modelo'] ?></td>
                  <td><?= $item['ano'] ?></td>
                  <td ><?= $item['placa'] ?></td>
                  <td><?= $item['tipo']!=''?$item['tipo']:'Pendiente'  ?></td>
                  <td>
                    <span class="hidden-sm-down"><?= $item['marca'] ?> <?= $item['modelo'] ?></span>
                    <span class="hidden-md-up">
                      <i class="fas fa-truck" data-toggle="tooltip" title="<?= $item['marca'] ?> <?= $item['modelo'] ?>" ></i>
                    </span>
                  </td>
                  <td class="tdStat" data-id="<?=$item['id_auto']?>" data-st="<?=$item['estatus']?>">
                     <span class="showIcon" data-toggle="tooltip" title="<?= $item['auto'] ?>">
                       <?php if ($item['estatus']==5){ ?>
                           <span class="badge badge-danger badge-pill">Desincorporado</span>: <?=$item['condicion']?>
                       <?php }else{ ?>
                          <i class="<?= @$item['icon_auto'] ?> text-<?= @$item['css'] ?>"></i><span style="display: none;" class="showText"><?=$item['auto']?></span> <span class="font-weight-bold<?= colorfill(calc_cant($days,'10-20'),'text-','desc') ?>"><small><?=@$ubicacion['estatus']=="ABIERTA"?$days." Dias":'';?></small>
                       <?php }?>
                     </span>
                   </td>
                  <td>
                    <span>
                    <?php if($this->uri->segment(2)=='readKmt') :?>
                      <?=$item['kilometraje']?>
                    <?php else: ?>
                      <?=@$ubicacion['desc']=='Sin Diagnostico'?@$ubicacion['obs']:@$ubicacion['desc']?>
                    <?php endif; ?>
                    </span>
                  </td>
                  <td>
                    <i class="fas fa-warehouse" data-toggle="tooltip" title="<?=@$ubicacion['nombre'] ?>"></i>
                    <span class="showText hidden-sm-down">
                      <?=@$ubicacion['nombre']?>
                    </span>
                  </td>
                </tr>
                <?php $n++ ?>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
        <div class="card-footer d-flex justify-content-center">
          <?= $this->pagination->create_links(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="tst" style="display: none"></div>
<script type="text/javascript">

 $(function() {

   $('.datDisplay').DataTable({
    fixedHeader: {
            "ordering": false,
            "info":     true,
            "header": false
        },
      
      "lengthChange": false,
      "displayLength": true,
      "paginate": false ,
     dom: 'Bfrtip',
       "columnDefs": [
           {
               "targets": [ 1,4,5,6 ],
               "visible": false,
               "searchable": false
           }
       ],
     buttons: [
       {
         extend:'excel',
         text: '<i class="fa fa-file-excel"></i> Excel',
         className: 'btn btn-sm text-test',
         exportOptions: {
            columns: [ 0,2,3,4,5,6,7,8,10,11,12 ]
        } 
       },
       {
         extend:'pdf',
         text: '<i class="fa fa-file-pdf"></i> Pdf',
         className: 'btn btn-sm text-test'
       },
       {
         extend:'print',
         text: '<i class="fa fa-print"></i>',
         className: 'btn btn-sm text-test'
       }
     ]
   });
 });
  $(document).ready(function() {
    $(".text-test").click(function() {
      $(".showIcon").hide();
      $(".showText").show();
      $(".showIcon").show();
      $(".showText").hide();

    });
  });
  $(document).ready(function(){
      function load_stat(dest)
      {
          $(dest).each(function() {
              id=$(this).attr('data-id');
              st=$(this).attr('data-st');
              url='<?= site_url('vehiculo/status/')?>'+id+'/'+st;
              $(this).load(url,function(data) {
                      $(this).html(data);
              });
          });
      }
      setInterval(function(){
          load_stat('.tdStat');
          }, 5000);
  });
</script>