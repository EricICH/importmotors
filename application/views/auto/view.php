<style type="text/css">
  .tab-active{
    background-color: #FF7200;
    color: #ffffff;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="checkout">
    <div class="container">
      <div class="row">

        <!-- Cart Details -->
        <div class="container">
          <div class="cart_details">
            <div class="checkout_title">Datos del vehiculo</div>
            <div class="cart_total">
                  <div ><img width="400px" src="<?=base_url('images')?>/<?=$view['pic']?>" alt=""></div>

              <ul>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <div class="cart_total_title">Condición</div>
                  <div class="cart_total_price ml-auto"></div>
                </li>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <div class="cart_total_title">Marca</div>
                  <div class="cart_total_price ml-auto"><strong><?=$view['brand']?></strong></div>
                </li>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <div class="cart_total_title">Modelo</div>
                  <div class="cart_total_price ml-auto"><strong><?=$view['model']?></strong></div>
                </li>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <div class="cart_total_title">Tipo de vehiculo</div>
                  <div class="cart_total_price ml-auto"></div>
                </li>
               <li class="d-flex flex-row align-items-center justify-content-start">
                  <div class="cart_total_title">Transmisión</div>
                  <div class="cart_total_price ml-auto"></div>
                </li>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <div class="cart_total_title">Año</div>
                  <div class="cart_total_price ml-auto"><strong><?=$view['year']?></strong></div>
                </li>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <div class="cart_total_title">Asientos</div>
                  <div class="cart_total_price ml-auto"><strong><?=$view['cap']?></strong></div>
                </li>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <div class="cart_total_title">Kilometraje</div>
                  <div class="cart_total_price ml-auto"><strong><?=$view['km']?></strong></div>
                </li>
                <li class="d-flex flex-row align-items-center justify-content-start">  
                  <div class="cart_total_title">Motor</div>
                  <div class="cart_total_price ml-auto"><strong><?=$view['type']?></strong></div>
                </li>
              </ul>
            </div>
            
        
          </div>
        </div>
      </div>
    </div>
  </div>
</div>