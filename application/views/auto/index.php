<?php $read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1)));?> 
<?php
  $v_tot = count($this->vehiculo->read(['estatus!='=>5,'disp'=>'S']));
  $m_tot = count($this->vehiculo->read(['estatus!='=>5,'disp'=>'S','trailer'=>'n']));
  $t_tot = count($this->vehiculo->read(['estatus!='=>5,'disp'=>'S','trailer'=>'s']));
  $v_dis = count($this->vehiculo->read(['estatus'=>1,'disp'=>'S']));
  $m_dis = count($this->vehiculo->read(['estatus'=>1,'disp'=>'S','trailer'=>'n']));
  $t_dis = count($this->vehiculo->read(['estatus'=>1,'disp'=>'S','trailer'=>'s']));
  $v_fue = count($this->vehiculo->read(['estatus'=>2,'disp'=>'S']));
  $v_otr = count($this->vehiculo->read(['estatus!='=>5,'disp'=>'N']));
 ?>
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="http://ci3.prevenauto.com/">Inicio</a></li>
        <li class="breadcrumb-item"><a href="http://ci3.prevenauto.com/index.php/vehiculo/">Flota</a></li>
        <li class="breadcrumb-item active">Vehiculos</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center"> 
      <div class="d-flex m-t-10 justify-content-end" style="margin-top: initial;">
        <!-- <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a> -->
        <?php if($read==2){ ?>
        
      <?php } ?>
        <a href="<?= site_url('vehiculo/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list" style="margin-right: 9px;"></i> <p>Listado</p>
        </a>
        <!-- <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div> -->
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3" style="margin-right: 15px;">
      <a href="<?= site_url('vehiculo/create')?>" class="card card-body card-primary text-white" style=" height: 130px; background-color: #FF7200;">
        <p class="font-light m-b-0 text-center font-weight-bold"><i class="fa fa-plus fa-4x"></i><br>Agregar Vehiculo</p>
      </a>
    </div>
    <div class="col-md"  style="margin-right: 15px;">
      <div class="card card-body" style="cursor: pointer" onclick="location ='<?= site_url($this->uri->segment(1).'/read/e-1')?>'">
        <!-- Row -->
        <div class="row">
          <!-- Column -->
          <div class="col col-8 p-r-0 align-self-center">
            <h2 class="font-light m-b-0" style="color: <?=$this->hex['success']?>"> <?= $v_dis ?> <span class="text-muted">Disponibles</span></h2>
            <h4 class="mb-1" style="color: <?=$this->hex['primary']?>"><?= $m_dis ?>/<?=$m_tot?> <small class="text-muted">Camiones</small></h4>
            <h4 class="mb-0 pt-0" style="color: <?=$this->hex['warning']?>"><?= $t_dis ?>/<?=$t_tot?> <small class="text-muted">Trailers</small></h4>
          </div>
          <!-- Column -->
          <div class="col text-right align-self-center">
            <div data-label="<?= calc_percent($v_dis,$v_tot,0) ?>%" class="css-bar m-b-0 css-bar-success css-bar-<?= round_number(calc_percent($v_dis,$v_tot,0)) ?>"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md">
      <div class="card card-body" style="cursor: pointer; 
    height: 130px;
" onclick="location ='<?= site_url($this->uri->segment(1).'/read/e-2')?>'">
        <!-- Row -->
        <div class="row">
          <!-- Column -->
          <div class="col p-r-0 align-self-center">
            <h2 class="font-light m-b-0"> <?= $v_fue ?></h2>
            <h6 class="text-muted">Fuera de Servicio</h6>
          </div>
          <!-- Column -->
          <div class="col text-right align-self-center">
            <div data-label="<?= calc_percent($v_fue,$v_tot,0) ?>%" class="css-bar m-b-0 css-bar-warning css-bar-<?= round_number(calc_percent($v_fue,$v_tot,0)) ?>"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="w-100"></div>

    <div class="col-md">
      <div class="card">
        <div class="card-header text-uppercase">Tendencia de Disponibilidad</div>
        <div class="card-body p-0">
          <div id="tendencia"></div>
        </div>
      </div>
    </div>
    <div class="w-100"></div>
    <div class="col-md">
      <div class="card">
        <div class="card-header text-uppercase">Disponibilidad Promedio Semanal</div>
        <div class="card-body p-0">
          <div id="promsem"></div>
        </div>
      </div>
    </div>
    <div class="col-md">
      <div class="card">
        <div class="card-header text-uppercase">Disponibilidad Promedio Mensual</div>
        <div class="card-body p-0">
          <div id="promed"></div>
        </div>
      </div>
    </div>
    <div class="w-100"></div>
    <div class="col-md">
      <div class="card">
        <div class="card-header text-uppercase">Resumen Flota</div>
        <div class="card-body p-0">
          <div id="status"></div>
        </div>
      </div>
    </div>
    <div class="col-md">
      <div class="card">
        <div class="card-header text-uppercase">Resumen de Disponibilidad por Grupo</div>
        <div class="card-body p-0">
          <div id="grupo"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('vehiculo/scripts'); ?>