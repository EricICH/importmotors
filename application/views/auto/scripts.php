<script src="<?= base_url('core') ?>/assets/plugins/highcharts/code/modules/heatmap.js"></script>
<?php 
  $read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1)));
  $IDU=$this->usuario->id_user();
  $month = [
    '01'=>'Enero',
    '02'=>'Febrero',
    '03'=>'Marzo',
    '04'=>'Abril',
    '05'=>'Mayo',
    '06'=>'Junio',
    '07'=>'Julio',
    '08'=>'Agosto',
    '09'=>'Septiembre',
    '10'=>'Octubre',
    '11'=>'Noviembre',
    '12'=>'Diciembre'
  ];
  $day = [
    '01'=>'Lunes',
    '02'=>'Martes',
    '03'=>'Miercoles',
    '04'=>'Jueves',
    '05'=>'Viernes',
    '06'=>'Sabado',
    '07'=>'Domingo'
  ];
?>
<?php
  $status = $this->vehiculo->read(null,true,['group_by'=>['au.estatus']]);
  $tipo = $this->vehiculo->read(['disp'=>'s'],true,['group_by'=>['au.tipo']]);
  $informe_auto = array_reverse($this->resumen->read(['id_usuario'=>$this->usuario->id_user()],'result',['fecha'=>'desc'],null,[15=>0]));

  $campos=['MONTH(ir.fecha) AS mes','Avg(ir.disponibilidad) as promDisp','Avg(ir.motor) as promMot','Avg(ir.trailer) as promTrail'];
  $where=['where'=>['ir.id_usuario'=>$IDU],"YEAR(ir.fecha) = YEAR(NOW())"];
  $group=['0'=>'MONTH(ir.fecha)'];
  $promedio=$this->extra->read($campos, 'informe_resumen ir',null, $where, 'result',null,$group);  

  $campos=['WEEK(ir.fecha) AS semana','Avg(ir.disponibilidad) as promDisp','Avg(ir.motor) as promMot','Avg(ir.trailer) as promTrail'];
  $group=['0'=>'WEEK(ir.fecha)'];
  $promsem=array_reverse($this->extra->read($campos, 'informe_resumen ir',null, $where, 'result',['fecha'=>'desc'],$group,[12=>0]));  

 ?>
<script>
    Highcharts.setOptions({
      chart: {
          style: {
              fontFamily: '"Poppins", sans-serif',
          }
      }
  });



  Highcharts.chart('status', {
   chart: {
     type: 'column'
   },
   title: {
     text: null,
   },
   xAxis: {
     categories: ['Resumen'],
     crosshair: true
   },
   yAxis: {
      min: 0,
      title: {
        text: null
      },
      stackLabels: {
        enabled: true,
        fontWeight: 'normal',
        style: {
          fontWeight: 'normal !important',
          fontSize: '12px',
          color: (Highcharts.theme && Highcharts.theme.textColor) || '#555555'
        }
      }
    },
   legend: {
     itemStyle: {
       'font-weight': 'normal',
       'font-size': '12px',
     }
   },
   subtitle: {
     text: null
   },
   tooltip: {
     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
     '<td style="padding:0"><b>{point.y:.0f} Unidades</b></td></tr>',
     footerFormat: '</table>',
     shared: false,
     useHTML: true
   },
   plotOptions: {
     column: {
       pointPadding: 0.2,
       borderWidth: 0
     },
     series: {
       point: {
         events: {
           click: function () {
             window.location.href = this.series.options.url;
           }
         }
       },
       cursor: 'pointer',
       dataLabels: {
        enabled: true,
        style: {
          fontWeight: 'normal !important',
          fontSize: '12px',
          color: (Highcharts.theme && Highcharts.theme.textColor) || '#555555'
        }
         //format: '{point.y:.0f}', // one decimal
         //y: 30, // 10 pixels down from the top
       }
     }
   },
   series: [
    {
        name: 'Total General',
        data: [<?= count($this->vehiculo->read(['disp'=>'S'])) ?>],
        url: '<?= site_url('vehiculo/read') ?>',
        color: '#FF7200',
   },
   {
     name: 'Total Operativos',
     data: [<?= count($this->vehiculo->read(['au.estatus <>'=>5,'disp'=>'S'])) ?>],
     color: '#6dbfd6',
     url: '<?= site_url('vehiculo/read') ?>'
   },
   <?php foreach ($status as $item): if($item['id_estatus']==5){ $item['hex']='#555555';  } if($item['id_estatus']==2){ $item['hex']='#f08181';  } if($item['id_estatus']==1){ $item['hex']='#a2e54a';  }?>
     {
       name: '<?= $item['auto'] ?>',
       data: [<?= count($this->vehiculo->read(['au.estatus'=>$item['id_estatus'],'disp'=>'S'])) ?>],
       color: '<?= $item['hex'] ?>',
       url: '<?= site_url('vehiculo/read/e-'.$item['id_estatus']) ?>'
     },
   <?php endforeach ?>
   {
     name: 'Otros (N/A Disp)',
     data: [<?= count($this->vehiculo->read(['au.estatus <>'=>5,'disp'=>'N'])) ?>],
     color: 'black',
     url: '<?= site_url('vehiculo/read') ?>'
   }
   ]
 });
</script>

<script>
   Highcharts.chart('tendencia', {
    chart: {
      zoomType: 'x'
    },
    title: {
      text: null
    },
    subtitle: {
      text: null
    },
    xAxis: {
      categories: [
        <?php foreach ($informe_auto as $item): ?>
          '<?= nice_date($item['fecha'],'d/m') ?>',
        <?php endforeach ?>
      ],
      crosshair: true,
      gridLineWidth: 1
    },
    yAxis:[
            {
              labels:{format:'{value}'},
              title: {text: 'Disponibilidad Total (%)'},
              height:50
            },
            {
              title: {text: 'Disponibilidad por Tipo'},
              labels: {format: '{value}'},
              opposite: true,
              max: 100,
              top: 100,
              height: 180
            }
    ],
    legend: {
      itemStyle: {
          fontWeight: 'normal',
          fontSize: '12px',
          color: (Highcharts.theme && Highcharts.theme.textColor) || '#555555'
        }
    },
    tooltip: {
      shared:true,
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y}%<br>'
    },
    plotOptions: {
      line: {
        dataLabels: {
          enabled: true,
          format: '{point.y:.0f}%',
          style: {
            fontWeight: '400',
            fontSize: '12px'
          }
        },
        enableMouseTracking: true,
      },
      column: {
        dataLabels: {
          enabled: true,
          format: '{point.y:.0f}%',
          style: {
            fontWeight: 'normal',
            fontSize: '12px'
          }
        },
        enableMouseTracking: true,
      }

    },
    series: [
    {

      type: 'line',
      yAxis: 0,
      name: 'General',
      data: [<?php 
        foreach ($informe_auto as $item): 
          ?><?=$item['disponibilidad']==''?0:$item['disponibilidad']?>,<?php 
        endforeach 
      ?>],
      color:'#a2e54a'
    },
    {
      type: 'column',
      yAxis:1,
      name: 'Chuto/Camiones',
      data: [
      <?php foreach ($informe_auto as $item): ?>
        <?=$item['motor']==''?0:$item['motor']?>,
      <?php endforeach ?>
      ],
      color:"#8e70e0"
    },
    {

      type: 'column',
      name: 'Trailers',
      yAxis: 1,
      data: [
      <?php foreach ($informe_auto as $item): ?>
        <?=$item['trailer']==''?0:$item['trailer']?>,
      <?php endforeach ?>
      ],
      color:"#53457b"
    }
    ]
  });
</script>

<script>
  Highcharts.chart('grupo', {
    chart: {
      type: 'column'
    },
    title: {
      text: null
    },
    xAxis: {
      categories: [
        <?php foreach ($tipo as $item): ?>
          '<?= html_entity_decode($item['tipo']) ?>',
        <?php endforeach ?>
      ]
    },
    yAxis: {
      min: 0,
      title: {
        text: null
      },
      stackLabels: {
        enabled: true,
        fontWeight: 'normal',
        style: {
          fontWeight: 'normal',
          fontSize: '12px',
          color: (Highcharts.theme && Highcharts.theme.textColor) || '#555555'
        }
      }
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      itemMarginBottom: 20,
      itemStyle: {
          fontWeight: '300',
          fontSize: '12px',
          color: (Highcharts.theme && Highcharts.theme.textColor) || '#555555'
        }
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
          color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
        }
      },
      series: {
        point: {
          events: {
            click: function () {
              window.location.href = this.series.options.url;
            }
          }
        },
        cursor: 'pointer'
      }
    },
    series: [
    <?php foreach ($status as $item):  if($item['id_estatus']==5){ $item['hex']='#555555';  } if($item['id_estatus']==2){ $item['hex']='#f08181';  } if($item['id_estatus']==1){ $item['hex']='#a2e54a';  }?>    
      {
        name: '<?= $item['auto'] ?>',
        data: [
        <?php foreach ($tipo as $item2): ?>
          <?= count($this->vehiculo->read(['at.id_tipo'=>$item2['id_tipo'],'es.id_estatus'=>$item['id_estatus']])) ?>,
        <?php endforeach ?>
        ],
        url: '<?= site_url('vehiculo/read/e-'.$item['id_estatus']) ?>',
        color: '<?= $item['hex'] ?>'
      },
    <?php endforeach ?>
    ]
  });

  Highcharts.chart('promed', {
    chart: {
      zoomType: 'x'
    },
    title: {
      text: null
    },
    subtitle: {
      text: null
    },
    xAxis: {
      categories: [
        <?php $c=''; foreach ($promedio as $item): if($item['mes']!=null): $mes=zerofill($item['mes'],2); ?>
          <?=$c?>'<?= $this->month[$mes] ?>'
        <?php endif; $c=','; endforeach; ?>
      ],
      crosshair: true,
      gridLineWidth: 1
    },
    yAxis: [
            {
              labels:{format:'{value}'},
              title: {text: 'Disponibilidad Total (%)'},
              height:110
            },
            {
              title: {text: 'Disponibilidad por Tipo'},
              labels: {format: '{value}'},
              opposite: true,
              max: 100,
              top: 100,
              height: 180
            },
    ],
    tooltip: {
      shared:true,
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '<b>{series.name}:</b> {point.y}%<br>'
    },
    plotOptions: {
      series: {
        dataLabels: {
          enabled: true,
            format: '{point.y:.1f} %'
        },
        enableMouseTracking: true,
      }
    },
    series: [
    {
      type: 'line',
      name: 'promedio',
      data: [<?php $c=''; 
        foreach ($promedio as $item): 
          if($item['promDisp']==null):
            $item['promDisp']=0;;
          endif; 
          $prom=number_format($item['promDisp'],2);
          ?><?=$c.$prom?><?php 
          $c=','; 
        endforeach; 
        ?>],
      color:'rgb(162, 229, 74)'
    },
    {
      type: 'column',
      name: 'Chuto/Camiones',
      yAxis:1,
      data: [<?php $c=''; 
        foreach ($promedio as $item): 
          if($item['promMot']==null):
            $item['promMot']=0;
          endif;
          $prom=number_format($item['promMot'],2);
          ?><?=$c?><?=$prom?><?php 
          $c=','; 
        endforeach; 
        ?>],
      color:'#4c956c'
    },
    {
      type: 'column',
      name: 'Trailers',
      yAxis:1,
      data: [<?php $c=''; 
        foreach ($promedio as $item): 
          if($item['promTrail']==null): 
            $item['promTrail']=0;
          endif;
          $prom=number_format($item['promTrail'],2);
          ?><?=$c?><?=$prom?><?php 
          $c=','; 
        endforeach; 
        ?>],
      color:'#8fcb9b'
    }
    ]
  });


  Highcharts.chart('promsem', {
    chart: {
      zoomType: 'x'
    },
    title: {
      text: null
    },
    subtitle: {
      text: null
    },
    xAxis: {
      categories: [
        <?php $c=''; foreach ($promsem as $item): if($item['semana']!=null): $semana=zerofill($item['semana'],2); ?>
          <?=$c?>'Semana <?= $semana ?>'
        <?php endif; $c=','; endforeach; ?>
      ],
      crosshair: true,
      gridLineWidth: 1
    },
    yAxis: [
            {
              labels:{format:'{value}'},
              title: {text: 'Disponibilidad Total (%)'},
              height:110
            },
            {
              title: {text: 'Disponibilidad por Tipo'},
              labels: {format: '{value}'},
              opposite: true,
              max: 100,
              top: 100,
              height: 180
            }
    ],
    tooltip: {
      shared: true,
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '<b>{series.name}:</b> {point.y}%<br>'
    },
    plotOptions: {
      series: {
        dataLabels: {
          enabled: true,
            format: '{point.y:.1f} %'
        },
        enableMouseTracking: true,
      }
    },
    series: [
    {
      type: 'line',
      name: 'promedio',
      data: [<?php $c=''; 
        foreach ($promsem as $item): 
          if($item['promDisp']==null):
            $item['promDisp']=0;;
          endif; 
          $prom=number_format($item['promDisp'],2);
          ?><?=$c.$prom?><?php 
          $c=','; 
        endforeach; 
        ?>],
      color:'rgb(162, 229, 74)'
    },
    {
      type: 'column',
      name: 'Chuto/Camiones',
      yAxis:1,
      data: [<?php $c=''; 
        foreach ($promsem as $item): 
          if($item['promMot']==null):
            $item['promMot']=0;
          endif;
          $prom=number_format($item['promMot'],2);
          ?><?=$c?><?=$prom?><?php 
          $c=','; 
        endforeach; 
        ?>],
      color:'#4c956c'
    },
    {
      type: 'column',
      name: 'Trailers',
      yAxis:1,
      data: [<?php $c=''; 
        foreach ($promsem as $item): 
          if($item['promTrail']==null): 
            $item['promTrail']=0;
          endif;
          $prom=number_format($item['promTrail'],2);
          ?><?=$c?><?=$prom?><?php 
          $c=','; 
        endforeach; 
        ?>],
      color:'#8fcb9b'
    }

    ]
  });
</script>