<div class="checkout">
    <div class="container">
      <div class="row">

        <!-- Billing Details -->
        <div class="col-lg-6">
          <div class="billing">
            <div class="checkout_title">Detalles</div>
            <div class="checkout_form_container">
              <?=form_open_multipart(site_url('auto/create'), ['id'=>"checkout_form"],['id_auto'=>@$edit['id_auto']])?>
                <select name="condition" id="condition" class="country_select checkout_input">
                  <option disabled selected>Condición</option>
                  <option value="1" <?=@$edit['condition']==1?'selected':''?>>Nuevo</option>
                  <option value="0" <?=@$edit['condition']==0?'selected':''?>>Usado</option>
                </select>
                <select name="brand" id="brand" class="country_select checkout_input">
                  <option disabled selected>Marcas</option>
                  <?php foreach ($brands as $brand): ?>
                    <option value="<?=$brand['id_brand']?>" <?=$brand['id_brand']==@$edit['brand']?'selected':''?>><?=$brand['brand']?></option>
                  <?php endforeach ?>
                  <option value="0">Nuevo</option>
                </select>
                <input type="text" class="checkout_input" name="n_brand" id="n_brand" placeholder="nueva marca" style="display: none;">
                <select name="model" id="model" class="country_select checkout_input">
                <?php if(@$edit['model']!==null): ?>
                  <?php foreach ($models as $model): ?>
                    <option value="<?=$model['id_model']?>" <?=$model['id_model']==@$edit['model']?'selected':''?>>
                      <?=$model['model']?>
                    </option>
                  <?php endforeach ?>
                <?php endif; ?>  
                </select>
                
                <input type="text" class="checkout_input" name="n_model" id="n_model" placeholder="nuevo modelo" style="display: none;">
                  <select name="type" id="type" class="country_select checkout_input">
                  <option disabled>Tipo de vehiculo</option>
                  <option  <?=@$edit['condition']==0?'selected':''?>>Electrico</option>
                  <option <?=@$edit['condition']==1?'selected':''?>>Combustión</option>
                </select>
                <select name="transmission" id="transmission" class="country_select checkout_input">
                  <option>Transmisión</option>
                  <option>Sicrononico</option>
                  <option>Automatico</option>
                </select>
                <div class="d-flex flex-lg-row flex-column align-items-start justify-content-between">
                  <input type="text" class="checkout_input checkout_input_50" name="year" id="year"  placeholder="Año" >
                  <input type="text" class="checkout_input checkout_input_50" placeholder="Asientos"  >
                </div>
                <input type="text" class="checkout_input" placeholder="Kilometraje" value="<?=@$edit['km']?>" name="km" id="km">
                <input type="text" class="checkout_input" placeholder="Motor" value="<?=@$edit['consumo']?>" name="consumo">
                <input type="text" class="checkout_input" placeholder="color" value="<?=@$edit['color']?>" name="color">
                <input type="text" class="checkout_input" placeholder="Autonomia" value="<?=@$edit['autonomy']?>" name="autonomy">
                <input type="text" class="checkout_input" placeholder="placa" name="plate" value="<?=@$edit['value']?>">
                <input type="file" class="checkout_input" placeholder="pic" name="picture" id="picture"  capture>
                <textarea name="obs" class="checkout_input" placeholder="observaciones"><?=@$edit['obs']?></textarea>
                <div class="d-flex flex-lg-row flex-column align-items-start justify-content-between">
                </div>
                <button type="submit" class="btn btn-secondary">Guardar</button>
                <div class="billing_options">
                </div>
              </form>
            </div>
          </div>
        </div>

        <!-- Cart Details -->
           </div>
        </div>
      </div>
    </div>
  </div>
<script>
  $(function() {
    
    var id_brand = $('select[name="brand"] option:selected').val();
    $('select[name="model"]').load('<?= site_url('auto/option_model') ?>/'+id_brand ,{id_brand: id_brand, selected: ''},function(response,data,xhr) {
      if (status=='error') {
        alert('Error');
      }
    });
    
    $('select[name="brand"]').on('change', function() {
      $('select[name="brand"] option:selected').each(function() {
        var id_brand = $(this).val();
        if(id_brand==0){
          $("#n_brand").show();
          $("#n_model").show();
        }else{
          $("#n_brand").hide(); 
          $("#n_model").hide(); 
          //alert(id_brand);
        }
        $('select[name="model"]').load('<?= site_url('auto/option_model') ?>/'+id_brand ,{id_brand: id_brand, selected: ''},function(response,data,xhr) {
          if (status=='error') {
            alert('Error');
          }
        });
      });
    });

    $('select[name="model"]').on('change', function() {
      $('select[name="model"] option:selected').each(function() {
        var id_model = $(this).val();
        if(id_model==0){
          $("#n_model").show();
        }else{
          $("#n_model").hide();
        }
      });
    });

  });
</script>