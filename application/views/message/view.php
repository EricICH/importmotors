<?php 
$atts = array(
  'width'       => 800,
  'height'      => 600,
  'scrollbars'  => 'yes',
  'status'      => 'yes',
  'resizable'   => 'yes',
  'screenx'     => 0,
  'screeny'     => 0,
  'window_name' => '_blank'
);
?>
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Listado</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">

        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="row">
          <div class="col-xlg-2 col-lg-3 col-md-4">
            <div class="card-body inbox-panel"><a href="<?=site_url('message/read')?>#compose" class="btn btn-danger m-b-20 p-10 btn-block waves-effect waves-light">Redactar</a>
              <ul class="list-group list-group-full">
                <li class="list-group-item"> 
                  <a href="<?=site_url('message/read')?>#inbox"><i class="mdi mdi-gmail"></i> Recibidos </a>
                  <?php if($this->unread>0){ ?><span class="badge badge-success ml-auto"><?=$this->unread?></span><?php }?>
                </li>
                <li class="list-group-item">
                  <a href="<?=site_url('message/read')?>#stared"> <i class="mdi mdi-send"></i> Archivados </a>
                </li>
                <li class="list-group-item ">
                  <a href="<?=site_url('message/read')?>#sent"> <i class="mdi mdi-file-document-box"></i> Enviados </a>
                </li>
                <li class="list-group-item">
                  <a href="<?=site_url('message/read')?>#trash"> <i class="mdi mdi-delete"></i> Papelera </a>
                </li>
              </ul>
            </div>
            </div>
            <div class="col-xlg-10 col-lg-9 col-md-8">
              <div class="card-body">
                <div class="btn-group m-b-10 m-r-10" role="group" aria-label="Button group with nested dropdown">
                  <button type="button" class="btn btn-secondary font-18"><i class="mdi mdi-inbox-arrow-down"></i></button>
                  <button type="button" class="btn btn-secondary font-18"><i class="mdi mdi-delete"></i></button>
                </div>
              </div>
              <div class="card-body p-t-0">
                <div class="card b-all shadow-none">
                  <div class="card-body">
                    <h3 class="card-title m-b-0"><?=$view['title']?></h3>
                  </div>
                  <div>
                    <hr class="m-t-0">
                  </div>
                  <div class="card-body">
                    <div class="d-flex m-b-40">
                      <div>
                        <a href="javascript:void(0)"><img src="<?=base_url("{$view['fromPic']}")?>" alt="user" width="40" class="img-circle" /></a>
                      </div>
                      <div class="p-l-10">
                        <h4 class="m-b-0"><?=$view['from']?></h4>
                      </div>
                    </div>
                    <?=$view['mensaje']?>
                  </div>
                  <div>
                    <hr class="m-t-0">
                  </div>
                  <div class="card-body">
                    <!--<h4><i class="fa fa-paperclip m-r-10 m-b-10"></i> Attachments <span>(3)</span></h4>
                    <div class="row">
                      <div class="col-md-2">
                        <a href="#"> <img class="img-thumbnail img-responsive" alt="attachment" src="../assets/images/big/img1.jpg"> </a>
                      </div>
                      <div class="col-md-2">
                        <a href="#"> <img class="img-thumbnail img-responsive" alt="attachment" src="../assets/images/big/img2.jpg"> </a>
                      </div>
                      <div class="col-md-2">
                        <a href="#"> <img class="img-thumbnail img-responsive" alt="attachment" src="../assets/images/big/img3.jpg"> </a>
                      </div>
                    </div>
                  -->
                    <div class="b-all m-t-20 p-20 compose" id="view">
                      <p class="p-b-20">click aqui para <a href="javascript:void(0)" class="panelTab reply" data-target="#reply">Responder</a> o <a href="javascript:void(0)" class="panelTab forward" data-target="#forward">Reenviar</a></p>
                    </div>

                  </div>
                    <div class="card-body p-t-0 compose reply" id="reply" style="display: none;">
                        <h3 class="card-title">Responder Mensaje</h3>
                        <?=form_open('message/create', ['id'=>'create'],['from'=>$this->session->IdUser])?>
                        <div class="form-group">
                            <input type="hidden" name="id_to" value="<?=$view['id_from']?>">
                            <input type="hidden" name="reply" value="<?=$view['id_message']?>">
                        </div>
                        <div class="form-group">
                            <textarea class="textarea_editor form-control" name="mensaje" rows="15" placeholder="Mensaje ..."></textarea>
                        </div>
                        <!--<h4><i class="ti-link"></i> Adjuntar</h4>

                          <div class="fallback">
                            <input name="file" type="file" multiple />
                          </div>-->

                        <button type="submit" class="btn btn-success m-t-20"><i class="fa fa-envelope-o"></i> Enviar</button>
                        <button type="button" class="btn btn-inverse m-t-20 panelTab view" data-target="#view"><i class="fa fa-times"></i> Descartar</button>
                        </form>
                    </div>
                    <div class="card-body p-t-0 compose" id="forward" style="display: none;">
                        <h3 class="card-title">Reenviar Mensaje</h3>
                        <div class="form-group">
                            <?=form_open('message/create', ['id'=>'create'],['from'=>$this->session->IdUser])?>

                            <select name="id_to[]" class="form-control form-control-sm s2" style="width: 100%"  multiple>
                                <option value="0">Todos</option>
                                <?php if($this->session->IdUser==3){?>
                                    <?php foreach ($this->extra->read('*','usuarios',null,['where'=>['id_usuario_master'=>0]]) as $item): ?>
                                        <option value="<?= $item['id_usuario'] ?>"><?=$item['nombre'] ?></option>
                                    <?php endforeach ?>
                                <?php }?>
                                <?php foreach ($this->extra->read('*','usuarios',null,['where'=>['id_usuario_master'=>$this->session->IdUser]]) as $item): ?>
                                    <option value="<?= $item['id_usuario'] ?>"><?=$item['nombre'] ?></option>
                                <?php endforeach ?>

                                <?php if($this->session->Parent!=0){?>
                                    <?php foreach ($this->extra->read('*','usuarios',null,['where'=>['id_usuario_master'=>$this->session->Parent,'id_usuario!='=>$this->session->IdUser]]) as $item): ?>
                                        <option value="<?= $item['id_usuario'] ?>"><?=$item['nombre']?></option>
                                    <?php endforeach ?>
                                <?php } ?>
                                <option value="3">Inversiones Impormotor</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="form-control" name="title" placeholder="Asunto:" value="Reenviado: <?=$view['title']?>">
                        </div>
                        <div class="form-group">
                            <textarea class="textarea_editor form-control" name="mensaje" rows="15"  placeholder="Mensaje ..."><?=$view['mensaje']?></textarea>
                        </div>
                        <!--<h4><i class="ti-link"></i> Adjuntar</h4>

                          <div class="fallback">
                            <input name="file" type="file" multiple />
                          </div>-->

                        <button type="submit" class="btn btn-success m-t-20"><i class="fa fa-envelope-o"></i> Enviar</button>
                        <button type="button" class="btn btn-inverse m-t-20 panelTab" data-target="#view"><i class="fa fa-times"></i> Descartar</button>

                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script>
    $('.panelTab').click( function() {
        $('.compose').hide();
        target=$(this).attr('data-target');
        $(target).show();
    });
</script>