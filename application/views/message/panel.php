<div class="card-body inbox-panel panelTab compose" data-target="#compose"><a href="javascript:void(0)" class="btn btn-danger m-b-20 p-10 btn-block waves-effect waves-light">Redactar</a>
  <ul class="list-group list-group-full">
   <li class="list-group-item inbox panelTab active" data-target="#inbox"> 
    <a href="javascript:void(0)"><i class="mdi mdi-gmail"></i> Recibidos </a>
    <?php if($this->unread>0){ ?><span class="badge badge-success ml-auto"><?=$this->unread?></span><?php }?>
  </li>
  <li class="list-group-item panelTab stared" data-target="#stared">
    <a href="javascript:void(0)"><i class="mdi mdi-send"></i> Archivados</a>
  </li>
  <li class="list-group-item panelTab sent" data-target="#sent">
    <a href="javascript:void(0)"> <i class="mdi mdi-file-document-box"></i> Enviados </a>
  </li>
  <li class="list-group-item panelTab trash" data-target="#trash">
    <a href="javascript:void(0)"> <i class="mdi mdi-delete"></i> Papelera </a>
  </li>
</ul>
</div>