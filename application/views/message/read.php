<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Listado</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="row">
          <div class="col-xlg-2 col-lg-3 col-md-4">
            <div class="card-body inbox-panel"><a href="javascript:void(0)" class="btn btn-danger m-b-20 p-10 btn-block waves-effect waves-light panelTab compose" data-target="#compose">Redactar</a>
              <ul class="list-group list-group-full">
               <li class="list-group-item panelTab inbox active" data-target="#inbox"> 
                <a href="javascript:void(0)"><i class="mdi mdi-gmail"></i> Recibidos </a>
                <?php if($this->unread>0){ ?><span class="badge badge-success ml-auto"><?=$this->unread?></span><?php }?>
              </li>
              <li class="list-group-item panelTab stared" data-target="#stared">
                <a href="javascript:void(0)"><i class="mdi mdi-send"></i> Archivados</a>
              </li>
              <li class="list-group-item panelTab sent" data-target="#sent">
                <a href="javascript:void(0)"> <i class="mdi mdi-file-document-box"></i> Enviados </a>
              </li>
              <li class="list-group-item panelTab trash" data-target="#trash">
                <a href="javascript:void(0)"> <i class="mdi mdi-delete"></i> Papelera </a>
              </li>
            </ul>
            </div>
            </div>
            <div class="col-xlg-10 col-lg-9 col-md-8">

                <div class="card-body">
                  <div class="btn-group m-b-10 m-r-10" role="group" aria-label="Button group with nested dropdown">
                    <button type="button" class="btn btn-secondary font-18" data-toggle="tooltip" data-title="Archivar" onclick="$('#estatus').val(3); $('#list').submit() "><i class="mdi mdi-inbox-arrow-down"></i></button>
                    <button type="button" class="btn btn-secondary font-18" data-toggle="tooltip" data-title="Marcar Como Leido" onclick="$('#estatus').val(1); $('#list').submit()"><i class="mdi mdi-alert-octagon"></i></button>
                    <button type="button" class="btn btn-secondary font-18" data-toggle="tooltip" data-title="Eliminar" onclick="$('#estatus').val(2); $('#list').submit()"><i class="mdi mdi-delete"></i></button>

                  </div>
                </div>
                <div class="card-body p-t-0 bandeja" id="compose" style="display: none;">
                    <h3 class="card-title">Redactar Mensaje</h3>
                    <div class="form-group">
                      <?=form_open('message/create', ['id'=>'create'],['from'=>$this->session->IdUser])?>

                      <select name="id_to[]" class="form-control form-control-sm s2" style="width: 100%"  multiple>
                        <option value="0">Todos</option>
                          <?php if($this->session->IdUser==3){?>
                              <?php foreach ($this->extra->read('*','usuarios',null,['where'=>['id_usuario_master'=>0]]) as $item): ?>
                                  <option value="<?= $item['id_usuario'] ?>"><?=$item['nombre'] ?></option>
                              <?php endforeach ?>
                          <?php }?>
                        <?php foreach ($this->extra->read('*','usuarios',null,['where'=>['id_usuario_master'=>$this->session->IdUser]]) as $item): ?>
                          <option value="<?= $item['id_usuario'] ?>"><?=$item['nombre'] ?></option>
                        <?php endforeach ?>

                        <?php if($this->session->Parent!=0){?>
                          <?php foreach ($this->extra->read('*','usuarios',null,['where'=>['id_usuario_master'=>$this->session->Parent,'id_usuario!='=>$this->session->IdUser]]) as $item): ?>
                          <option value="<?= $item['id_usuario'] ?>"><?=$item['nombre']?></option>
                        <?php endforeach ?>
                        <?php } ?>
                        <option value="3">Inversiones Impormotor</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <input class="form-control" name="title" placeholder="Asunto:">
                    </div>
                    <div class="form-group">
                      <textarea class="textarea_editor form-control" name="mensaje" rows="15" placeholder="Mensaje ..."></textarea>
                    </div>
                    <!--<h4><i class="ti-link"></i> Adjuntar</h4>

                      <div class="fallback">
                        <input name="file" type="file" multiple />
                      </div>-->

                    <button type="submit" class="btn btn-success m-t-20"><i class="fa fa-envelope-o"></i> Enviar</button>
                    <button class="btn btn-inverse m-t-20 panelTab" data-target="#inbox"><i class="fa fa-times"></i> Descartar</button>

                     </form>
                  </div>
                <?=form_open('message/read', ['id'=>'list', 'after-send'=>'message/read'])?>
                <input type="hidden" name="estatus" id="estatus"/>

                <div class="card-body p-t-0 bandeja" id="inbox">
                    <div class="card b-all shadow-none">
                      <div class="inbox-center table-responsive">
                        <table class="table table-hover no-wrap">
                          <tbody>
                            <?php
                              $i=0;
                              if(count($inbox)>0){
                              
                                foreach($inbox as $msg):
                                $from=$this->usuario->read(['id_usuario'=>$msg['id_from']],'row');

                              ?>
                              <tr class="<?=$msg['status']==0?'font-bold':''?>">
                                <td style="width:40px">
                                  <div class="checkbox">
                                    <input type="checkbox" name="msgCheck[]" id="checkbox_<?=$i?>" value="<?=$msg['id_message']?>">
                                    <label for="checkbox_<?=$i?>"></label>
                                  </div>
                                </td>
                                <td style="width:40px" class="hidden-xs-down"><i class="fa fa-star-o"></i></td>
                                <td class="hidden-xs-down text-black-100"><?=$msg['from']?></td>
                                <td class="max-texts"> <a href="<?=site_url('message/view/'.$msg['id_message'])?>" />
                                  <?php if($msg['fromParent']==0){ ?><span class="label label-warning m-r-10">Admin</span> <?php } ?>
                                  <?=$msg['title']?>
                                </td>
                                <?php if($msg['attach']!=null){ ?><td class="hidden-xs-down"><i class="fa fa-paperclip"></i></td><?php }?>
                                <td class="text-right"> <?=nice_date($msg['date'],(nice_date($msg['date'],'Y-m-d')==date("Y-m-d"))?'H:i a':'d/m/Y')?> </td>
                              </tr>

                            <?php $i++; endforeach;
                              }else{ ?>
                              <tr class="unread">
                                <td colspan="6">
                                  No Posee Mensajes en la bandeja de entrada
                                </td>
                              </tr>
                              <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                <div class="card-body p-t-0 bandeja" id="sent" style="display: none;">
                    <div class="card b-all shadow-none">
                      <div class="inbox-center table-responsive">
                        <table class="table table-hover no-wrap">
                          <tbody>
                            <?php
                              if(count($sent)>0){

                                foreach($sent as $msg):

                              ?>
                              <tr >
                                <td style="width:40px">
                                  <div class="checkbox">
                                    <input type="checkbox" name="msgCheck[]" id="checkbox_<?=$i?>" value="<?=$msg['id_message']?>">
                                    <label for="checkbox_<?=$i?>"></label>
                                  </div>
                                </td>
                                <td style="width:40px" class="hidden-xs-down"><i class="fa fa-star-o"></i></td>
                                <td class="hidden-xs-down text-black-100"><?=$msg['to']?></td>
                                <td class="max-texts"> <a href="<?=site_url('message/view/'.$msg['id_message'])?>" />
                                  <?php if($msg['fromParent']==0){ ?><span class="label label-warning m-r-10">Admin</span> <?php } ?>
                                  <?=$msg['title']?>
                                </td>
                                <?php if($msg['attach']!=null){ ?><td class="hidden-xs-down"><i class="fa fa-paperclip"></i></td><?php }?>
                                <td class="text-right"> <?=nice_date($msg['date'],(nice_date($msg['date'],'Y-m-d')==date("Y-m-d"))?'H:i a':'d/m/Y')?> </td>
                              </tr>

                            <?php $i++; endforeach;
                              }else{ ?>
                              <tr class="unread">
                                <td colspan="6">
                                  No Posee Mensajes en la bandeja de Enviados
                                </td>
                              </tr>
                              <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                <div class="card-body p-t-0 bandeja" id="stared" style="display: none;">
                    <div class="card b-all shadow-none">
                      <div class="inbox-center table-responsive">
                        <table class="table table-hover no-wrap">
                          <tbody>
                            <?php
                              if(count($stared)>0){
                                foreach($stared as $msg):
                              ?>
                             <tr class="<?=$msg['status']==0?'font-bold':''?>">
                                <td style="width:40px">
                                  <div class="checkbox">
                                    <input type="checkbox" name="msgCheck[]" id="checkbox_<?=$i?>" value="<?=$msg['id_message']?>">
                                    <label for="checkbox_<?=$i?>"></label>
                                  </div>
                                </td>
                                <td style="width:40px" class="hidden-xs-down"><i class="fa fa-star-o"></i></td>
                                <td class="hidden-xs-down text-black-100"><?=$msg['from']?></td>
                                <td class="max-texts"> <a href="<?=site_url('message/view/'.$msg['id_message'])?>" />
                                  <?php if($msg['fromParent']==0){ ?><span class="label label-warning m-r-10">Admin</span> <?php } ?>
                                  <?=$msg['title']?>
                                </td>
                                <?php if($msg['attach']!=null){ ?><td class="hidden-xs-down"><i class="fa fa-paperclip"></i></td><?php }?>
                                <td class="text-right"> <?=nice_date($msg['date'],(nice_date($msg['date'],'Y-m-d')==date("Y-m-d"))?'H:i a':'d/m/Y')?> </td>
                              </tr>

                            <?php $i++; endforeach;
                              }else{ ?>
                              <tr class="unread">
                                <td colspan="6">
                                  No Posee Mensajes en la bandeja de Importantes
                                </td>
                              </tr>
                              <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                <div class="card-body p-t-0 bandeja" id="trash" style="display: none;">
                <div class="card b-all shadow-none">
                  <div class="inbox-center table-responsive">
                    <table class="table table-hover no-wrap">
                      <tbody>
                        <?php 
                          if(count($trash)>0){ 

                            foreach($trash as $msg):
                          ?>
                          <tr class="<?=$msg['status']==0?'font-bold':''?>">
                            <td style="width:40px">
                              <div class="checkbox">
                                <input type="checkbox" name="msgCheck[]" id="checkbox_<?=$i?>" value="<?=$msg['id_message']?>">
                                <label for="checkbox_<?=$i?>"></label>
                              </div>
                            </td>
                            <td style="width:40px" class="hidden-xs-down"><i class="fa fa-star-o"></i></td>
                            <td class="hidden-xs-down text-black-100"><?=$msg['from']?></td>
                            <td class="max-texts"> <a href="<?=site_url('message/view/'.$msg['id_message'])?>" />
                              <?php if($msg['fromParent']==0){ ?><span class="label label-warning m-r-10">Admin</span> <?php } ?>
                              <?=$msg['title']?>
                            </td>
                            <?php if($msg['attach']!=null){ ?><td class="hidden-xs-down"><i class="fa fa-paperclip"></i></td><?php }?>
                            <td class="text-right"> <?=nice_date($msg['date'],(nice_date($msg['date'],'Y-m-d')==date("Y-m-d"))?'H:i a':'d/m/Y')?> </td>
                          </tr>

                        <?php $i++; endforeach; 
                          }else{ ?>
                          <tr class="unread">
                            <td colspan="6">
                              No Posee Mensajes en la Papelera
                            </td>
                          </tr>
                          <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
             </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script>
  $('.panelTab').click( function() {
      $('.panelTab').removeClass('active');
      $(this).addClass('active');
      $('.bandeja').hide();
      target=$(this).attr('data-target');
      $(target).show();
      window.location.hash = target;
  });
  
  var hash = window.location.hash, //get the hash from url
  cleanhash = hash.replace("#", ""); //remove the #
  //alert(cleanhash);
  if (cleanhash == "") {
    cleanhash = "inbox";
  }
  $("."+cleanhash).click();
</script>