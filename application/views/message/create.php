<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Agregar</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <?=form_open('message/create', ['id'=>'create'],['from'=>$this->session->IdUser])?>
        <div class="row">
            <div class="col-xlg-2 col-lg-3 col-md-4">
             <?php $this->load->view('message/panel'); ?>
            </div>
            
            <div class="col-xlg-10 col-lg-9 col-md-8">              
              <div class="card-body">
                <h3 class="card-title">Redactar Mensaje</h3>
                <div class="form-group">
                 
                  <input class="form-control" placeholder="To:">
                </div>
                <div class="form-group">
                  <input class="form-control" placeholder="Subject:">
                </div>
                <div class="form-group">
                  <textarea class="textarea_editor form-control" rows="15" placeholder="Enter text ..."></textarea>
                </div>
                <h4><i class="ti-link"></i> Adjuntar</h4>
                <form action="#" class="dropzone">
                  <div class="fallback">
                    <input name="file" type="file" multiple />
                  </div>
                </form>
                <button type="submit" class="btn btn-success m-t-20"><i class="fa fa-envelope-o"></i> Enviar</button>
                <button class="btn btn-inverse m-t-20"><i class="fa fa-times"></i> Descartar</button>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script>
  //send_form('#add_marca');
</script>