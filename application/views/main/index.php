

  <!-- Home -->
<div class="home">
    <div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/categories.jpg" data-speed="0.8"></div>
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="home_container">
            <div class="home_content">
              <div class="home_title">Catalogo</div>
              <div class="breadcrumbs">
                <ul>

                
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Products -->

  <div class="products">
    <div class="container">
      <div class="row">
        <div class="col-12">
          
          <!-- Sidebar Left -->

          <div class="sidebar_left clearfix">

            <!-- Categories -->
            <div class="sidebar_section">
              <div class="sidebar_title">Marcas</div>
              <div class="sidebar_section_content">
                <ul>
                  <li><a href="#"></a></li>
                  <li><a href="#"></a></li>
                  <li><a href="#"></a></li>
                  <li><a href="#"></a></li>
                </ul>
              </div>
            </div>
            
            <!-- Color -->
            <div class="sidebar_section">
              <div class="sidebar_title">Color</div>
              <div class="sidebar_section_content sidebar_color_content mCustomScrollbar" data-mcs-theme="minimal-dark">
                <ul>
                  <li><a href="#"><span style="background:#a3ccff"></span>Azul (23)</a></li>
                  <li><a href="#"><span style="background:#937c6f"></span>Marron (11)</a></li>
                  <li><a href="#"><span style="background:#000000"></span>Negro (61)</a></li>
                  <li><a href="#"><span style="background:#ff5c00"></span>Naranja (34)</a></li>
                  <li><a href="#"><span style="background:#a3ffb2"></span>Verde (17)</a></li>
                  <li><a href="#"><span style="background:#f52832"></span>Rojo (22)</a></li>
                  <li><a href="#"><span style="background:#fdabf4"></span>Rosado (7)</a></li>
                  <li><a href="#"><span style="background:#ecf863"></span>Amarillo (13)</a></li>
                </ul>
              </div>
            </div>

            <!-- Size -->
            

            <!-- Price -->
            
            <!-- Best Sellers -->


            <!-- Size -->

          </div>


        </div>
        <div class="col-12">
          <div class="product_sorting clearfix">
            <div class="sorting">
              <ul class="item_sorting">
                <li>
                  <span class="sorting_text">Mostrar todo</span>
                  <i class="fa fa-caret-down" aria-hidden="true"></i>
                  <ul>
                    <li class="product_sorting_btn" data-isotope-option='{ "sortBy": "original-order" }'><span>Mostrar todo</span></li>
                    <li class="product_sorting_btn" data-isotope-option='{ "sortBy": "price" }'><span>Precio</span></li>
                    <li class="product_sorting_btn" data-isotope-option='{ "sortBy": "stars" }'><span>Estrellas</span></li>
                  </ul>
                </li>
                <li>
                  <span class="num_sorting_text">12</span>
                  <i class="fa fa-caret-down" aria-hidden="true"></i>
                  <ul>
                    <li class="num_sorting_btn"><span>3</span></li>
                    <li class="num_sorting_btn"><span>6</span></li>
                    <li class="num_sorting_btn"><span>12</span></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="row products_container">
        <div class="col">
          
          <!-- Products -->

          <div class="product_grid">

            <!-- Product -->
          <?php foreach ($list as $item): ?>
            
            <div class="product">
              <div class="product_image"><img src="<?=base_url('images')?>/<?=$item['pic']?>" alt=""></div>
              <div class="rating rating_4" data-rating="4">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
              </div>
              <div class="product_content clearfix">
                <div class="product_info">
                  <div class="product_name"><a href="<?=site_url('auto/view/')?><?=$item['id_auto']?>"><?=$item['brand']?> <?=$item['model']?> <?=$item['color']?></a></div>
                  <div class="product_price">$<?=$item['price']?></div>
                </div>
                <div class="product_options">
                  <div class="product_buy product_option"><img src="<?=base_url('images')?>/<?=$item['pic']?>" alt=""></div>
                  <div class="product_fav product_option">+</div>
                </div>
              </div>
            </div>
          <?php endforeach ?>
           
            
          </div>
        </div>
          
      </div>

      <div class="row page_num_container">
        <div class="col text-right">
          <ul class="page_nums">
            <li><a href="#">01</a></li>
            <li class="active"><a href="#">02</a></li>
            <li><a href="#">03</a></li>
            <li><a href="#">04</a></li>
            <li><a href="#">05</a></li>
          </ul>
        </div>
      </div>

    </div>
    
    <!-- Sidebar Right -->

 

  </div>

  <!-- Extra -->

  

  <!-- Newsletter -->

  <div class="newsletter">
    <div class="newsletter_content">
      <div class="newsletter_image" style="background-image:url(<?=base_url('images')?>/newsletter.jpg)"></div>
      <div class="container">
        <div class="row">
          <div class="col">
            
          </div>
        </div>
        <div class="row newsletter_container">
          <div class="col-lg-10 offset-lg-1">
            <div class="newsletter_form_container">
              
            </div>
          
          </div>
        </div>
      </div>
    </div>
  </div>
