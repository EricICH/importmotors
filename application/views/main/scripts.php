<?php
if ($this->session->Root !== true) {
  if ($this->session->Parent == 0) {
    $U=['where'=>['tp.id_usuario'=>$this->session->IdUser]];
    //$where=array_merge($, $P);
  }else{
    $U=['group_or'=>['tp.id_usuario'=>[$this->session->IdUser,$this->session->Parent]]];
  }
}
$campos=[
  'Count(tr.id_trabajo) AS trabajo',
  'Count(case when tr.estatus = "Finalizado" then 1 else null end) as finalizados',
  'CONCAT(tp.nombre," ", tp.apellido) AS tecnico',
  'count(case WHEN ord.tipo LIKE "%MANT%" then 1 end) as preventivo',
  'count(case WHEN ord.tipo LIKE "%REP%" then 1 end) as correctivo',
  'count(case WHEN (ord.tipo NOT LIKE  "%REP%" and ord.tipo NOT LIKE "MANT%")then  1 end) as otros'
];
$join=['trabajos tr'=>'tr.id_talleres_personal_1 = tp.id_taller_personal OR tr.id_talleres_personal_2 = tp.id_taller_personal OR tr.id_talleres_personal_3 = tp.id_taller_personal','talleres ta' => 'ta.id_taller = tp.id_taller', 'ordenes as ord' => 'ord.id_orden = tr.id_orden'];

$campos2=['sum(case WHEN tp.tipo LIKE "%MANTENIMIENTO%" then 1 end) as preventivo','sum(case WHEN tp.tipo LIKE "%REPARACION%" then 1 end) as correctivo','sum(case WHEN (tp.tipo NOT LIKE  "%REPARACION%" and tp.tipo NOT LIKE "%MANTENIMIENTO%")then  1 end) as otros',
'count(tp.id_orden) as total'];
$where=array_merge($U,["tr.fecha_in >= DATE_SUB(CURRENT_DATE(),INTERVAL 1 MONTH) "]);

$where2=array_merge($U,["tp.fecha_in >= DATE_SUB(CURRENT_DATE(),INTERVAL 1 MONTH) "]);

$group=['group'=>'tecnico'];
$tec_trabs=$this->extra->read($campos,'talleres_personal tp',$join, $where, 'result',['trabajo'=>'desc'], $group);
//print_r($this->db->last_query());
$tip_trab=$this->extra->read($campos2, 'ordenes tp',null, $where2, 'row');

$personal='';
$trabajos='';
$finalizados='';
$trab_perc='';
$eficiencia='';
$total=0;
$c='';
$actDate=date('Y-m-d');
?>
<script>
  $(document).ready(function() {
    $(".carousel").carousel('pause');
  });
  $('.carousel').on('slid.bs.carousel', function () {
    $(this).carousel('pause'); 
  });

Highcharts.chart('tecGraph', {
      chart: {
          type: 'pie'
      },
      title: {
          text: null
      },
      subtitle: {
          text: 'Total Ordenes: <?=nice_date($actDate, 'd/m/Y')?> <b><?=$tip_trab['total']?></b><br>Periodo evaluado de: <b><?=nice_date(date("Y-m-d",strtotime($actDate."- 1 month")),'d/m/Y')?></b> hasta <b><?=nice_date($actDate,'d/m/Y')?></b>'
      },
      xAxis: {
          type: 'category',
          labels: {
            animate: true
          }
      },
      yAxis: {
          title: {
              text: 'Tipos de Trabajos'
          }
      },
      legend: {
          enabled: false
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{point.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> Orden(es)'
      },
      plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.y:.0f} Ordenes | {point.percentage:.1f} %',
              style: {
                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
              }
            }
          }
        },
      series: [{
       name: 'Distribucion del trabajo por tipo',
       colorByPoint: true,
       data: [
       {
          name: 'Preventivo',
          y: <?= number_format($tip_trab['preventivo'],1,'.','') ?>,
          sliced: true,
          selected:true,
          color:'#007cbc'  },
       {
          name: 'Correctivo',
          y: <?= number_format($tip_trab['correctivo'],1,'.','') ?>,
          color:'<?= $this->hex['danger'] ?>'
       },
       {
         name: 'Otros',
         y: <?= number_format($tip_trab['otros'],1,'.','')  ?>,
         color:'gray'
       }
       ]
      }]
  });
  <?php foreach ($tec_trabs as $trabs) {
        $total+=$trabs['trabajo'];
 } ?>

var categories = [<?php $c='';foreach ($tec_trabs as $trabs) {?><?=$c?>"<?=$trabs['tecnico']?>"<?php $c=',';}?>];
Highcharts.chart('tecGraph2', {
  chart: {
    type: 'bar',
    events: {
      load: function() {
        var points = this.series[0].points,
          chart = this,
          newPoints = [];

        Highcharts.each(points, function(point, i) {
          point.update({
            name: categories[i]
          }, false);
          newPoints.push({
            x: point.x,
            y: point.y,
            name: point.name
          });
        });
        chart.redraw();

        // Sorting max - min
        $('#sort1').on('click', function() {
          newPoints.sort(function(a, b) {
            return b.y - a.y
          });

          Highcharts.each(newPoints, function(el, i) {
            el.x = i;
          });

          chart.series.setData(newPoints, true,true, true);
        });

      }
    }
  },
 title: {
     text: null
 },
 subtitle: {
     text: "Total Trabajos: <?=$total?>"
 },
  xAxis: {
    categories: [ <?php $c=''; foreach ($tec_trabs as $trabs) { echo $c; ?>"<?=$trabs['tecnico']?>"<?php $c=','; }?>],
    labels: {
      animate: true
    }
  },
  yAxis: {
    title: {
      text: 'Porcentaje de Distribucion'
    },
    stackLabels: {
      enabled: true,
      filter: {
        property: 'percentage',
        operator: '>',
        value: 1
      },
      style: {
        fontWeight: 'bold',
        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
      }
    }
  },
  tooltip: {
    valueSuffix: '',
      format: '{point.name}: {point.y}'
  },
  legend: {
    enabled: true,
    reversed: true,
    align: 'center',
    verticalAlign: 'top',
    itemDistance: 5,
    itemStyle: {
      "font-family": "'Open Sans', sans-serif",
      "fontSize": "16px",
      "fontWeight": "normal"
    }
  },
  plotOptions: {
    series: {
      stacking: 'normal',
      dataLabels: {
        enabled: true,
        filter: {
          property: 'percentage',
          operator: '>',
          value: 0
        },
        style: {
          "font-family": "'Open Sans', sans-serif",
          "fontSize": "10px",
          "fontWeight": "normal",
          "textOutline": false
        },
        format: '{point.y:.0f}'
      }
    }
  },
  series: [
  {
    name: "Preventivo",
    data: [<?php $c=''; foreach ($tec_trabs as $trabs) {?><?=$c?><?=$trabs['preventivo']==null?'0':$trabs['preventivo']?><?php $c=','; } ?>],
    color: "#007cbc"
  },
  {
    name: "Correctivo",
    data: [<?php $c=''; foreach ($tec_trabs as $trabs) {?><?=$c?><?=$trabs['correctivo']==null?'0':$trabs['correctivo']?><?php $c=','; } ?>],
    color: '<?= $this->hex['danger'] ?>'
  }, 
  {
    name: "Otros",
    data: [<?php $c=''; foreach ($tec_trabs as $trabs) {?><?=$c?><?=$trabs['otros']==null?'0':$trabs['otros']?><?php $c=','; } ?>],
    color: 'gray'
  }
  ]
});
$('#sort1').click();
</script>