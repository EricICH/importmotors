<?php $read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1)));?> 
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Ver</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('usuario') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <?php if($read==2){ ?>
        <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
      <?php } ?>
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>

  <!-- Main content -->
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header text-uppercase">
          <h3 class="card-title">Lista</h3>
          <div class="card-actions">
            <a class="btn-minimize" href="javascript:void(0);">
              <i class="fa fa-filter"></i>
            </a>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-head-fixed table-sm dt2 url">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>CI</th>
                <th>Cargo</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($this->personal->read() as $item): ?>
                <?php $nombre = strtolower($item['nombre'].' '.$item['apellido']) ?>
                <tr data="<?= site_url($this->uri->segment(1).'/view/'.$item['id_usuario']) ?>">
                  <td><?= ucwords($nombre) ?></td>
                  <td><?= $item['ci'] ?></td>
                  <td><?= $item['cargo'] ?></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
</div><!-- /.container-fluid -->
  