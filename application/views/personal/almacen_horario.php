<?php 
    $idUser=$this->usuario->id_user();
?>
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Listado</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md">
      <div class="card">
        <ul class="nav nav-tabs customtab" role="tablist">

          <li class="nav-item"> 
            <a class="nav-link" data-toggle="tab" id="nav-hor" href="#horario" role="tab" aria-controls="nav-hor" aria-selected="true">
              <span class="hidden-lg-up">
                <i class="fas fa-calendar-alt"></i>
              </span> 
              <span class="hidden-md-down"><i class="fas fa-calendar-alt"></i> Horario Semanal</span>
            </a> 
          </li>

          <li class="nav-item"> 
            <a class="nav-link active" data-toggle="tab" href="#gestion" role="tab" id="nav-ges" aria-controls="nav-ges" >
              <span class="hidden-lg-up">
                <i class="fas fa-user"  data-toggle="tooltip" title="Personal"></i>
              </span> 
              <span class="hidden-md-down"><i class="fas fa-user" ></i>  Gestion Turnos Personal</span>
            </a> 
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade " id="horario" role="tabpanel">
            <div class="card-header text-uppercase">
              Horario Personal Almacen
              <div class="card-actions">
                <?php if ($this->uri->segment(3)=='alert-plan'): ?>
                  <div class="">
                    <button id="print" class="btn btn-danger">
                      <span class="fa fa-print"></span>
                    </button>
                  </div>
                <?php endif ?>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive printableArea">
              <table class="table table-sm table-striped table-bordered">
                <thead class="text-center">
                  <tr>
                      <th width="10%">Turno/Dia</th>
                    <?php for($i=1; $i<=7; $i++){ ?>
                      <th width="12%" class="border"><b><strong><?=$day[$i]?></strong></b></th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($turnos as $t){ ?>
                  <tr>
                    <?php 
                    $hora_in=new DateTime(nice_date($t['hora_in'], 'H:i')); 
                    $int='PT'.($t['jornada']+$t['alm']).'H';
                    ?>
                    <th class="text-center "><strong><?php echo $hora_in->format('h:i a'); ?></strong> <strong><?php 
                    $hora_in->add(new DateInterval($int));
                    echo $hora_in->format('h:i a');
                    ?></strong></th>
                    <?php for($i=1; $i<=7; $i++){ ?>
                      <td class="border">
                    
                    <?php foreach($personal as $p){
                        $tp=$this->personal->turnos(['where'=>['id_turno'=>$p['turno']]],false);
                        $dia_fin=$tp['dia_in']+$tp['duracion']>7?$tp['dia_in']+$tp['duracion']-7:$tp['dia_in']+$tp['duracion'];
                        $lab='n';
                          if($dia_fin<$tp['dia_in']){
                            if(($i>=$tp['dia_in'])||($i<$dia_fin)){ $lab='s'; }  
                          }else{
                            if(($i>=$tp['dia_in'])&&($i<$dia_fin)){ $lab='s'; }  
                          }
                          
                          
                          if($i>5){ $tp['turno']=2; }
                          if ($t['turno']==$tp['turno']){ 
                            if ($lab=='s'){  ?>
                              <div id="list_<?=$p['id_personal_alm']?>">
                                <span class="badge"><?=$p['nombre']?></span><br>
                              </div>
                      <?php }
                          }
                        } ?>
                      </td>
                    <?php } ?>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
            <div class="card-footer d-flex justify-content-between">
              <button id="print" class="btn btn-danger">
                <span class="fa fa-print"></span>
              </button>
            </div>
          </div>

          <div class="tab-pane active show " id="gestion" role="tabpanel">
            <div class="card">
            <div class="card-header text-uppercase">
              Horario Personal Almacen
              <div class="card-actions">
                <?php if ($this->uri->segment(3)=='alert-plan'): ?>
                  <div class="">
                    <button id="print" class="btn btn-danger">
                      <span class="fa fa-print"></span>
                    </button>
                  </div>
                <?php endif ?>
              </div>
            </div>
            <!-- /.card-header -->
             <div class="card-body table-responsive printableArea">
             
              <table class="table table-sm table-striped table-bordered" style="border: solid">
                <thead class="text-center">
                  <tr>
                      <th width="5%">item</th>
                      <th width="">Personal</th>
                    <?php for($i=1; $i<=7; $i++){ ?>
                      <th width="5%" class="border"><b><strong><?=$d[$i]?></strong></b></th>
                    <?php } ?>
                    <th>O.B.S.</th>
                    <th>Horario</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0; $n=1;?>
                  <?php foreach($turnos as $t){ 
                          $row=0;
                          foreach ($personal as $pe) {
                            $tp=$this->personal->turnos(['where'=>['id_turno'=>$pe['turno']]],false);
                            if($tp['hora_in']==$t['hora_in']){
                              $row++;
                            }
                          }
                          $tot=[1=>0, 2=>0, 3=>0, 4=>0, 5=>0, 6=>0, 7=>0];
                          foreach ($personal as $p) {
                            $tp=$this->personal->turnos(['where'=>['id_turno'=>$p['turno']]],false);
                            if($tp['hora_in']==$t['hora_in']){
                              $dia_fin=$tp['dia_in']+$tp['duracion']>7?$tp['dia_in']+$tp['duracion']-7:$tp['dia_in']+$tp['duracion'];
                             
                                
                              ?>
                                <tr <?=$i==0?'style="border-top: solid;"':''?>>
                                  <td><?=$n?></td>
                                  <td><?=$p['nombre']?></td>
                                  <?php for($c=1; $c<=7; $c++){ ?>
                                    <td><b><strong>
                                    <?php  
                                      $lab='';
                                      if($dia_fin<$tp['dia_in']){
                                        if(($c>=$tp['dia_in'])||($c<$dia_fin)){ $lab='X'; $tot[$c]++;}  
                                      }else{
                                        if(($c>=$tp['dia_in'])&&($c<$dia_fin)){ $lab='X'; $tot[$c]++;}  
                                      }
                                      echo $lab;
                                      ?>
                                    </strong></b></td>
                                  <?php } ?>
                                  <td><?=$p['cargo']?> [<?= ucwords(substr($p['compania'], 0,3))?>]</td>
                                  <?php if ($i==0){ ?>
                                    <td rowspan="<?=$row?>" class="text-center align-middle"><?php 
                                      $hora_in=new DateTime(nice_date($t['hora_in'], 'H:i')); 
                                      $int='PT'.($t['jornada']+$t['alm']).'H'; ?>
                                      <strong><?php echo $hora_in->format('h:i a'); ?></strong>
                                      <br>A<br>
                                      <strong>
                                        <?php $hora_in->add(new DateInterval($int)); 
                                        echo $hora_in->format('h:i a'); ?>
                                      </strong>
                                    </td>
                                  <?php }
                                  $i++; ?>  
                                </tr>
                              <?php  if ($i==$row){ $i=0; ?>
                                <tr class="border-1">
                                  <th colspan="2" class="text-right" >Personal de Guardia</th>
                                  <?php for($c=1; $c<=7; $c++){ ?>
                                    <td><b><strong><?=$tot[$c]?></strong></b></td>
                                  <?php } ?>
                                  <td></td>
                                  <td></td>
                                </tr>
                              <?php }  
                              $n++; 
                            }
                          }
                        }?>
                </tbody>
              </table>
              <span>(<strong><b>X</b></strong>) Dia Laboral
             </div>
            <div class="card-footer d-flex justify-content-between">
              <button id="print" class="btn btn-danger">
                <span class="fa fa-print"></span>
              </button>
            </div>

          </div>
        </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
</div>
<div class="modal" id="info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Asignar Camion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
        <?= form_open('plansemana/set_auto', ['id'=>'asig_auto', 'class'=>'col-12']); ?>
        <input type="hidden" id="day" name="day">
        <input type="hidden" id="id_usuario" name="id_usuario" value="<?=$idUser?>">
        <label for="id_auto">Flota</label>
        <select name="id_auto[]" id="id_auto" class="col-11 form-control form-control-sm s2" style="width: 100%" multiple>    
         <?php  foreach ($this->vehiculo->read(['estatus !='=>5], true, ['order_by'=>['length(flota)'=>'asc', 'flota'=>'asc']]) as $item): 
              $this->db->select('COUNT(*) total')
                  ->from('ordenes o')
                  ->where('o.id_auto', $item['id_auto'])
                  ->where('o.tipo !=', 'PERDIDA TOTAL')
                  ->where_not_in('o.estatus', ['CERRADA','ANULADA','FACTURADA'])
                  ->order_by('fecha_in','desc')
                  ->limit(1);
              $total = $this->db->get()->row_array();
              $act=$total['total']>0?0:1;
              echo '<option value="'.$item['id_auto'].'" '; 
              echo $act==0?'disabled':''; 
              echo '>';
              echo 'Flota '.$item['flota'].' - Placa '.$item['placa'] .' / '. $item['marca'] .' '. $item['modelo'];
              echo '</option>';
          endforeach; ?>
        </select>
      </form>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="sendButton" data-id="" data-dismiss="modal" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<script>

  $('.btnSel').on('click', function() {
    var dia=$(this).attr('data-id');
    //alert(dia);
    $('#day').val(dia);
    $.get('<?= site_url('plansemana/read_flota/') ?>'+dia, null, function(data, textStatus, xhr) {
      $('select[name="id_auto"]').html(data);
    });
    $('#sendButton').attr('data-id', dia);
  });

  $('.iconDay').on('click', function() {
    var dia=$(this).attr('data-day');
    var id=$(this).attr('data-id');  
    $.get('<?= site_url('plansemana/unset_auto/') ?>'+id, {delete: true}, function(data) {
      $('#list_'+dia).load('<?= site_url('plansemana/read_autos/') ?>'+dia,function(data) {
        $('#list_'+dia).html(data);
      });
    });
  });

  $('#sendButton').on('click', function() {
    var dia=$(this).attr('data-id');
    //alert(dia);
    send_form_load('#asig_auto','#list_'+dia,'<?= site_url('plansemana/read_autos/') ?>'+dia);
    $('#asig_auto').submit();
    $('#asig_auto').reset();
  });
</script>