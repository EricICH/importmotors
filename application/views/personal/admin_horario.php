<?php 
    $idUser=$this->usuario->id_user();
?>
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Listado</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md">
      <div class="card">
        <div class="card-header text-uppercase">
          Horario Personal Almacen
          <div class="card-actions">
            <?php if ($this->uri->segment(3)=='alert-plan'): ?>
              <div class="">
                <button id="print" class="btn btn-danger">
                  <span class="fa fa-print"></span>
                </button>
              </div>
            <?php endif ?>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0 printableArea">
          <div >
          <table class="table table-sm table-striped table-bordered">
            <thead class="text-center">
              <tr>
                  <th width="10%">Turno/Dia</th>
                <?php for($i=1; $i<=7; $i++){ ?>
                  <th width="12%" class="border"><b><strong><?=$day[$i]?></strong></b></th>
                <?php } ?>
              </tr>
            </thead>
            <tbody id="main_table">
              <?php foreach($turnos as $t){ ?>
              <tr>
                <?php 
                $hora_in=new DateTime(nice_date($t['hora_in'], 'H:i')); 
                $int='PT'.($t['jornada']+$t['alm']).'H';
                ?>
                <th class="text-center "><strong><?php echo $hora_in->format('h:i a'); ?></strong> <strong><?php 
                $hora_in->add(new DateInterval($int));
                echo $hora_in->format('h:i a');
                ?></strong></th>
                <?php for($i=1; $i<=7; $i++){ ?>
                  <td class="border">
                
                <?php foreach($turn as $tp){
                    $dia_fin=$tp['dia_in']+$tp['duracion']>7?$tp['dia_in']+$tp['duracion']-7:$tp['dia_in']+$tp['duracion'];
                    $lab='n';
                      if($dia_fin<$tp['dia_in']){
                        if(($i>=$tp['dia_in'])||($i<$dia_fin)){ $lab='s'; }  
                      }else{
                        if(($i>=$tp['dia_in'])&&($i<$dia_fin)){ $lab='s'; }  
                      }
                      
                      $turno=$tp['turno'];
                      if($i>5){ $turno=2; }
                      if ($t['turno']==$turno){ 
                        if ($lab=='s'){  ?>
                          <div id="list_">
                            <span class="badge"><?=$tp['turno'].'-'.$tp['tabla']?></span><br>
                          </div>
                  <?php }
                      }
                    } ?>
                  </td>
                <?php } ?>
              </tr>
            <?php } ?>
            </tbody>
          </table>
         </div>
            <?= form_open('personal/edit_turno', ['id'=>'edit', 'class'=>'col-12'],['acc'=>'edit']); ?>
               <div class="form-row">
              <div class="col col-4">
                <label for="id_turno">Turno</label>
                <select name="id_turno" id="id_turno">
                  <option value="">Ninguno</option>
                  <?php foreach($turn as $tp){ ?>
                    <option value="<?=$tp['id_turno']?>"><?=$tp['turno']?>-<?=$tp['tabla']?></option>
                  <?php }?> 
                </select>
              </div>
              <div class="col col-4">
                <label for="dia">Dia Inicio</label>
                <select name="dia" id="dia" required="">
                  <option value="">N/A</option>
                <?php for($i=1; $i<=7; $i++){ ?>
                  <option value="<?=$i?>"><?=$day[$i]?></option>
                <?php } ?>
              </select>
              </div>
              <div class="col col-3">
                <label for="hora">hora Inicio</label>
                <select name="hora" id="hora" required>
                  <option value="">N/A</option>
                <?php for($i=0; $i<=23; $i++){ 
                  $hora=zerofill($i,2).':00';

                  ?>
                  <option value="<?=nice_date($hora,'H:i')?>"><?=nice_date($hora,'h:i a')?></option>
                <?php } ?>
              </select>
              </div>
              <div class="col col-1">
                <button class="btn btn-sm btn-info switch" style="display: none;" type="submit"><i class="fas fa-save"></i></button>
              </div>
              </div>
            <?=form_close()?>


        </div>
          <div class="card-footer d-flex justify-content-between">
            <button id="print" class="btn btn-danger">
              <span class="fa fa-print"></span>
            </button>
          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
</div>
<div class="modal" id="info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Crear Horario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
        <?= form_open('personal/admin_horario', ['id'=>'create', 'class'=>'col-12']); ?>
       
      </form>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="sendButton" data-id="" data-dismiss="modal" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<script>
  send_form_load('#edit','#main_table','<?= site_url('personal/read_turnos/') ?>');
  $('.btnSel').on('click', function() {
    var dia=$(this).attr('data-id');
    //alert(dia);
    $('#day').val(dia);
    $.get('<?= site_url('plansemana/read_flota/') ?>'+dia, null, function(data, textStatus, xhr) {
      $('select[name="id_auto"]').html(data);
    });
    $('#sendButton').attr('data-id', dia);
  });

  $(function() {
    
    $('select[name="id_turno"]').on('change', function() {
      $('select[name="id_turno"] option:selected').each(function() {
        var turno = $(this).val();
        //alert(turno);
        if(turno!=''){
          $('.switch').show();
        }else{
          $('.switch').hide();
        }
        $.post('<?= site_url('personal/option_turno') ?>', {turno: turno}, function(data, textStatus, xhr) {
          $('select[name="dia"]').html(data);
        });

        $.post('<?= site_url('personal/option_hora') ?>', {turno: turno}, function(data, textStatus, xhr) {
          $('select[name="hora"]').html(data);
        });
      });
    });


  });
</script>