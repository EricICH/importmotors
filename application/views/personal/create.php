<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Nuevo Usuario</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('falla') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1) . '/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
        <a href="<?= site_url($this->uri->segment(1) . '/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list"></i>
        </a>
        <div class="">
          <button
            class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
              class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row" id="validation">
    <div class="col-12">
      <div class="card wizard-content">
        <div class="card-body">
          <?= form_open(uri_string(),['class' => 'validation-wizard wizard-circle', 'id' => 'create'], ['id_taller_personal' => $edit['id_taller_personal'],'id_taller' => $taller['id_taller']]) ?>
            <h6>Datos Personales</h6>
            <section>
              <div class="form-row">
                <div class="col-md">
                  <?= form_label('<i class="fa fa-address-card text-primary"></i> Nombre') ?>
                  <?= form_input('nombre',$edit['nombre'], ['class' => 'form-control', 'required' => 'true']); ?>
                </div>
                <div class="col-md">
                  <?= form_label('<i class="fa fa-address-card text-primary"></i> Apellido'); ?>
                  <?= form_input('apellido',$edit['apellido'], ['class' => 'form-control', 'required' => 'true']); ?>
                </div>

                <div class="col-md">
                  <?= form_label('<i class="fa fa-hashtag text-primary"></i> Cedula'); ?>
                  <?= form_input('ci',$edit['ci'], ['class' => 'form-control', 'required' => 'true']); ?>
                </div>
                <div class="w-100 mt-2"></div>
            </section>

            <h6>Datos Laborales</h6>
            <section>
              <div class="row">
                <div class="col-md">
                  <?= form_label('<i class="fa fa-user-tie text-primary"></i> Dependencia'); ?>
                  <select name="dependencia" class="form-control">
                      <option value="0" selected>Ninguno</option>
                        <?php $dependencia= $this->taller->read(); ?>
                      <?php foreach ($dependencia as $item): ?>
                      <option  value="<?= $item['id_taller'] ?>"><?= $item['nombre'] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>

                <div class="col-md">
                  <?= form_label('<i class="fa fa-id-badge text-primary"></i> Cargo'); ?>
                  <?= form_input('cargo',$edit['cargo'], ['class' => 'form-control']); ?>
                </div>
                 <div class="col-md" style="padding-top: 35px;">
                  <input type="checkbox" id="jefe" name="jefe_taller" value="1" <?=$edit['jefe_taller']==1?'checked':''?>> 
                  <label for="jefe" ><small>Jefe Taller</small></label>
                </div>
                <div class="w-100 mt-2"></div>
              </div>
            </section>

            <h6>Otros</h6>
            <section>
              <div class="form-row">
                <div class="col-md">
                  <?= form_label('Direccion'); ?>
                  <textarea name="direccion" rows="3" class="form-control"><?=$edit['direccion']?></textarea>
                </div>
                <div class="w-100 mt-2"></div>
                <div class="col-md">
                  <?= form_label('Telefono'); ?>
                  <?= form_input('telefono',$edit['telefono'], ['class' => 'form-control']); ?>
                </div>
                <div class="col-md">
                  <?= form_label('Email'); ?>
                  <?= form_input('email',$edit['email'],['class' => 'form-control']) ?>
                </div>
                <div class="w-100 mt-2"></div>
                <div class="col-md">
                  <?= form_label('Observacion'); ?>
                  <textarea name="observaciones" rows="3" class="form-control"><?=$edit['observaciones']?></textarea>
                </div>
                <div class="w-100 mt-2"></div>
              </div>
            </section>
          </form>
        </div>
      </div>
    </div>
  </div>
