<?php $read=permite_btn($this->session->Permisos,strtolower($this->uri->segment(1).'/read'));?> 
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Ver</li>
      </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center">
      <div class="d-flex m-t-10 justify-content-end">
        <a href="<?= site_url('cliente') ?>" class="btn btn-sm btn-danger mr-1">
          <i class="fa fa-home"></i>
        </a>
        <?php if($read==2){ ?>
        <a href="<?= site_url($this->uri->segment(1).'/create') ?>" class="btn btn-sm btn-primary mr-1">
          <i class="fa fa-plus"></i>
        </a>
      <?php } ?>
        <a href="<?= site_url($this->uri->segment(1).'/read') ?>" class="btn btn-sm btn-info">
          <i class="fa fa-list"></i>
        </a>
        <div class="">
          <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header text-uppercase">
          Listado
          <div class="card-actions">
            <a class="btn-minimize" href="javascript:void(0);">
              <i class="fa fa-filter"></i>
            </a>
          </div>
        </div>
        <div class="card-body p-0">
          <div id="accordionexample" class="accordion" role="tablist" aria-multiselectable="false">
            <?php $n = 1 ?>
            <?php foreach ($list as $item): ?>
              <?php $secs=$this->modulo->read(['where'=>['m.id_parent'=>$item['id_modulo-data']]],true,['order_by'=>['order','asc']]); ?>
              <?php if(count($secs)>0): ?>
                <div class="card">  
                  <div class="card-header" role="tab" id="headingOne">
                    <h5 class="mb-0">
                      <i class="fas fa-genderless text-<?=$item['is_active']==1?'success':'danger'?>"></i>
                      <a data-toggle="collapse" data-parent="#accordionexample" href="#C<?= $item['id_modulo-data'] ?>" aria-expanded="true" aria-controls="collapseexaOne">
                        <?= $item['name'] ?>
                      </a>
                      <small>
                        <button 
                          data-toggle="modal" 
                          data-target="#edit_mod" 
                          data-n="<?= $item['name'] ?>" 
                          data-o="<?= $item['order'] ?>" 
                          data-p="<?= $item['id_parent'] ?>" 
                          data-a="<?= $item['is_active'] ?>" 
                          data-id="<?= $item['id_modulo-data'] ?>"  
                          data-i="<?= $item['icon'] ?>"
                          class="btn btn-sm border-0 p-1"
                        >
                          <i data-title="Editar" data-toggle="tooltip" class="fas fa-edit text-secondary"></i>
                        </button>
                      </small>
                    </h5>

                  </div>
                  <div id="C<?= $item['id_modulo-data'] ?>" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="card-body table-responsive">
                      <table class="table table-sm table-striped table-hover">
                        <tbody>
                          <?php foreach($secs as $item2): ?>
                          <tr>
                            <td width="5%"></td>
                            <td>
                              <i class="fas fa-genderless text-<?=$item2['is_active']==1?'success':'danger'?>"></i>
                              <?= $item2['name'] ?>
                            </td>
                            <td>
                              <small>
                                <button data-toggle="modal" 
                                data-n="<?= $item2['name'] ?>" 
                                data-o="<?= $item2['order'] ?>" 
                                data-p="<?= $item2['id_parent'] ?>" 
                                data-a="<?= $item2['is_active'] ?>" 
                                data-id="<?= $item2['id_modulo-data'] ?>" 
                                data-target="#edit_temp"  class="btn btn-sm btn-primary">
                                  <i data-title="Editar" data-toggle="tooltip" class="fas fa-edit"></i>
                                </button>
                              </small>
                            </td>
                          </tr>
                          <?php endforeach ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
              <?php $n++ ?>
            <?php endforeach ?>
          </div>
        </div>
        <div class="card-footer d-flex justify-content-center">
          <?= $this->pagination->create_links(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal data-pass" id="edit_temp">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Modal title</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          <span class="sr-only">Cerrar</span>
        </button>
      </div>
      <div class="modal-body">
        <?= form_open('admin/create_mod?s=modulo',['id'=>'edit_form']); ?>
          <div class="form-row">
            <div class="col">
              <label>Titulo</label>
              <input hidden name="id_modulo-data" id="id_modulo-data" value="">
              <input type="text" name="name" class="form-control form-control-sm" id="name">
            </div>
            <div class="w-100 mt-3"></div>
            <div class="col">
              <label class="w-100">Orden</label>
              <input type="number" name="order" min="1" class="form-control form-control-sm w-50" id="order" step="1">
            </div>
            <div class="col">
              <label>Parent</label>
              <select name="id_parent" class="form-control form-control-sm" id="id_parent">
                <?php foreach ($list as $item): ?>
                  <option value="<?=$item['id_modulo-data']?>"><?=$item['name']?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col">
              <label class="w-100">Activo</label>
              <div class="switch">
                <label>No
                  <input type="checkbox" id="is_active" name="is_active">
                  <span class="lever"></span>Si
                </label>
              </div>
            </div>
            
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
          <i class="fa fa-times"></i> Cerrar
        </button>
        <button type="button" class="btn btn-primary" id="asig_btn">
          <i class="fa fa-save"></i> Guardar
        </button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<div class="modal data-pass" id="edit_mod">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Modal title</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          <span class="sr-only">Cerrar</span>
        </button>
      </div>
      <div class="modal-body">
        <?= form_open('admin/create_mod?s=modulo',['id'=>'editC_form']); ?>
          <div class="form-row">
            <div class="col">
              <label>Titulo</label>
              <input hidden name="id_modulo-data" id="id_modulo-data" value="">
              <input type="text" name="name" class="form-control form-control-sm" id="name">
            </div>
            <div class="w-100 mt-3"></div>
            <div class="col">
              <label class="w-100">Orden</label>
              <input type="number" name="order" min="1" class="form-control form-control-sm w-50" id="order" step="1">
            </div>
            
            <div class="col">
              <label>Icon Class</label>
              <input type="text" name="icon" id="icon" class="form-control form-control-sm" value="">
            </div>
            <div class="col">
              <label class="w-100">Activo</label>
             <div class="switch">
              <label>No
                <input type="checkbox" id="is_active" name="is_active">
                <span class="lever"></span>Si
              </label>
             </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
          <i class="fa fa-times"></i> Cerrar
        </button>
        <button type="button" class="btn btn-primary" id="asig_btn2">
          <i class="fa fa-save"></i> Guardar
        </button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<script>
  button_submit('#edit_form','#asig_btn');
  button_submit('#editC_form','#asig_btn2');
  $('.data-pass').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var n = button.data('n');
    var o = button.data('o');
    var p = button.data('p');
    var a = button.data('a');
    var i = button.data('i');
    var check = false;
    var modal = $(this)
    modal.find('.modal-title').text('Ajuste de MODULO ')
    modal.find('.modal-body #id_modulo-data').val(id)
    modal.find('.modal-body #id_parent').val(p)
    modal.find('.modal-body #name').val(n)
    modal.find('.modal-body #order').val(o)
    modal.find('.modal-body #icon').val(i)    
    if(a==1){ check=true; }    
    modal.find('.modal-body #is_active').prop('checked',check)
    
  });

</script>