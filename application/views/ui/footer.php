</div>

  <!-- Footer -->

  <footer class="footer">
    <div class="container">
      <div class="row">
        <div class="col text-center">
          <div class="footer_logo"><a href="#">Impormotors</a></div>
          <nav class="footer_nav">
            <ul>
              
              <li><a href="categories.html">Modelo</a></li>
          
            </ul>
          </nav>
          <div class="footer_social">
            <ul>
              <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-reddit-alien" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            </ul>
          </div>
          <div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados<i aria-hidden="true"></i> by <a href="https://MesterVenom.com" target="_blank">Impormotors</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
        </div>
      </div>
    </div>
  </footer>
</div>

<script src="<?=base_url('js/')?>jquery-3.2.1.min.js"></script>
<script src="<?=base_url('core/css')?>/bootstrap4/popper.js"></script>
<script src="<?=base_url('core/css')?>/bootstrap4/bootstrap.min.js"></script>
<script src="<?=base_url('plugins')?>/easing/easing.js"></script>
<script src="<?=base_url('plugins')?>/parallax-js-master/parallax.min.js"></script>
<script src="<?=base_url('plugins')?>/Isotope/isotope.pkgd.min.js"></script>
<script src="<?=base_url('plugins')?>/malihu-custom-scrollbar/jquery.mCustomScrollbar.js"></script>
<script src="<?=base_url('plugins')?>/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
<script src="<?=base_url('js/')?>categories_custom.js"></script>
</body>
</html>