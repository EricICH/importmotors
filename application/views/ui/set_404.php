<?php
  $n_tot = count($this->medicion->read());

  $est['optimo'] = 0;
  $est['medio'] = 0;
  $est['critico'] = 0;
  foreach ($this->medicion->read() as $item) {
    $val = calc_cant($item['promedio'],'4-12');
    if ($val==1) { $est['optimo']++; }
    if ($val==2) { $est['medio']++; }
    if ($val==3) { $est['critico']++;}
  }
?>
<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor"><?= ucfirst($this->uri->segment(1)) ?></h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $this->app['title'] ?></a></li>
        <li class="breadcrumb-item active">Modulo en proceso de migracion</li>
      </ol>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <h1 class="display-4">Migrando data... <i class="text-success fa fa-spinner fa-spin"></i></h1>
          <p class="lead">El modulo solicitado, se encuentra en proceso de migracion, el mismo estara disponible al terminar el proceso</p>
        </div>
      </div>
    </div>
  </div>
</div>