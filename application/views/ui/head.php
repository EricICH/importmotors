<?php $title = implode(' | ', $this->uri->segment_array()) ?>
<?php 
setlocale(LC_ALL, 'spanish');
$this->app['title'] = 'Importmotors';
$this->app['subtitle'] = '';
$this->app['version'] = '1.0';
$this->hex = [
  'primary' => '#007bff',
  'secondary' => '#6c757d',
  'info' => '#17a2b8',
  'success' => '#28a745',
  'warning' => '#ffc107',
  'danger' => '#dc3545',
  'black' => '#000000',
  'gray dark' => '#343a40',
  'gray' => '#adb5bd',
  'light' => '#1f2d3d',
  'indigo' => '#6610f2',
  'navy' => '#001f3f',
  'purple' => '#605ca8',
  'fuchsia' => '#f012be',
  'pink' => '#e83e8c',
  'maroon' => '#d81b60',
  'blood' => '#a80000',
  'orange' => '#ff851b',
  'lime' => '#01ff70',
  'teal' => '#39cccc',
  'olive' => '#3d9970',

];
$this->month = [
  '01'=>'Enero',
  '02'=>'Febrero',
  '03'=>'Marzo',
  '04'=>'Abril',
  '05'=>'Mayo',
  '06'=>'Junio',
  '07'=>'Julio',
  '08'=>'Agosto',
  '09'=>'Septiembre',
  '10'=>'Octubre',
  '11'=>'Noviembre',
  '12'=>'Diciembre'
];
$this->day = [
  '01'=>'Lunes',
  '02'=>'Martes',
  '03'=>'Miercoles',
  '04'=>'Jueves',
  '05'=>'Viernes',
  '06'=>'Sabado',
  '07'=>'Domingo'
];
    $theme='green';
  
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Immportmotors</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="ShoesStore project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?= base_url('core/css/')?>bootstrap4/bootstrap.min.css">
<link href="<?=base_url('plugins')?>/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?=base_url('plugins')?>/malihu-custom-scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?=base_url('plugins')?>/jquery-ui-1.12.1.custom/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('core/css/')?>categories.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('core/css/')?>categories_responsive.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('core/css/')?>checkout.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('core/css/')?>checkout_responsive.css">
  <!-- Favicon icon -->
  <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('core/assets/images/logo/logo-min.png')?>">

  <title>Prevenauto</title>
  <!-- Bootstrap Core CSS -->

  <!-- All Jquery -->
  <!-- ============================================================== -->
  <script src="<?= base_url('core/')?>assets/plugins/jquery/jquery.min.js"></script>
  <!-- Vector CSS -->
  <link href="<?= base_url('core/')?>assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
  <link href="<?= base_url('core/')?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
  <!-- Custom CSS -->
  <link href="<?= base_url('core/material/')?>css/style.css" rel="stylesheet">
  <!-- Select2 -->
  <!-- You can change the theme colors from here -->
  <link href="<?= base_url('core/assets/plugins/')?>datatables/media/css/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="<?= base_url('core/assets/plugins/')?>datatables-buttons/css/buttons.bootstrap4.min.css" rel="stylesheet">

  <link href="<?= base_url('core/material/')?>css/colors/<?=$theme?>.css" id="theme" rel="stylesheet">

  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="fix-header card-no-border">
  <header class="header">
    <div class="header_inner d-flex flex-row align-items-center justify-content-start">
      <div class="logo"><img src="<?=base_url('images')?>/importmotors_logo.png" height="50px"></div>
      <nav class="main_nav">
      </nav>
      <div class="header_content ml-auto">
        <div class="search header_search">
          
        </div>
        <div class="shopping">
          <!-- Cart -->
          
          <!-- Star -->
          
          <!-- Avatar -->
          
        </div>
      </div>

      <div class="burger_container d-flex flex-column align-items-center justify-content-around menu_mm"><div></div><div></div><div></div></div>
    </div>
  </header>
    <!-- Menu -->

  <div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
    <div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
    <div class="logo menu_mm"><a href="#">Impormotors</a></div>
    <div class="search">
      <form action="#">
        <input type="search" class="search_input menu_mm" required="required">
        <button type="submit" id="search_button_menu" class="search_button menu_mm"><img class="menu_mm" src="<?=base_url('images')?>/magnifying-glass.svg" alt=""></button>
      </form>
    </div>
    <nav class="menu_nav">
      <ul class="menu_mm">    
          <li><a href="<?=site_url('auto/create')?>">Agregar</a></li>
          <li><a href="<?=site_url('main/')?>">Catalogo</a></li>

      </ul>
    </nav>
  </div>

      <div class="page-wrapper">  