<link href="<?= base_url('core/material/')?>css/style.css" rel="stylesheet">
<link href="<?= base_url('core/')?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
<section id="" class="">
	<div class="p-0" style="height: 100%; width: 100%; background: url('<?=base_url('core')?>/assets/images/background/carga.jpg'); background-size: cover; opacity: 20%;    position: absolute; top: 0;left: 0;">
	</div>
	<div class="error-body text-center font-weight-bolder" style="">
		<h1 class="text-info"><i class="fa fa-cog fa-spin"></i></h1>
		<h3 class="text-uppercase text-dark" style="font-weight: bolder;">Procesando Su Solicitud por favor espere</h3>
		<p class="text-dark m-t-30 m-b-30"  style="font-weight: bolder;">No recargue ni cierre la ventana</p>
	</div>
</section>