<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LogSys_model extends CI_Model {
  
  public function create($array)
  {
    if($this->session->Parent==0){ 
        $idUser=$this->session->IdUser;
      }else{
        $idUser=$this->session->Parent;
      }
    
    $array['date']= date('Y-m-d H:i:s');
    $array['id_usuario']=$this->session->IdUser;
    $array['id_master']=$idUser;
    //var_dump($array);
    $this->db->insert('log', $array);
  }

  public function read($where=null,$ret='result',$order=null,$group=null,$limit=null)
  {
    $this->db->select('l.*, u.*');
    $this->db->from('log l');
    $this->db->join('usuarios u ', 'u.id_usuario = l.id_usuario', 'left');
    if ($order!==null) {
      foreach ($order as $key => $value) {
        $this->db->order_by($key,$value);
      }
    }else{
        $this->db->order_by('id_log','desc');
    }

    if ($group!==null) {
      foreach ($group as $key => $value) {
        $this->db->group_by($value);
      }
    }
    if ($limit!==null) {
      foreach ($limit as $key => $value) {
        $this->db->limit($key,$value);
      }
    }
    if ($this->session->Root !== true) {
      if ($this->session->Parent != 0) {
        $this->db->group_start();
        $this->db->where('l.id_master', $this->session->IdUser);
        $this->db->or_where('l.id_master', $this->session->Parent);
        $this->db->group_end();
      }else{
         $this->db->where('l.id_master', $this->session->IdUser);
      }
    }
    if ($where!==null) {
      $this->db->where($where);
    }
    if ($ret=='row') {
      return $this->db->get()->row_array();
    }
    if ($ret=='result') {
      return $this->db->get()->result_array();
    }
  }

  public function update()
  {
  }

  public function delete($id)
  {
  }

}