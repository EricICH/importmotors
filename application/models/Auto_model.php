<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auto_model extends CI_Model {
  public function create()
  {
    $idUser=1;
    $data=$this->input->post();
    $data['id_user']=$idUser;
    if($data['n_brand']!=null){ 
      $this->db->insert('brands', ['brand'=>$data['n_brand'], 'id_user'=>1]);  
      $data['brand']=$this->db->insert_id();
    }
    unset($data['n_brand']);
    if($data['n_model']!=null){ 
        $this->db->insert('models', ['model'=>$data['n_model'], 'id_brand'=>$data['brand']]);  
        $data['model']=$this->db->insert_id();
    }
    unset($data['n_model']);
    unset($data['id_auto']);
   
    if (isset($_FILES['pic']['name']) && !empty($_FILES['pic']['name'])) {
     $imagen=$this->extra->img_encode($_FILES['pic']['tmp_name'], $_FILES['lic']['type']);
     $data['pic']= $imagen;
    }
    //var_dump($post);
    $this->db->insert('autos', $data);
    $last= $this->db->insert_id();
    return $last['id_auto'];

  }

  public function add_foto($post)
  {
    $this->db->insert('autos_fotos', $post);
    return true;
  }

  public function update_foto($id,$data)
  {
    $this->db->where('id_auto_foto', $id);
    $this->db->update('autos_fotos',['foto'=> $data, 'fecha'=>date('Y-m-d')]);
    $this->vehiculo->delete_foto($id);
  }

  public function insDoc()
  {
    $post=$this->input->post();
    $post['id_usuario']=$this->usuario->id_user();
    if (isset($_FILES['doc']['name']) && !empty($_FILES['doc']['name'])) {
      $post['doc']=$this->extra->img_encode($_FILES['doc']['tmp_name'], $_FILES['doc']['type']);
    }
    $this->db->insert('autos_documentos', $post);
    return true;
  }

  
  public function read($where=null,$ret=true,$attrs=[],$join=null,$fields=['au.*','b.*','m.*'])
  {
    $table='autos au';
    $conditions=$where==null?[]:((is_array($where)&&!isset($where['where']))?['where'=>$where]:$where);
    $join['brands b']='b.id_brand = au.brand';
    $join['models m']='m.id_model = au.model';
    
   
    if(!isset($attrs['order_by'])){
      $attrs['order_by']=['au.id_auto'=>'asc'];
    }
    if(!isset($attrs['group_by'])){
      $attrs['group_by']=['au.id_auto'];
    }
    $query=$this->db->custom_query($fields,$table,$join,$conditions,$ret,$attrs);
    //print_r($this->db->last_query());
    return $query;
    
  }
  public function update($id)
  {
    $post = $this->input->post();
    
    $this->db->where('id_auto', $id);
    $this->db->update('autos', $post);
    return $id;
  }

  public function delete($id)
  {
    $post=$this->input->post();
    $data=[
      'salida_motivo' => $post['salida_motivo'],
      'salida_fecha' => date('Y-m-d'),
      'condicion'   => $post['condicion'],
      'estatus' => 5
    ];
    $item=$this->vehiculo->read(['au.id_auto'=>$id],false);
    $this->db->update('autos', $data,['id_auto'=>$id]);
  }

  public function delauto($id)
  {
    $this->db->delete('autos',['id_auto'=>$id]);
    $this->logSys->create(['controller'=>'vehiculo', 'action'=>'Elimina Vehiculo: '.@$item['flota']]);
  }

  public function delete_foto($id)
  {
    $this->db->delete('autos_fotos',['id_auto_foto'=>$id]);
  }
  
}