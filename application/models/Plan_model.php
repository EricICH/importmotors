<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan_model extends CI_Model {
  public function create()
  {
    if($this->session->Parent==0){ 
        $idUser=$this->session->IdUser;
      }else{
        $idUser=$this->session->Parent;
      }
    $post = $this->input->post();
    $dif['tiempo'] = $post['tiempo']-$post['rango_min_t'];
    $dif['kilometraje'] = $post['kilometraje']-$post['rango_min_k'];
    $data = [
      'id_usuario'=>$idUser,
      'titulo'=>$post['titulo'],
      'descripcion'=>$post['descripcion'],
      'tiempo'=>$post['tiempo'],
      'kilometraje'=>$post['kilometraje'],
      'rango_min_t'=>$post['rango_min_t'],
      'rango_min_k'=>$post['rango_min_k'],
      'rango_max_t'=>$post['tiempo']+$dif['tiempo'],
      'rango_max_k'=>$post['kilometraje']+$dif['kilometraje']
    ];

    $this->logSys->create(['controller'=>'Plan', 'action'=>'Creo nuevo plan de mantenimiento: '.$post['titulo']]);
    return $this->db->insert('plan_mantenimiento', $data);
  }


   public function read($conditions=[],$ret=true,$attrs=[],$join=null,$fields=['pm.*'])
  {
    
    $table='plan_mantenimiento pm';
    $conditions=$conditions==null?[]:$conditions;
    $conditions['where']['pm.id_usuario']=$this->usuario->id_user();
    $query=$this->db->custom_query($fields,$table,$join,$conditions,$ret,$attrs);
    
    return $query;
  }

   
  public function read_config($conditions=[],$ret=true,$attrs=[],$join=null,$fields=['pg.*'])
  {
   // var_dump($attrs);
    $table='plan_config pg';
    $conditions=$conditions==null?[]:$conditions;
    //$conditions['where']['pm.id_usuario']=$this->usuario->id_user();
    //$join['autos a']='a.id_auto = pm.id_auto';
    $query=$this->db->custom_query($fields,$table,$join,$conditions,$ret,$attrs);
    //print_r($this->db->last_query());
    return $query;
  }
  
  public function update($id,$array)
  {
    
    $item=$this->plan->read(['where'=>['id_plan'=>$id]],false);
    $this->logSys->create(['controller'=>'Plan Mantenimiento', 'action'=>'Modifico plan de mantenimiento: '.$item['titulo'],'newVal'=>json_encode([$array]), 'lastVal'=>json_encode($item)]);
    $this->db->where('id_plan', $id);
    $this->db->update('plan_mantenimiento', $array);
   
  }

  public function delete()
  {
    # code...
  }
 
  public function read_suministro($where=null,$ret='result',$order=null,$group=null,$limit=null)
  {
    $this->db->select('pms.*,i.*');
    $this->db->from('plan_mantenimiento_suministro pms');
    $this->db->join('inventario i', 'i.id_inventario = pms.id_inventario', 'left');
    if ($order!==null) {
      foreach ($order as $key => $value) {
        $this->db->order_by($key,$value);
      }
    }
    if ($group!==null) {
      foreach ($group as $key => $value) {
        $this->db->group_by($value);
      }
    }
    if ($limit!==null) {
      foreach ($limit as $key => $value) {
        $this->db->limit($key,$value);
      }
    }
    if ($where!==null) {
      $this->db->where($where);
    }
    if ($this->session->Root !== true) {
      if ($this->session->Parent != 0) {
        $this->db->group_start();
        $this->db->where('pms.id_usuario', $this->session->IdUser);
        $this->db->or_where('pms.id_usuario', $this->session->Parent);
        $this->db->group_end();
      }else{
         $this->db->where('pms.id_usuario', $this->session->IdUser);
      }
    }
    if ($ret=='row') {
      return $this->db->get()->row_array();
    }
    if ($ret=='result') {
      return $this->db->get()->result_array();
    }
  }
  
  public function deletesum($id){
    $this->db->where('id_plan', $id);
    $this->db->delete('plan_mantenimiento_suministro');
  }

  public function agregasum($id,$array)
  {
      //echo $id." var post".var_dump($array);
    $i=0;
    $IDU=$this->usuario->id_user();
    foreach ($array['id_inventario'] as $id_inventario) {
      $datPart = [
        'cantidad'      => $array['cantidad'][$i],
        'id_inventario' => $id_inventario,
        'id_usuario'    => $IDU,
        'id_plan'       => $id
      ];
      $this->db->insert("plan_mantenimiento_suministro", $datPart);          
      $i++;
    }
  }

  public function set_config()
  {
    $idUser=$this->usuario->id_user();
    
    $post = $this->input->post();
    $data = [
      'title'=>$post['title'],
      'id_plan'=>$post['id_plan'],
      'desc'=>$post['desc']
    ];
    $this->db->insert('plan_config', $data);
    $new_conf=$this->db->insert_id();
    foreach ($post['units'] as $u) {
      $this->db->insert('plan_autos', [ 'id_auto'=>$u, 'id_pconfig'=>$new_conf]);

    }
    foreach ($this->cart->contents() as $item) {
      $data=[ 
        'id_usuario'    =>  $this->usuario->id_user(),
        'id_inventario' =>  $item['id'],
        'id_plan'       =>  $new_conf,
        'cantidad'      =>  $item['qty'],
        'id_modelo'     =>  $post['modelo']
      ];
      $this->db->insert('plan_mantenimiento_suministro', $data);
    }
    $this->cart->destroy();
    $plan=$this->plan->read(['id_plan'=>$post['id_plan']],'row');
    $this->logSys->create(['controller'=>'Plan', 'action'=>'Creo nueva configuracion '.$post['title'].' para plan '.$plan['titulo']]);
  }

  public function del_config($id){
    $this->db->delete('plan_config', ['id_pconfig'=>$id]);
    $this->db->delete('plan_autos', ['id_pconfig'=>$id]);
    $this->db->delete('plan_mantenimiento_suministro', ['id_plan'=>$id]);
  }

}