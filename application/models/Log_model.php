<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_model extends CI_Model {
  
  public function create($array)
  {
    $array['fecha']=date('Y-m-d h:i:s');
    $array['id_usuario']=$this->session->IdUser;
    $array['id_master']=$this->session->Parent;
    var_dump($array);
    $this->db->insert('log', $array);
  }

  public function read($where=null,$ret='result',$order=null,$group=null,$limit=null)
  {
    $this->db->select('lo.*');
    $this->db->from('log lo');
    if ($order!==null) {
      foreach ($order as $key => $value) {
        $this->db->order_by($key,$value);
      }
    }
    if ($group!==null) {
      foreach ($group as $key => $value) {
        $this->db->group_by($value);
      }
    }
    if ($limit!==null) {
      foreach ($limit as $key => $value) {
        $this->db->limit($key,$value);
      }
    }
    if ($this->session->Root !== true) {
      if ($this->session->Parent != 0) {
        $this->db->group_start();
        $this->db->where('fa.id_usuario', $this->session->IdUser);
        $this->db->or_where('fa.id_usuario', $this->session->Parent);
        $this->db->group_end();
      }else{
         $this->db->where('fa.id_usuario', $this->session->IdUser);
      }
    }
    if ($where!==null) {
      $this->db->where($where);
    }
    if ($ret=='row') {
      return $this->db->get()->row_array();
    }
    if ($ret=='result') {
      return $this->db->get()->result_array();
    }
  }

  public function update()
  {
  }

  public function delete($id)
  {
  }

}