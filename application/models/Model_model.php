<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_model extends CI_Model {
  public function create()
  {
    # code...
  }

  public function read($conditions=[],$ret=true,$attrs=[],$join=null,$fields=['b.*','m.*'])
  {
   // var_dump($attrs);
    $table='brands b';
    $conditions=$conditions==null?[]:$conditions;
    $join['models as m']=['m.id_brand = b.id_brand'=>'left'];
    $query=$this->db->custom_query($fields,$table,$join,$conditions,$ret,$attrs);
    //print_r($this->db->last_query());
    return $query;
  }


  
  public function create_brand()
  {
    $post = $this->input->post();
    $idUser=$this->user->id_user();
    $data = [
      'id_user' => $idUser,
      'brand' => $post['brand']
    ];
    $this->db->insert('brands', $data);

    //$this->logSys->create(['controller'=>'auto', 'action'=>'Registro nueva brand : <strong>'.$post['brand'].'</strong>']);
  }

  public function create_model()
  {
    $post = $this->input->post();
    $idUser=$this->user->id_user();
    $data = [
      'id_brand' => $post['id_brand'],
      'model' => $post['model']
    ];
    $this->db->insert('models', $data);
    //$item=$this->auto->read(['plate'=>$form['plate']],false);   
    //$this->logSys->create(['controller'=>'auto', 'action'=>'Registro nuevo Modelo : <strong>'.$post['model'].'</strong>']);
  }


  public function delete()
  {
    # code...
  }
}