<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal_model extends CI_Model {
  public function create()
  {
    $form = $this->input->post();
    $form['id_usuario']=$this->usuario->id_user();
    if(!isset($form['dependencia'])||$form['dependencia']!=null){
        $form['dependencia']=0;
    }
    $form['fecha_in']=date('Y-m-d');
    $form['estatus']=1;
   
    $this->db->insert('talleres_personal', $form);
  }

   public function read($where=[],$ret=true,$attrs=[],$join=null,$fields=['tp.*'])
  {
   // var_dump($attrs);
    if($ret==='row'){ $ret=false;}
    if($ret==='result'){ $ret=true;}
    $table='talleres_personal tp';
    $conditions=$where==null?[]:((is_array($where)&&!isset($where['where']))?['where'=>$where]:$where);
    $conditions['where']['tp.id_usuario']=$this->usuario->id_user();
    //$join['autos a']='a.id_auto = pm.id_auto';
    $query=$this->db->custom_query($fields,$table,$join,$conditions,$ret,$attrs);
    //print_r($this->db->last_query());
    return $query;
  }

  public function personal_almacen($conditions=[],$ret=true,$attrs=[],$join=null,$fields=['p.*'])
  {
   // var_dump($attrs);
    $table='personal_almacen p';
    $conditions=$conditions==null?[]:$conditions;
    $conditions['where']['p.id_usuario']=$this->usuario->id_user();
    //$join['autos a']='a.id_auto = pm.id_auto';
    $query=$this->db->custom_query($fields,$table,$join,$conditions,$ret,$attrs);
    //print_r($this->db->last_query());
    return $query;

  }

 public function turnos($conditions=[],$ret=true,$attrs=[],$join=null,$fields=['t.*'])
  {
   // var_dump($attrs);
    $table='turno t';
    $conditions=$conditions==null?[]:$conditions;
    $attrs=$attrs==null?['order_by'=>['t.turno','ASC']]:$attrs;
    $conditions['where']['t.id_usuario']=$this->usuario->id_user();
    //$join['autos a']='a.id_auto = pm.id_auto';
    $query=$this->db->custom_query($fields,$table,$join,$conditions,$ret,$attrs);
    //print_r($this->db->last_query());
    return $query;
  }


  public function update()
  {
    $form = $this->input->post();

    if(!isset($form['jefe_taller'])){ $form['jefe_taller']=0; }
    //var_dump($form);
    $this->db->update('talleres_personal', $form,['id_taller_personal'=>$form['id_taller_personal']]);
  }

  public function edit_turno()
  {
    $form = $this->input->post();
    $data = [
      'dia_in' => $form['dia'],
      'hora_in' => $form['hora']
    ];
    $this->db->update('turno', $data,['id_turno'=>$form['id_turno']]);
  }

  public function delete($id)
  {
    $this->db->delete('talleres_personal',['id_taller_personal'=>$id]);
  }
}