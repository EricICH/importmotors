<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
  public function create($array)
  {
    if(isset($array['passconf'])){ unset($array['passconf']); }
    if(isset($array['id_user'])){ unset($array['id_user']); }
    $array['id_parent']=$this->session->IdUser;
    $this->db->insert('users', $array);
    $last = $this->db->insert_id();
    return $last;
  }

  public function id_user(){
    if($this->session->Parent==0){ 
         return $this->session->IdUser;
       }else{
         return $this->session->Parent;
       }
  }

  public function read($where=null,$ret='result',$order=null,$group=null,$limit=null)
  {
    $this->db->select('u.*');
    $this->db->from('users u');
    if ($order!==null) {
      foreach ($order as $key => $value) {
        $this->db->order_by($key,$value);
      }
    }
    if ($group!==null) {
      foreach ($group as $key => $value) {
        $this->db->group_by($value);
      }
    }
    if ($limit!==null) {
      foreach ($limit as $key => $value) {
        $this->db->limit($key,$value);
      }
    }
    
    
    
    if ($where!==null) {
      $this->db->where($where);
    }
    if ($ret=='row') {
      return $this->db->get()->row_array();
    }
    if ($ret=='result') {
      return $this->db->get()->result_array();
    }
  }
  public function view($where=null)
  {
    $this->db->select('us.*');
    $this->db->from('users us');
        
    if ($where!==null) {
      $this->db->where($where);
    }
      return $this->db->get()->row_array();
    
  }

  public function read_permiso($id_user, $id_modulo=null,$parent=null){
    
    if($id_modulo!=null){  
      if($parent=='p'){
       // echo "parent";
        $where=['where'=>['id_user'=>$id_user, 'parent'=>$id_modulo]];
      }else{
        $where=['where'=>['id_user'=>$id_user, 'id_modulo'=>$id_modulo]];  
      }
      $permiso=$this->extra->read(['ac.*'],'accesos ac',null,$where,'row');
    }else{
      $where=['where'=>['id_user'=>$id_user]];
      $permiso=$this->extra->read(['ac.*'],'accesos ac',null,$where,'result',['order' => 'asc']);
    }
    
    return $permiso;
   }

   
  public function updt_permisos($id, $array){
    $i=0;
    foreach ($array['id_modulo-data'] as $id_modulo) {
      $permiso=$this->user->read_permiso($id, $id_modulo);
      if(empty($permiso)||count($permiso)==0){
        $reg=[
          'id_user'  =>   $id,
          'id_modulo'   =>  $id_modulo,
          'read'        =>  $array['read'][$id_modulo],
          'parent'      =>  $array['parent'][$id_modulo]
        ];
        $this->db->insert('accesos', $reg);
      }else{
        $this->db->where('id_user', $id);
        $this->db->where('id_modulo', $id_modulo);
        $this->db->update('accesos', ['read'=>$array['read'][$id_modulo],'parent'=>$array['parent'][$id_modulo]]);
      }
    }
  
  }

  public function login($where=null)
  {

    $this->db->select('us.*');
    $this->db->from('users us');
    $this->db->where($where);
    return $this->db->get()->row_array();
  }

  public function update($id, $data)
  {
    
    $this->db->where('id_user', $id);
    $this->db->update('users', $data);
  }

  public function delete($id)
  {
      $this->db->where('id_user', $id);
      $this->db->delete('users');
  }

  
}