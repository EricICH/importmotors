<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Extra_model extends CI_Model {


 public function read($campo,$tabla ,$join=null, $where=null,$ret='result',$order=null,$group=null,$limit=null)
  {
    
    if ($campo!==null) {
       $this->db->select(implode(",", $campo));
    }
    
    $this->db->from($tabla);
    
    if ($join!==null) {
      foreach ($join as $key => $value) {
        if(is_array($value)){
          foreach ($value as $key2 => $value2) {
            $this->db->join($key, $key2, $value2);    # code...
          }

        }else{
          $this->db->join($key, $value, 'left');
        }
        
      }
    }
    
    if ($order!==null) {
      foreach ($order as $key => $value) {
        $this->db->order_by($key,$value);
      }
    }
    if ($group!==null) {
      foreach ($group as $key => $value) {
        $this->db->group_by($value);
      }
    }
    if ($limit!==null) {
      foreach ($limit as $key => $value) {
        $this->db->limit($key,$value);
      }
    }
    if ($where!==null) {
      foreach ($where as $key => $value) {
        switch ($key) {
          case 'where':
                  $this->db->where($value);
            break;
          case 'or_where':
                  $this->db->or_where($value);
            break;
          case 'or_group':
                  $this->db->or_group_start();
                    foreach($value as $item){
                      foreach($item as $it=>$val){
                        $this->db->or_where(key($value),$val);
                      } 
                      
                    }
                  $this->db->group_end();
                  
            break;
            case 'group_or':
                    $this->db->group_start();
                      foreach($value as $item){
                        foreach($item as $it=>$val){
                          $this->db->or_where(key($value),$val);
                        } 
                        
                      }
                    $this->db->group_end();
                    
              break;
          case 'group':
                  switch($value){
                    case 'start':
                        $this->db->group_start();
                    break;
                    case 'end':
                        $this->db->group_end();
                    break;
                  }
            break;
          case 'where_in':
                foreach ($value as $key2 => $value2) {
                  $this->db->where_in($key2, $value2);
                }
          case 'or_where_in':
                 foreach ($value as $key2 => $value2) {
                    $this->db->or_where_in($key2, $value2);
                 }
          break;
          case 'where_not_in':
                 foreach ($value as $key2 => $value2) {
                    $this->db->where_not_in($key2, $value2);
                 }
          break;
          case 'like':
                    $this->db->like($value);
          break;
          case 'or_like':
                    $this->db->or_like($value);
          break;
          case 'not_like':
                    $this->db->or_not_like($value);
          break;
          case 'or_not_like':
                    $this->db->or_where_in($value); 
          break;
          default:
              $this->db->where($key);
            break;
        }
      }
    }
    if ($ret=='row') {
      return $this->db->get()->row_array();
    }
    if ($ret=='result') {
      return $this->db->get()->result_array();
    }
  }

  public function img_encode($path,$type){
    $data=file_get_contents($path);

    $base64= 'data:'.$type.';base64,'.base64_encode($data);
    return $base64;
  }

  public function file_encode($path,$type){
    $data=file_get_contents($path);

    $base64= 'data:'.$type.';base64,'.base64_encode($data);
    return $base64;
  }

  public function system_verify($sys_belong){
    $sys=$this->session->sys;
    if(in_array($sys, $sys_belong)==true){
      return true;
    }
  }

  public function upload($input,$route,$height,$types,$watermark=null) {
        $path=FCPATH.$route;
        $config['upload_path'] = $path;
        $config['encrypt_name']=true;
        $config['allowed_types'] = $types;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($input)) {
            $result = array('error' => $this->upload->display_errors());
        } else {
          $result= $this->upload->data();
          //var_dump($result);
          if($result['is_image']==TRUE):

              $image_data = $this->upload->data();
              $config['image_library'] = 'gd2';
              $config['source_image'] = $result['full_path'];
              $config['new_image'] = $path . $result['file_name'];
              $config['maintain_ratio'] = TRUE;
              $config['height'] = $height;
              if($watermark!=null){
                $config=array_merge($config,$watermark);
              }

              $this->load->library('image_lib', $config);

              /*if (!$this->image_lib->watermark()) {
                $result= $this->image_lib->display_errors();
              //  echo var_dump($result);
              }*/
              if (!$this->image_lib->resize()) {
                $result= $this->image_lib->display_errors();
                echo var_dump($result);
              }
             //unlink($result['full_path']);
          endif;   
        }
        return $result;

    
    }


}