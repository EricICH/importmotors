<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lead_model extends CI_Model {
  public function create($avatar)
  {
    $form = $this->input->post();
    $data = [
      'id_usuario' => $form['id_usuario'],
      'nombre' => $form['nombre'],
      'apellido' => $form['apellido'],
      'nacimiento' => $form['nacimiento'],
      'descripcion' => $form['descripcion'],
      'estado' => $form['estado'],
      'ciudad' => $form['ciudad'],
      'direccion' => $form['direccion'],
      'tel_casa' => $form['tel_casa'],
      'tel_oficina' => $form['tel_oficina'],
      'tel_movil' => $form['tel_movil'],
      'email' => $form['email'],
      'website' => $form['website'],
      'avatar' => $avatar,
      'referido' => $form['referido'],
      'id_origen' => $form['id_origen'],
      'id_responsable' => $form['id_responsable'],
      'id_status' => $form['id_status']
    ];
    $this->db->insert('crm_cliente', $data);
  }

  public function read($where=null,$ret='result',$order=null,$group=null,$limit=null)
  {
    $this->db->select('c.*,s.*');
    $this->db->from('crm_cliente c');
    $this->db->join('crm_cliente_status s', 's.id_status = c.id_status', 'left');
    if ($order!==null) {
      foreach ($order as $key => $value) {
        $this->db->order_by($key,$value);
      }
    }
    if ($group!==null) {
      foreach ($group as $key => $value) {
        $this->db->group_by($value);
      }
    }
    if ($limit!==null) {
      foreach ($limit as $key => $value) {
        $this->db->limit($key,$value);
      }
    }
    if ($this->session->Root !== true) {
      $this->db->where('id_usuario', $this->session->IdUser);
    }
    if ($where!==null) {
      $this->db->where($where);
    }
    if ($ret=='row') {
      return $this->db->get()->row_array();
    }
    if ($ret=='result') {
      return $this->db->get()->result_array();
    }
  }

  public function update($avatar)
  {
    $form = $this->input->post();
    $data = [
      'nombre' => $form['nombre'],
      'apellido' => $form['apellido'],
      'nacimiento' => $form['nacimiento'],
      'descripcion' => $form['descripcion'],
      'estado' => $form['estado'],
      'ciudad' => $form['ciudad'],
      'direccion' => $form['direccion'],
      'tel_casa' => $form['tel_casa'],
      'tel_oficina' => $form['tel_oficina'],
      'tel_movil' => $form['tel_movil'],
      'email' => $form['email'],
      'website' => $form['website'],
      'avatar' => $avatar,
      'referido' => $form['referido'],
      'id_origen' => $form['id_origen'],
      'id_responsable' => $form['id_responsable'],
      'id_status' => $form['id_status']
    ];
    if (empty($avatar)) {
      unset($data['avatar']);
    }
    $this->db->update('crm_cliente', $data,['id_cliente'=>$form['id_cliente']]);
  }

  public function delete()
  {
    # code...
  }
}