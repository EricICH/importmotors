<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('zerofill'))
{
  function zerofill($value,$long=0)
  {
    return str_pad($value, $long, '0', STR_PAD_LEFT);
  }
}
if ( ! function_exists('colorfill'))
{
  function colorfill($val=4,$ret='text-',$ord='desc')
  {
    if ($val==4 || $val=="" || $val == null) {
      return $ret.'white';
    }

    if ($ord=='asc') {
      if ($val==1) {
        return $ret.'success';
      }
      if ($val==2) {
        return $ret.'warning';
      }
      if ($val==3) {
        return $ret.'danger';
      }
    }
    if ($ord=='desc') {
      if ($val==3) {
        return $ret.'success';
      }
      if ($val==2) {
        return $ret.'warning';
      }
      if ($val==1) {
        return $ret.'danger';
      }
    }
  }
}