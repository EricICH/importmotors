<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('permite'))
{
  function permite($permisos,$ctrl,$read)
  {
    foreach ($permisos as $permiso) {
      if($permiso['action']==$ctrl){
        if($permiso['read']<$read){
          redirect($_SERVER['HTTP_REFERER']);
        }
      }
    }
  }
}

if ( ! function_exists('permite_btn'))
{
  function permite_btn($permisos, $ctrl)
  {
  $read=0;
    foreach ($permisos as $permiso) {
      if($permiso['action']==$ctrl){
        $read=$permiso['read'];
      }
    }
    return $read;
  }
}

if ( ! function_exists('verif_form'))
{
  function verif_form($result,$data,$dest)
  {
    if($result){
      redirect($dest,'refresh');
    }else{
      echo "ERROR EN PROCESAMIENTO<br>";
      var_dump($data);
      print_r($result);
    }
  }
}