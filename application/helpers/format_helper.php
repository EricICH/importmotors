<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('money'))
{
  function money($cant=15000,$locale='es_US')
  {
    return number_format($cant,2,',','.');
  }
}

if ( ! function_exists('sqlnum'))
{
  function sqlnum($cant=0,$locale='es_US')
  {
    return floatval(str_replace(',', '.',str_replace('.', '', $cant)));
  }
}

if ( ! function_exists('small_number'))
{
  /**
   * Formats a numbers as bytes, based on size, and adds the appropriate suffix
   *
   * @param	mixed	will be cast as int
   * @param	int
   * @return	string
   */
  function small_number($num, $precision = 1)
  {
    if ($num >= 1000000000000)
    {
      $num = round($num / 1000000000000, $precision);
      $unit = 'Bil.';
    }
    elseif ($num >= 1000000000)
    {
      $num = round($num / 1000000000, $precision);
      $unit = 'MM.';
    }
    elseif ($num >= 1000000)
    {
      $num = round($num / 1000000, $precision);
      $unit = 'Mil.';
    }
    elseif ($num >= 1000)
    {
      $num = round($num / 1000, $precision);
      $unit = 'M';
    }
    else
    {
      return number_format($num);
    }

    return number_format($num, $precision,",",".").' '.$unit;
  }
}