<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('count_days'))
{

	function count_days($inicio=null,$fin,$ret='days')
	{
		if ($inicio == null) {
			$inicio = date('Y-m-d');
		}
		$start = new DateTime($inicio);
		$end = new DateTime($fin);
		$diff = $start->diff($end);
		if ($ret=='days') {
			return $diff->days;
		}
	}
}

if ( ! function_exists('get_week'))
{
	function get_week($month=null,$year=null)
	{
	  $year = $year==null?date('Y'):$year;
	  $num_week = date("W",mktime(0,0,0,12,28,$year));
		$l_day = date('t',mktime(0,0,0,$month,1,$year));
		$f_week = date('W',mktime(0,0,0,$month,1,$year));
		$l_week = date('W',mktime(0,0,0,$month,$l_day,$year));
		$ret['first'] = $f_week;
		if ($month==12){
      $ret['last'] = ($l_week<$num_week)?$num_week:$l_week;
    }else{
      $ret['last'] = $l_week;
    }
		return $ret;
	}
}

if ( ! function_exists('alert_stock'))
{

	function alert_stock($cant=10,$min=5)
	{
		if ($cant == 0 && $min == 0) {
			return '
				<span class="text-info" data-toggle="tooltip" title="Verificar datos">
					<i class="fa fa-exclamation-triangle"></i>
				</span>
			';
		}

		$success = $min * 3;
		$warning = $min * 2;
		$danger = $min;

		if ($cant >= $success) {
			return '
				<span class="text-success" data-toggle="tooltip" title="Nivel Optimo">
					<i class="fa fa-battery-full"></i>
				</span>
			';
		}

		if ($cant >= $warning) {
			return '
				<span class="text-warning" data-toggle="tooltip" title="Nivel Medio">
					<i class="fa fa-battery-half"></i>
				</span>
			';
		}

		if ($cant < $warning) {
			return '
				<span class="text-danger" data-toggle="tooltip" title="Nivel Critico">
					<i class="fa fa-battery-quarter"></i>
				</span>
			';
		}
	}
}

if ( ! function_exists('calc_stock'))
{

	function calc_stock($cant=10,$min=5)
	{
		if ($cant == 0 && $min == 0) {
			return 4;
		}

		$success = $min * 3;
		$warning = $min * 2;
		$danger = $min;

		

		if (($cant >= $warning)&&($cant < $success)) {
			return 2;
		}

		if ($cant >= $success) {
			return 1;
		}

		if ($cant < $warning) {
			return 3;
		}
	}
}

if ( ! function_exists('calc_cant'))
{

	function calc_cant($cant=10,$val=null,$ord='asc')
	{
		if ($val == null || $val==0 || $cant==-1) {
			return 4;
		}

		$comp = explode('-', $val);
		$c['min'] = $comp[0];
		$c['max'] = $comp[1];
		if ($cant > $c['max']) {
			return 1;
		}elseif ($cant >= $c['min'] && $cant <= $c['max']) {
			return 2;
		}elseif($cant<$c['min'] ) {
			return 3;
		}
	}
}

if ( ! function_exists('calc_percent'))
{
	function calc_percent($part=10,$total=100,$decimals=2)
	{
		@$percentage = round(($part/$total)*100,$decimals);
		if (is_nan($percentage)) {
			$percentage = 0;
		}
		if (is_infinite($percentage)) {
			$percentage = 0;
		}
		return $percentage;
	}
}

if ( ! function_exists('num_percent'))
{
	function num_percent($percentage=50,$total=600)
	{
		return ($percentage/100)*$total;
	}
}

if ( ! function_exists('round_number'))
{
	function round_number($n=10,$r=5)
	{
		return (round($n)%$r === 0) ? round($n) : round(($n+$r/2)/$r)*$r;
	}
}
