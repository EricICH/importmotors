<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('neumatico'))
{
  function neumatico($schema,$placa=null,$url=null)
  {
    $CI =& get_instance();
    $CI->load->model('medicion_model');
    $sch_table = '
      <table class="table table-border table-sm">
        <thead>
          <tr class="text-center">
            <th scope="col" colspan="5">ESQUEMA</th>
          </tr>
        </thead>
    ';

    if($schema==null){$schema=1;}
    $esquem=$CI->vehiculo->readSchema(['id_esquema'=>$schema],'row');
    $sch=json_decode($esquem['posicion'],true);

    if ($schema==1) {
      
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = @$n_act['promedio']==null?-1:@$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = @$n_act['promedio'];
      }
      $sch_table .='
      <tbody>
        <tr class="text-center">
          <td width="21%"></td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p01'].' mm.">
            <a href="#p01" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['01'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="15%" rowspan="5" class="border border-dark">&nbsp;</td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p02'].' mm.">
            <a href="#p02" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['02'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%"></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="text-center">
          <td width="21%"></td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">
            <a href="#p03" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['03'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
            <a href="#p04" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['04'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%"></td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <a href="#p05" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['05'],'tire-','asc').'.png').'"  style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
            </a>
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2"></td>
        </tr>
      </tfoot>';
    }
    if ($schema==2) {
      
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = $n_act['promedio']==null?-1:$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = $n_act['promedio'];
      }
      $sch_table .='
      <tbody>
        <tr class="text-center">
          <td width="21%"></td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p01'].' mm.">
            <a href="#p01" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['01'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="15%" rowspan="5" class="border border-dark">&nbsp;</td>
          <td width="22%"  data-toggle="tooltip" title="Promedio: '.$info['p02'].' mm.">
            <a href="#p02" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['02'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%"></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr >
          <td width="21%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">
            <a href="#p03" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['03'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
            <a href="#p04" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['04'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="22%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <a href="#p05" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['05'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p06'].' mm.">
            <a href="#p06" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['06'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p07'].' mm.">
            <a href="#p07" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['07'],'tire-','asc').'.png').'" style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
            </a>
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2"></td>
        </tr>
      </tfoot>';
    }
    if ($schema==3) {
      
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = $n_act['promedio']==null?-1:$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = $n_act['promedio'];
      }
      $sch_table .='
      <tbody>
        <tr class="text-center">
          <td width="21%"></td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p01'].' mm.">
            <a href="#p01" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['01'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="15%" rowspan="5" class="border border-dark">&nbsp;</td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p02'].' mm.">
            <a href="#p02" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['02'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%"></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="21%" class="text-right p-0"  data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">
            <a href="#p03" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['03'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
            <a href="#p04" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['04'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="22%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <a href="#p05" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['05'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p06'].' mm.">
            <a href="#p06" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['06'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
        </tr>
        <tr>
          <td width="21%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p07'].' mm.">
            <a href="#p07" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['07'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p08'].' mm.">
            <a href="#p08" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['08'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="22%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p09'].' mm.">
            <a href="#p09" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['09'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p10'].' mm.">
            <a href="#p10" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['10'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p11'].' mm.">
            <a href="#p11" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['11'],'tire-','asc').'.png').'" style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
            </a>
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2"></td>
        </tr>
      </tfoot>';
    }
    if ($schema==4) {
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = $n_act['promedio']==null?-1:$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = $n_act['promedio'];
      }
      $sch_table .='
      <tbody>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td width="15%" rowspan="5" class="border border-dark">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="21%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p01'].' mm.">
            <a href="#p01" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['01'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p02'].' mm.">
            <a href="#p02" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['02'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="22%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">
            <a href="#p03" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['03'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
            <a href="#p04" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['04'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <a href="#p05" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['05'],'tire-','asc').'.png').'" style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
            </a>
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p06'].' mm.">
            <a href="#p06" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['06'],'tire-','asc').'.png').'" style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
            </a>
          </td>
        </tr>
      </tfoot>';
    }
    if ($schema==5) {
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = $n_act['promedio']==null?-1:$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = $n_act['promedio'];
      }
      $sch_table .='
      <tbody>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td width="15%" rowspan="5" class="border border-dark">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr >
          <td width="21%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p01'].' mm.">
            <a href="#p01" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['01'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p02'].' mm.">
            <a href="#p02" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['02'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="22%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">
            <a href="#p03" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['03'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
            <a href="#p04" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['04'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
        </tr>
        <tr class="text-center">
          <td width="21%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <a href="#p05" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['05'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p06'].' mm.">
            <a href="#p06" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['06'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="22%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p07'].' mm.">
            <a href="#p07" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['07'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p08'].' mm.">
            <a href="#p08" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['08'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p09'].' mm.">
            <a href="#p09" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['09'],'tire-','asc').'.png').'" style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
            </a>
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p10'].' mm.">
            <a href="#p10" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['10'],'tire-','asc').'.png').'" style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
            </a>
          </td>
        </tr>
      </tfoot>';
    }
    if ($schema==6) {
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = $n_act['promedio']==null?-1:$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = $n_act['promedio'];
      }
      $sch_table .='
      <tbody>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td width="15%" rowspan="6" class="border border-dark">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="text-center">
          <td width="21%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p01'].' mm.">
            <a href="#p01" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['01'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p02'].' mm.">
            <a href="#p02" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['02'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="22%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">
            <a href="#p03" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['03'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
            <a href="#p04" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['04'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
        </tr>
        <tr class="text-center">
          <td width="21%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <a href="#p05" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['05'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p06'].' mm.">
            <a href="#p06" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['06'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="22%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p07'].' mm.">
            <a href="#p07" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['07'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p08'].' mm.">
            <a href="#p08" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['08'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
        </tr>
        <tr class="text-center">
          <td width="21%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p09'].' mm.">
            <a href="#p09" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['09'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p10'].' mm.">
            <a href="#p10" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['10'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="22%" class="text-right p-0" data-toggle="tooltip" title="Promedio: '.$info['p11'].' mm.">
            <a href="#p11" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['11'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
          <td width="21%" class="text-left p-0" data-toggle="tooltip" title="Promedio: '.$info['p12'].' mm.">
            <a href="#p12" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['12'],'tire-','asc').'.png').'" width="25px">
            </a>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p13'].' mm.">
            <a href="#p13" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['13'],'tire-','asc').'.png').'" style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
            </a>
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p14'].' mm.">
            <a href="#p14" data-toggle="modal">
              <img src="'.base_url('core/assets/images/icon/'.colorfill($info['14'],'tire-','asc').'.png').'" style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
            </a>
          </td>
        </tr>
      </tfoot>';
    }
    $sch_modal = '';

      foreach ($sch['posicion'] as $key => $value) {
      @$m_log = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
      $codneum=@$m_log['cod_neumatico'];
      $neum=$CI->neumatico->read_asignado(['na.placa'=>$placa,'na.pos'=>$key],'row');
      if(isset($neum['id_neumatico'])&&$neum['id_neumatico']!=null){ $codneum=$neum['serial'];}
      $sch_modal .='
      <div class="modal fade" id="p'.$key.'"  role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">'.$value.'</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            '.form_open($url,['id'=>'create', 'after-send'=>site_url().'/medicion/create/'.$placa],['id_medicion'=>@$m_log['id_medicion'],'placa'=>$placa]).'
              <div class="modal-body">
                <div class="form-row">
                  <div class="col-md">
                    '.form_label('Ubicacion').'
                    '.form_input('ubicacion', $key,'hidden').'
                    '.form_input('view_ubicacion', $value,['class'=>'form-control']).'
                  </div>
                  <div class="col-md">';
        if($codneum!='') {
            $sch_modal .= form_label('Serial Neumatico','cod_neumatico',['for'=>'cod_neumatico']);
            $sch_modal .= form_input('cod_neumatico', $codneum, ['class' => 'form-control']);
            $sch_modal .= form_hidden('asigna', '0');
        }else{

         $sch_modal .=form_label('Serial Neumatico');
         $sch_modal .='<br><select name="cod_neumatico" id="cod_neumatico'.$key.'" class="form-control form-control-sm s2" style="width:100%" required="required">';
         foreach ($CI->neumatico->read_disponible() as $disp):
             $sch_modal.='<option data-rep="n" value="'.$disp['serial'].'">'.$disp['serial'].'</option>';
         endforeach;
            $sch_modal.='</select>';
            $sch_modal .= form_hidden('asigna','1');
        }
        $sch_modal .='</div>
                  <div class="w-100 mt-1"></div>
                  <div class="col-md">
                    '.form_label('Canal Ext.').'
                    '.form_input('ce', @$m_log['ce'],['class'=>'form-control']).'
                  </div>
                  <div class="col-md">
                    '.form_label('Canal Cen.').'
                    '.form_input('cc', @$m_log['cc'],['class'=>'form-control']).'
                  </div>
                  <div class="col-md">
                    '.form_label('Canal Int.').'
                    '.form_input('ci', @$m_log['ci'],['class'=>'form-control']).'
                  </div>
                </div>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-dark" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
              </div>
            '.form_close().'
          </div>
        </div>
      </div>';
    }

    $return['schema'] = $sch_table.'</table>';
    $return['form'] = $sch_modal;
    $return['qty'] = count($sch['posicion']);
    return $return;
  }
}

if ( ! function_exists('bateria'))
{
  function bateria($permisos, $ctrl)
  {
  $read=0;
    foreach ($permisos as $permiso) {
      if($permiso['action']==$ctrl){
        $read=$permiso['read'];
      }
    }
    return $read;
  }
}

if ( ! function_exists('neumatico_load'))
{
  function neumatico_load($schema=1,$placa=null,$url=null)
  {
    $CI =& get_instance();
    $CI->load->model('medicion_model');
    $CI->load->model('neumatico_model');
    $sch_table = '
      <table class="table table-border table-sm">
        <thead>
          <tr class="text-center">
            <th scope="col" colspan="5">ESQUEMA</th>
          </tr>
        </thead>';
    $asignados=$CI->neumatico->read_asignado(['na.placa'=>$placa],'result',['n.id_neumatico'=>'desc'],['group'=>'n.id_neumatico']);
    
    if(!empty($asignados)){
      foreach ($asignados as $tire) {
        @$asig[]=$tire['pos'];
      }     
    }
    $esquem= $CI->vehiculo->readSchema(['id_esquema'=>$schema],'row');
    $sch=json_decode($esquem['posicion'],true);
    if ($schema==1) {
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = $n_act['promedio']==null?-1:$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = $n_act['promedio'];
        $dis[$key]='';
        $col[$key]='white';
        if(!empty($asig)){
          $dis[$key]=in_array($key, $asig)?$dis[$key]='disabled="disabled"':'';
          $col[$key]=in_array($key, $asig)?$col[$key]='success':'white';
        }
      }

      $sch_table .='
      <tbody>
        <tr class="text-center">
          <td width="21%"></td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p01'].' mm.">
            <label for="radio_p01">              
              <input name="position" type="radio" id="radio_p01" data-rep="n" value="01" data-img="img_p01" class="with-gap radio-col-blue" '.$dis['01'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['01'].'.png').'" id="img_p01" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>01</b></span>
            </label>
          </td>
          <td width="15%" rowspan="5" class="border border-dark">&nbsp;</td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p02'].' mm.">
            <label for="radio_p02">        
            <input name="position" type="radio" data-rep="n" value="02" id="radio_p02" data-img="img_p02" '.$dis['02'].'>      
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['02'].'.png').'" id="img_p02" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>02</b></span>
            </label>
          </td>
          <td width="21%"></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="text-center">
          <td width="21%"></td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">
            <label for="radio_p03">        
            <input name="position" type="radio" data-rep="n" value="03" id="radio_p03" data-img="img_p03" '.$dis['03'].'>      
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['03'].'.png').'" id="img_p03" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>03</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
            <label for="radio_p04">        
            <input name="position" type="radio" data-rep="n" value="04" id="radio_p04" data-img="img_p04" '.$dis['04'].'>      
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['04'].'.png').'" id="img_p04" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>04</b></span>
            </label>
          </td>
          <td width="21%"></td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <label for="radio_p05">              
              <input name="position" type="radio" id="radio_p05" data-rep="s" value="05" data-img="img_p05" class="with-gap radio-col-blue" '.$dis['05'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['05'].'.png').'" id="img_p05" class="img_esq"  style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['05'].'" style="width: 22px;padding: 4px;margin-left:10px;"><b>05</b></span>
            </label>
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2"></td>
        </tr>
      </tfoot>';
    }
    if ($schema==2) {
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = $n_act['promedio']==null?-1:$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = $n_act['promedio'];
        $dis[$key]='';
        $col[$key]='white';
        if(!empty($asig)){
          $dis[$key]=in_array($key, $asig)?$dis[$key]='disabled="disabled"':'';
          $col[$key]=in_array($key, $asig)?$col[$key]='success':'white';
        }
      }
      
      $sch_table .='
      <tbody>
        <tr class="text-center">
          <td width="21%"></td>
          <td width="21%" data-toggle="tooltip" title="Delantera Izquierda<br>">            
            <label for="radio_p01">              
              <input name="position" type="radio" data-rep="n" value="01" id="radio_p01" data-img="img_p01" class="with-gap radio-col-blue" '.$dis['01'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['01'].'.png').'" id="img_p01" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>01</b></span>
            </label>
          </td>
          <td width="15%" rowspan="5" class="border border-dark">&nbsp;</td>
          <td width="22%" data-toggle="tooltip" title="Delantera Izquierda">  
            <label for="radio_p02">        
            <input name="position" type="radio"  data-rep="n" value="02" id="radio_p02" data-img="img_p02" '.$dis['02'].'>      
            <img src="'.base_url('core/assets/images/icon/tire-'.$col['02'].'.png').'" id="img_p02" class="img_esq" width="25px">
            <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>02</b></span>
            </label>
          </td>
          <td width="21%"></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="text-center">
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">          
            <label for="radio_p03"  >             
              <input name="position" type="radio" data-rep="n" value="03" id="radio_p03" data-img="img_p03" class="with-gap radio-col-blue" '.$dis['03'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['03'].'.png').'" id="img_p03" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>03</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
           
           <label for="radio_p04">              
              <input name="position" type="radio" data-rep="n" value="04" id="radio_p04" data-img="img_p04" class="with-gap radio-col-blue" '.$dis['04'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['04'].'.png').'" id="img_p04" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>04</b></span>
            </label>           
          </td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <label for="radio_p05" >
            <input name="position" type="radio" data-rep="n" value="05" id="radio_p05" data-img="img_p05" class="with-gap radio-col-blue" '.$dis['05'].'>
             <img src="'.base_url('core/assets/images/icon/tire-'.$col['05'].'.png').'" id="img_p05" id="img_p05" class="img_esq" width="25px">
             <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>05</b></span>            
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p06'].' mm.">
            <label for="radio_p06">
            <input name="position" type="radio" data-rep="n" value="06" id="radio_p06" data-img="img_p06" class="with-gap radio-col-blue" '.$dis['06'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['06'].'.png').'" id="img_p06" class="img_esq"width="25px">              
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>06</b></span>
            </label>
          </td>
        </tr>
      <tr>
          <td colspan="5"></td>
        </tr>
      </tbody>
      <tfoot>
      <tr>
          <td colspan="5"></td>
        </tr>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p07'].' mm.">
            <label for="radio_p07">              
              <input name="position" type="radio" data-rep="s" value="07" id="radio_p07" data-img="img_p07" class="with-gap radio-col-blue" '.$dis['07'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['07'].'.png').'" id="img_p07" class="img_esq"  style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['07'].'" style="width: 22px;padding: 4px;margin-left:10px;"><b>07</b></span>
            </label>  
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2"></td>
        </tr>
      </tfoot>';
    }
    if ($schema==3) {
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = $n_act['promedio']==null?-1:$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = $n_act['promedio'];
        $dis[$key]='';
        $col[$key]='white';
        if(!empty($asig)){
          $dis[$key]=in_array($key, $asig)?$dis[$key]='disabled="disabled"':'';
          $col[$key]=in_array($key, $asig)?$col[$key]='success':'white';
        }
      }
      $sch_table .='
      <tbody>
        <tr class="text-center">
          <td width="21%"></td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p01'].' mm.">
           <label for="radio_p01">              
              <input name="position" type="radio" data-rep="n" value="01" id="radio_p01" data-img="img_p01" class="with-gap radio-col-blue" '.$dis['01'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['01'].'.png').'" id="img_p01" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>01</b></span>
            </label>
          </td>
          <td width="15%" rowspan="5" class="border border-dark">&nbsp;</td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p02'].' mm.">
            <label for="radio_p02">              
              <input name="position" type="radio" data-rep="n" value="02" id="radio_p02" data-img="img_p02" '.$dis['02'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['02'].'.png').'" id="img_p02" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>02</b></span>
            </label>
          </td>
          <td width="21%"></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="text-center">
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">
            <label for="radio_p03">              
              <input name="position" type="radio" data-rep="n" value="03" id="radio_p03" data-img="img_p03" '.$dis['03'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['03'].'.png').'" id="img_p03" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>03</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
           <label for="radio_p04">              
              <input name="position" type="radio" data-rep="n" value="04" id="radio_p04" data-img="img_p04" '.$dis['04'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['04'].'.png').'" id="img_p04" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>04</b></span>
            </label>
          </td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <label for="radio_p05">              
              <input name="position" type="radio" data-rep="n" value="05" id="radio_p05" data-img="img_p05" '.$dis['05'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['05'].'.png').'" id="img_p05" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>05</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p06'].' mm.">
            <label for="radio_p06">              
              <input name="position" type="radio" data-rep="n" value="06" id="radio_p06" data-img="img_p06" '.$dis['06'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['06'].'.png').'" id="img_p06" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>06</b></span>
            </label>
          </td>
        </tr>
        <tr class="text-center">
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p07'].' mm.">
           <label for="radio_p07">              
              <input name="position" type="radio" data-rep="n" value="07" id="radio_p07" data-img="img_p07" '.$dis['07'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['07'].'.png').'" id="img_p07" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>07</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p08'].' mm.">
            <label for="radio_p08">              
              <input name="position" type="radio" data-rep="n" value="08" id="radio_p08" data-img="img_p08" '.$dis['08'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['08'].'.png').'" id="img_p08" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>08</b></span>
            </label>
          </td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p09'].' mm.">
            <label for="radio_p09">              
              <input name="position" type="radio" data-rep="n" value="09" id="radio_p09" data-img="img_p09" '.$dis['09'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['09'].'.png').'" id="img_p09" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>09</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p10'].' mm.">
            <label for="radio_p10">              
              <input name="position" type="radio" data-rep="n" value="10" id="radio_p10" data-img="img_p10" '.$dis['10'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['10'].'.png').'" id="img_p10" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>10</b></span>
            </label>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p11'].' mm.">
          <label for="radio_p11"> 
            <input name="position" type="radio" data-rep="s" value="11" id="radio_p11" data-img="img_p11" '.$dis['11'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['11'].'.png').'" id="img_p11" class="img_esq" style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['11'].'" style="width: 22px;padding: 4px;margin-left:10px;"><b>11</b></span>
          </label>
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2"></td>
        </tr>
      </tfoot>';
    }
    if ($schema==4) {
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = $n_act['promedio']==null?-1:$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = $n_act['promedio'];
        $dis[$key]='';
        $col[$key]='white';
        if(!empty($asig)){
          $dis[$key]=in_array($key, $asig)?$dis[$key]='disabled="disabled"':'';
          $col[$key]=in_array($key, $asig)?$col[$key]='success':'white';
        }
      }
      $sch_table .='
      <tbody>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td width="15%" rowspan="5" class="border border-dark">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="text-center">
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p01'].' mm.">
            <label for="radio_p01">              
              <input name="position" type="radio" data-rep="n" value="01" id="radio_p01" data-img="img_p01" class="with-gap radio-col-blue" '.$dis['01'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['01'].'.png').'" id="img_p01" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>01</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p02'].' mm.">
            <label for="radio_p02">              
              <input name="position" type="radio" data-rep="n" value="02" id="radio_p02" data-img="img_p02" '.$dis['02'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['02'].'.png').'" id="img_p02" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['02'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>02</b></span>
            </label>
          </td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">
            <label for="radio_p03">              
              <input name="position" type="radio" data-rep="n" value="03" id="radio_p03" data-img="img_p03" '.$dis['03'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['03'].'.png').'" id="img_p03" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['03'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>03</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
            <label for="radio_p04">              
              <input name="position" type="radio" data-rep="n" value="04" id="radio_p04" data-img="img_p04" '.$dis['04'].' >
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['04'].'.png').'" id="img_p04" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['04'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>04</b></span>
            </label>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <label for="radio_p07">              
              <input name="position" type="radio" data-rep="s" value="05" id="radio_p05" data-img="img_p05" class="with-gap radio-col-blue" '.$dis['05'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['05'].'.png').'" id="img_p05" class="img_esq"  style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['05'].'" style="width: 22px;padding: 4px;margin-left:10px;"><b>05</b></span>
            </label>
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p06'].' mm.">
            <label for="radio_p06">              
              <input name="position" type="radio" data-rep="s" value="06" id="radio_p06" data-img="img_p06" class="with-gap radio-col-blue" '.$dis['06'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['06'].'.png').'" id="img_p06" class="img_esq"  style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['06'].'" style="width: 22px;padding: 4px;margin-left:10px;"><b>06</b></span>
            </label>
          </td>
        </tr>
      </tfoot>';
    }
    if ($schema==5) {
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = $n_act['promedio']==null?-1:$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = $n_act['promedio'];
        $dis[$key]='';
        $col[$key]='white';
        if(!empty($asig)){
          $dis[$key]=in_array($key, $asig)?$dis[$key]='disabled="disabled"':'';
          $col[$key]=in_array($key, $asig)?$col[$key]='success':'white';
        }
      }
      $sch_table .='
      <tbody>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td width="15%" rowspan="5" class="border border-dark">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="text-center">
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p01'].' mm.">
            <label for="radio_p01">              
              <input name="position" type="radio" data-rep="n" value="01" id="radio_p01" data-img="img_p01" class="with-gap radio-col-blue" '.$dis['01'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['01'].'.png').'" id="img_p01" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>01</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p02'].' mm.">
            <label for="radio_p02">              
              <input name="position" type="radio" data-rep="n" value="02" id="radio_p02" data-img="img_p02" '.$dis['02'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['02'].'.png').'" id="img_p02" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['02'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>02</b></span>
            </label>
          </td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">
            <label for="radio_p03">              
              <input name="position" type="radio" data-rep="n" value="03" id="radio_p03" data-img="img_p03" '.$dis['03'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['03'].'.png').'" id="img_p03" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['03'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>03</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
            <label for="radio_p04">              
              <input name="position" type="radio" data-rep="n" value="04" id="radio_p04" data-img="img_p04" '.$dis['04'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['04'].'.png').'" id="img_p04" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['04'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>04</b></span>
            </label>
          </td>
        </tr>
        <tr class="text-center">
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <label for="radio_p05">              
              <input name="position" type="radio" data-rep="n" value="05" id="radio_p05" data-img="img_p05" '.$dis['05'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['05'].'.png').'" id="img_p05" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['05'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>05</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p06'].' mm.">
            <label for="radio_p06">              
              <input name="position" type="radio" data-rep="n" value="06" id="radio_p06" data-img="img_p06" '.$dis['06'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['06'].'.png').'" id="img_p06" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['06'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>06</b></span>
            </label>
          </td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p07'].' mm.">
            <label for="radio_p07">              
              <input name="position" type="radio" data-rep="n" value="07" id="radio_p07" data-img="img_p07" '.$dis['07'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['07'].'.png').'" id="img_p07" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['07'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>07</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p08'].' mm.">
            <label for="radio_p08">              
              <input name="position" type="radio" data-rep="n" value="08" id="radio_p08" data-img="img_p08" '.$dis['08'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['08'].'.png').'" id="img_p08" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['08'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>08</b></span>
            </label>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p09'].' mm.">
            <label for="radio_p09">              
              <input name="position" type="radio" data-rep="s" value="09" id="radio_p09" data-img="img_p09" class="with-gap radio-col-blue" '.$dis['09'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['09'].'.png').'" id="img_p09" class="img_esq"  style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
             <span class="badge badge-pill badge-info" id="badge'.$col['09'].'" style="width: 22px;padding: 4px;margin-left:10px;"><b>09</b></span> 
            </label>
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p10'].' mm.">
            <label for="radio_p10">              
              <input name="position" type="radio" data-rep="s" value="10" id="radio_p10" data-img="img_p10" class="with-gap radio-col-blue" '.$dis['10'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['10'].'.png').'" id="img_p10" class="img_esq"  style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['10'].'" style="width: 22px;padding: 4px;margin-left:10px;"><b>10</b></span>
            </label>
          </td>
        </tr>
      </tfoot>';
    }
    if ($schema==6) {
      $info = [];
      foreach ($sch['posicion'] as $key => $value) {
        $n_act = $CI->medicion->read(['m.placa'=>$placa,'m.ubicacion'=>$key],'row');
        $promedio = $n_act['promedio']==null?-1:$n_act['promedio'];
        $info[$key] = calc_cant($promedio,'4-12');
        $info['p'.$key] = $n_act['promedio'];
        $dis[$key]='';
        $col[$key]='white';
        if(!empty($asig)){
          $dis[$key]=in_array($key, $asig)?$dis[$key]='disabled="disabled"':'';
          $col[$key]=in_array($key, $asig)?$col[$key]='success':'white';
        }
      }
      $sch_table .='
      <tbody>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td width="15%" rowspan="6" class="border border-dark">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="text-center">
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p01'].' mm.">
            <label for="radio_p01">              
              <input name="position" type="radio" data-rep="n" value="01" id="radio_p01" data-img="img_p01" class="with-gap radio-col-blue" '.$dis['01'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['01'].'.png').'" id="img_p01" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['01'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>01</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p02'].' mm.">
            <label for="radio_p02">              
              <input name="position" type="radio" data-rep="n" value="02" id="radio_p02" data-img="img_p02" '.$dis['02'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['02'].'.png').'" id="img_p02" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['02'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>02</b></span>
            </label>
          </td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p03'].' mm.">
            <label for="radio_p03">              
              <input name="position" type="radio" data-rep="n" value="03" id="radio_p03" data-img="img_p03" '.$dis['03'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['03'].'.png').'" id="img_p03" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['03'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>03</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p04'].' mm.">
            <label for="radio_p04">              
              <input name="position" type="radio" data-rep="n" value="04" id="radio_p04" data-img="img_p04" '.$dis['04'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['04'].'.png').'" id="img_p04" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['04'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>04</b></span>
            </label>
          </td>
        </tr>
        <tr class="text-center">
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p05'].' mm.">
            <label for="radio_p05">              
              <input name="position" type="radio" data-rep="n" value="05" id="radio_p05" data-img="img_p05" '.$dis['05'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['05'].'.png').'" id="img_p05" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['05'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>05</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p06'].' mm.">
            <label for="radio_p06">              
              <input name="position" type="radio" data-rep="n" value="06" id="radio_p06" data-img="img_p06" '.$dis['06'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['06'].'.png').'" id="img_p06" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['06'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>06</b></span>
            </label>
          </td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p07'].' mm.">
            <label for="radio_p07">              
              <input name="position" type="radio" data-rep="n" value="07" id="radio_p07" data-img="img_p07" '.$dis['07'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['07'].'.png').'" id="img_p07" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['07'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>07</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p08'].' mm.">
            <label for="radio_p08">              
              <input name="position" type="radio" data-rep="n" value="08" id="radio_p08" data-img="img_p08" '.$dis['08'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['08'].'.png').'" id="img_p08" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['08'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>08</b></span>
            </label>
          </td>
        </tr>
        <tr class="text-center">
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p09'].' mm.">
            <label for="radio_p09">              
              <input name="position" type="radio" data-rep="n" value="09" id="radio_p09" data-img="img_p09" '.$dis['09'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['09'].'.png').'" id="img_p09" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['09'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>09</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p10'].' mm.">
            <label for="radio_p10">              
              <input name="position" type="radio" data-rep="n" value="10" id="radio_p10" data-img="img_p10" '.$dis['10'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['10'].'.png').'" id="img_p10" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['10'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>10</b></span>
            </label>
          </td>
          <td width="22%" data-toggle="tooltip" title="Promedio: '.$info['p11'].' mm.">
            <label for="radio_p11">              
              <input name="position" type="radio" data-rep="n" value="11" id="radio_p11" data-img="img_p11" '.$dis['11'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['11'].'.png').'" id="img_p11" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['11'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>11</b></span>
            </label>
          </td>
          <td width="21%" data-toggle="tooltip" title="Promedio: '.$info['p12'].' mm.">
            <label for="radio_p12">              
              <input name="position" type="radio" data-rep="n" value="12" id="radio_p12" data-img="img_p12" '.$dis['12'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['12'].'.png').'" id="img_p12" class="img_esq" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['12'].'" style="width: 22px;padding: 4px;margin-top:5px;"><b>12</b></span>
            </label>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">REPUESTO</td>
        </tr>
        <tr class="text-center">
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p13'].' mm.">
            <label for="radio_p13">              
              <input name="position" type="radio" data-rep="s" value="13" id="radio_p13" data-img="img_p13" class="with-gap radio-col-blue" '.$dis['13'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['13'].'.png').'" id="img_p13" class="img_esq"  style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['13'].'" style="width: 22px;padding: 4px;margin-left:10px;"><b>13</b></span>
            </label>
          </td>
          <td>'.nbs().'</td>
          <td width="21%" colspan="2" data-toggle="tooltip" title="Promedio: '.$info['p14'].' mm.">
            <label for="radio_p14">              
              <input name="position" type="radio" data-rep="s" value="14" id="radio_p14" data-img="img_p14" class="with-gap radio-col-blue" '.$dis['14'].'>
              <img src="'.base_url('core/assets/images/icon/tire-'.$col['14'].'.png').'" id="img_p14" class="img_esq"  style="transform: rotate(90deg);-ms-transform: rotate(90deg);-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);-o-transform: rotate(90deg);" width="25px">
              <span class="badge badge-pill badge-info" id="badge'.$col['14'].'" style="width: 22px;padding: 4px;margin-left:10px;"><b>14</b></span>
            </label>
          </td>
        </tr>
      </tfoot>';
    };
    $return['schema'] = $sch_table.'</table>';
    $return['qty'] = count($sch['posicion']);
    return $return;
  }
}
