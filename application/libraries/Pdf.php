<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Al requerir el autoload, cargamos todo lo necesario para trabajar

class Pdf {
  public function create($html,$title='',$orientation='P',$format='A4'){
    require_once FCPATH."assets/tcpdf/tcpdf.php";
    ob_start();
    //error_reporting(E_ALL & ~E_NOTICE);
    //ini_set('display_errors', 0);
    //ini_set('log_errors', 1);

    $pdf = new TCPDF($orientation, PDF_UNIT, $format, true, 'UTF-8', false);

    // Set some content to print
    $content=$html;
    if ($title=='') {
      $title = rand(1,1000).'_'.date('d-m-Y');
    }

    // set document information
    $pdf->SetCreator('IMPORDIESEL');
    $pdf->SetAuthor('IMPORDIESEL');
    $pdf->SetTitle($title);
    $pdf->SetSubject($title);
    $pdf->SetKeywords('Impordiesel, PDF');


    // remove default header/footer
    $pdf->setPrintHeader(false);
    //$pdf->setPrintFooter(false);

    // set default header data
    //$pdf->SetHeaderData(PDF_HEADER_LOGO,PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
    $pdf->setFooterData(array(0,64,0), array(0,64,128));

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(5, 5, 5);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/spa.php')) {
      require_once(dirname(__FILE__).'/lang/spa.php');
      $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // set default font subsetting mode
    $pdf->setFontSubsetting(true);

    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 14, '', true);

    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage();

    // set text shadow effect
    $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

    // Print text using writeHTMLCell()
    $pdf->writeHTML($content);

    // ---------------------------------------------------------
    ob_end_clean();
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output($title.'.pdf', 'I');
  }
}