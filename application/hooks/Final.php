<?php
class Report extends CI_Hooks {
  public function __construct() {
    $this->CI = &get_instance();
  }
  public function Resumen()
  {
    if ($this->CI->session->IdUser!==null){
      
      if ($this->CI->session->Parent==0){
        $id = $this->CI->session->IdUser;
      }else{
        $id = $this->CI->session->Parent;
      }
     /* for($i=15;$i>0;$i-1){
          $dateObj=date("Y-m-d",strtotime(date('Y-m-d')."- ".$i." days")); 
          $m_abi = $this->CI->extra->read(
              ['count(a.id_auto) as Activos'],
              'ordenes o',
              [
                'autos a'=>'a.id_auto=o.id_auto',
                'autos_tipos t'=>'a.tipo=t.id_tipo'
              ],
              ['where'=>[
                  'o.id_usuario'=>$this->CI->usuario->id_user(),
                  't.trailer'=>'s',
                  'o.estatus !='=>'PERDIDA TOTAL',
                  'o.fecha_out>='=>$dateObj,
                  "o.fecha_in <="=>$dateObj
                ],
                'where_not_in'=>[
                  'o.estatus'=>['CERRADA','ANULADA','FACTURADA'],
                ]
              ],
              'row',
              null,
              ['group'=>'a.id_auto']
            );
          echo $m_abi['Activos'];
      }*/
      $total = $this->CI
        ->extra
        ->read(['count(*) as total'],'autos',null, ['where'=>['id_usuario'=> $id, 'estatus!='=>5,'disp'=>'S']],'row');
      $op = $this->CI
        ->extra
        ->read(['count(*) as total'],'autos',null, ['where'=>['id_usuario'=> $id,'estatus'=>1,'disp'=>'S']],'row');
        
      $m_tot = count($this->CI->vehiculo->read(['estatus!='=>5,'disp'=>'S','trailer'=>'n']));
      $t_tot = count($this->CI->vehiculo->read(['estatus!='=>5,'disp'=>'S','trailer'=>'s']));
      $m_dis = count($this->CI->vehiculo->read(['estatus'=>1,'disp'=>'S','trailer'=>'n']));
      $t_dis = count($this->CI->vehiculo->read(['estatus'=>1,'disp'=>'S','trailer'=>'s']));

      $resumen = $this->CI->db->get_where('informe_resumen', ['id_usuario'=> $id,'fecha'=>date('Y-m-d')])->row_array();
         if($total['total']!=0) {
             $dispon = ($op['total'] / $total['total']) * 100;
             if (is_nan($dispon)) {
                 $dispon = 0;
             }
             if (is_infinite($dispon)) {
                 $dispon = 0;
             }
         }else{
             $dispon = 0;
         }

         if($m_tot!=0) {
             $dis_m = ($m_dis * 100)/$m_tot;
             if (is_nan($dis_m)) {
                 $dis_m = 0;
             }
             if (is_infinite($dis_m)) {
                 $dis_m = 0;
             }
         }else{
             $dis_m = 0;
         }

         if($t_tot!=0) {
             $dis_t = ($t_dis/$t_tot)*100;
             if (is_nan($dis_t)) {
                 $dis_t = 0;
             }
             if (is_infinite($dis_t)) {
                 $dis_t = 0;
             }
         }else{
             $dis_t = 0;
         }
         $data=[
            'disponibilidad'=>number_format($dispon, 1,".",""),
            'motor'=>number_format($dis_m, 1,".",""),
            'trailer'=>number_format($dis_t, 1,".","")
          ];
      if (!empty($resumen)){
        $this->CI->db->update('informe_resumen',$data,['id_resumen'=>$resumen['id_resumen']]);
      }else{
        $data['id_usuario']=$id;
        $data['fecha']=date('Y-m-d');
        $this->CI->db->insert('informe_resumen',$data);
      }
    }
  }
}
