

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Csv_import extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('csv_import_model');
		$this->load->library('csvimport');
	}

	public function index($ctrl)
	{
		$data=['control'=>$ctrl, 'file'=>'formato_'.$ctrl.'.csv'];
		$this->load->view('ui/head');
		$this->load->view('Import/csv_import',$data);
		$this->load->view('ui/footer');
	}

	public function load_data()
	{
		$result = $this->csv_import_model->select();
		$output = '
		<h3 align="center">Imported User Details from CSV File</h3>
		<div class="table-responsive">
		<table class="table table-bordered table-striped">
		<tr>
		<th>Sr. No</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Phone</th>
		<th>Email Address</th>
		</tr>
		';
		$count = 0;
		if($result->num_rows() > 0)
		{
			foreach($result->result() as $row)
			{
				$count = $count + 1;
				$output .= '
				<tr>
				<td>'.$count.'</td>
				<td>'.$row->first_name.'</td>
				<td>'.$row->last_name.'</td>
				<td>'.$row->phone.'</td>
				<td>'.$row->email.'</td>
				</tr>
				';
			}
		}
		else
		{
			$output .= '
			<tr>
			<td colspan="5" align="center">Data not Available</td>
			</tr>
			';
		}
		$output .= '</table></div>';
		echo $output;
	}

	public function planificacion()
	{
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';	');
		$IDU=$this->usuario->id_user();	
		$modelos=[];
		$k_plan = 0;
		//var_dump($file_data);
		foreach($file_data as $row)
		{
			var_dump($row);
			$week = $row['semana'];
			for($i=0; $i<7; $i++){
				$data[$i] = date('Y-m-d', strtotime('01/01 +' . $week . ' weeks first day +' . $i . ' day')) . '<br />';
			}
			echo $row["km_acumulado"];
			echo $row["flota"];
			$auto = $this->vehiculo->read(['flota'=>$row['flota']],false);
			if(isset($auto['id_auto'])&&$auto['id_auto']!=null) {
				if(isset($row['km_acumulado'])&&$row['km_acumulado']!=null){
					$data = array(
						'id_auto' => $auto['id_auto'],
						'semana' => $row['semana'],
						'kilometraje' => $row["km_acumulado"]
					);
					$this->planificacion->create($data);
				}
			}
			
		}
		$week_start= date("Y-m-d", strtotime('monday this week', time()));
		$resumen = $this->resumen->read(['fecha'=>$week_start],'row',['fecha'=>'desc']);

		$plan_min = $this->plan->read(['where'=>['kilometraje >'=>0]],false,['order_by'=>['kilometraje','asc'],'limit'=>[1,0]]);
		$minimo=$plan_min['kilometraje']-num_percent(10,$plan_min['kilometraje']);
		$pln =$this->vehiculo->read(['au.km_contador >='=>round_number($minimo), 'au.estatus!='=>5,'au.disp'=>'S']);
		

		foreach ($pln as $aut) {
			$orden=$this->orden_trabajo->read(['ot.id_auto'=>$aut['id_auto'],'ot.estatus'=>'ABIERTA','ot.descripcion_1'=>'Motor']);
			if (count($orden)==0){
				$k_plan+=1;
				if($aut['trailer']=='s'){
					$aut['modelo']='trailer';
				}
				if(array_key_exists($aut['modelo'], $modelos)){
					$modelos[$aut['modelo']]+=1;
				}else{
					$modelos[$aut['modelo']]=1;
				}	
			}	
		}
		
		$mods=json_encode($modelos);
		$datar=[
			'plan'=>$k_plan,
			'plan_models'=>$mods
		];
		if($resumen['prev']==null){
			$datar['prev']=0;
		}
		$this->db->update('informe_resumen', $datar,['id_resumen'=>$resumen['id_resumen']]);
		$this->logSys->create(['controller'=>'Planificacion', 'action'=>'Realizo Carga Masiva a: <strong>Planificacion Kilometraje</strong>']);
		redirect(site_url('planificacion/read'));
	}

	public function planmayor()
	{
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');
		$IDU=$this->usuario->id_user();	
		foreach($file_data as $row)
		{
			$auto = $this->vehiculo->read(['flota'=>$row['flota']],false);
			unset($row['flota']);
			foreach ($row as $key => $value) {
				if(($value!=null)&&($value!='')){
					$item=$this->vehiculo->readm_its(['item like'=>'%'.$key.'%'],'row');
					//var_dump($item);
					$search=$this->vehiculo->readm_val(['id_auto'=>$auto['id_auto'], 'pm.item'=>$key],'row');
					if($search['id_plan_mayor']!=null){
					 $this->db->update('planmayor',['val'=>$value],['id_plan_mayor'=>$search['id_plan_mayor']]);
					}else{
					 $this->db->insert('planmayor',['id_auto'=>$auto['id_auto'], 'item'=>$item['id_item_pm'],'val'=>$value]);
					}
					//print_r($this->db->last_query());
				}
			}			
		}
		redirect(site_url('vehiculo/readmayor'));
	}

	public function despacho()
	{
		$IDU=$this->usuario->id_user();
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');		
		foreach($file_data as $row)
		{
		//	echo 'leido';
			$dt=explode('/',$row['fecha']);
			$fecha=$dt[2].'-'.$dt[1].'-'.$dt[0];
			$nrot=explode(' ',$row['nombre_tienda']);
			$tiendan=str_replace($nrot[0].' ', '', $row['nombre_tienda']);
			$this->db->select('*')
			->from('tienda')
			->where('nombre like', $tiendan)
			->where('id_usuario', $IDU,false);
			$tienda = $this->db->get()->row_array();
			$this->db->select('*')
			->from('despachos')
			->where('id_tienda', $tienda['id_tienda'])
			->where('fecha', $fecha);
			$item = $this->db->get()->row_array();
		//	var_dump($item);
			if(isset($item)&&$item['id_despacho']!=null){
				$data=[
					'cesta'=>$row['cestas'],
					'bulto'=>$row['bultos'],
					'paleta'=>$row['paletas'],
					'carga'=>$row['vol'],
					'id_usuario'=> $IDU
				];
				$this->db->update('despachos', $data,['id_despacho'=>$item['id_despacho']]);
			}else{
				$data=[
					'id_tienda'=>$tienda['id_tienda'],
					'cesta'=>$row['cestas'],
					'bulto'=>$row['bultos'],
					'paleta'=>$row['paletas'],
					'carga'=>$row['vol'],
					'fecha'=>$fecha,
					'id_usuario'=> $IDU
				];
		//		var_dump($data);
				$this->db->insert('despachos',$data);
			}
		}
		$this->logSys->create(['controller'=>'Despachos', 'action'=>'Carga Planificacion de: <strong>Despachos</strong> - '.nice_date($fecha,'d/m/Y')]);
		redirect(site_url('viaje/plantilla/?fecha='.$fecha));
	}
	
	public function inventario()
	{
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');
		$IDU=$this->usuario->id_user();		
		foreach($file_data as $row)
		{
			$this->db->select('*')
			->from('inventario')
			->where('codigo', $row['codigo'])
			->where('referencia', $row['referencia'])
			->where('descripcion', $row['descripcion'])
			->where('id_usuario', $IDU,false);
			$item = $this->db->get()->row_array();
			$this->db->select('*')
			->from('inventario_almacenes')
			->where('nombre', $row['almacen'])
			->where('id_usuario', $IDU,false);
			$alm = $this->db->get()->row_array();
			if(isset($item)&&$item['id_inventario']!=null){
				$data['existencia']=$row['existencia'];
				if($row['costo']!=''){ $data['costo']=$row['costo']; }
				$this->db->update('inventario', $data,['referencia'=>$row['referencia']]);
				
				$data2=[
					'cantidad'  =>  $row['existencia'],
					'existencia'  =>  $item['existencia'],
					'costo'       =>  $row['costo'],
					'observacion'=> 'Actualizacion por Carga Masiva',
					'fecha_in'  => date('Y-m-d'),
		            'hora' => date('H:i'),
		            'id_usuario'=> $IDU,
		            'id_inventario'=> $item['id_inventario'],
		            'emisor'=> $this->session->IdUser,
		            'referencia'=> 'N/A',
		            'tipo' => 'A'
				];
				$this->db->insert('inventario_cargar', $data2);
			}
		}
		$this->logSys->create(['controller'=>'Inventario', 'action'=>'Realizo Carga Masiva de: <strong>Inventario</strong>']);
		redirect(site_url('inventario/read'));
	}



	public function inventario_renew()
	{
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');
		$IDU=$this->usuario->id_user();
		$this->db->update('inventario',['existencia'=>0],['id_usuario'=>$IDU]);
		foreach($file_data as $row)
		{
			$item=$this->inventario->read(['in.codigo'=>$row['codigo'],'in.id_usuario'=>$IDU],false);
			if(isset($item)&&$item['id_inventario']!=null){
				$data['existencia']=$row['existencia'];
				$data['referencia']=$row['referencia'];
				$this->db->update('inventario', $data,['id_inventario'=>$item['id_inventario']]);
				
				$data2=[
					'cantidad'  =>  $row['existencia'],
					'existencia'  =>  $item['existencia'],
					'observacion'=> 'Actualizacion por Carga Masiva',
					'fecha_in'  => date('Y-m-d'),
		            'hora' => date('H:i'),
		            'id_usuario'=> $IDU,
		            'id_inventario'=> $item['id_inventario'],
		            'emisor'=> $this->session->IdUser,
		            ];
				$this->db->insert('inventario_cargar', $data2);
			}
		}
		$this->logSys->create(['controller'=>'Inventario', 'action'=>'Realizo Carga Masiva de: <strong>Inventario</strong>']);
		redirect(site_url('inventario/read'));
	}






#################### REVISAR ##################################
	public function temparios()
	{
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');
		$IDU=$this->usuario->id_user();		
		$t= count($this->tempario->read());
		$i=$t+1;
		foreach($file_data as $row)
		{
			$datat= $this->tempario->read(['where'=>['tc.categoria'=>$row['categoria']]],false,['group_by'=>['tc.id_tempario_categoria']]);
			if(!empty($datat)){
				$id_categoria=$datat['id_tempario_categoria'];
			}else{
				$total=count($this->tempario->read(null,true,['group_by'=>['tc.id_tempario_categoria']]));
				$cod=$total+1;
				$this->db->insert('tempario_categorias', ['id_usuario'=>$IDU,'categoria'=>$row['categoria'],'codigo'=>'T-'.$cod,'costo_mo'=>$row['costo_mo']]);
				$id_categoria=$this->db->insert_id();
			}
				$data2=[
					'servicio'=>$row['servicio'],
					'horas'=>str_replace(',', '.', $row['horas']),
					'costo'=>$row['costo_mo'],
					'id_tempario_categoria'=>$id_categoria,
					'codigo'=>'M-OBRA'.zerofill($i,3),
					'id_usuario'=>$IDU
				];
				$this->db->insert('tempario_servicios', $data2);
				$i++;	
		}
		$this->logSys->create(['controller'=>'Inventario', 'action'=>'Realizo Carga Masiva de: <strong>Temparios</strong>']);
		//redirect(site_url('inventario/read'));
	}


####################################





	public function relacion()
	{
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');	
		$cont=0;	
		foreach($file_data as $row)
		{
				//echo $row['codigo'].'<br>';
			$IDU=$this->usuario->id_user();
			$match=0;

			$this->db->select('*')
			->from('inventario')
			->where('codigo', $row['codigo'])
			->where('id_usuario', $IDU);
			$item = $this->db->get()->row_array();
				//echo $item['id_inventario'].'-'.$row['rel'];
			if(($item['id_inventario']!='')&&($item['id_inventario']!=$row['rel'])){

				$this->db->select('*')
				->from('inventario_sustitutos')
				->where('id_inventario', $item['id_inventario'])
				->where('id_inventario_r1', $row['rel'])
				->where('id_usuario', $IDU,false);
				$rel1 = $this->db->get()->result_array();
				if(count($rel1)>0){ $match=1; }

				$this->db->select('*')
				->from('inventario_sustitutos')
				->where('id_inventario', $row['rel'])
				->where('id_inventario_r1', $item['id_inventario'])
				->where('id_usuario', $IDU);
				$rel2 = $this->db->get()->result_array();
				if(count($rel2)>0){ $match=1; }
				if($match==0){
					$cont++;
						//echo " -> SI<br>";
					$data2=[
						'id_inventario'  =>  $item['id_inventario'],
						'id_inventario_r1'  =>  $row['rel'],
						'id_usuario'=> $IDU
					];
					$this->db->insert('inventario_sustitutos', $data2);
				}
			}
		}
		echo $cont.'/'.count($file_data);
		$this->logSys->create(['controller'=>'Inventario', 'action'=>'Realizo Carga Masiva de: <strong>Inventario</strong>']);
			//redirect(site_url('inventario/read'));
	}


	public function inventario_super()
	{
		$IDU=$this->usuario->id_user();
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');
		foreach($file_data as $row)
		{
			$where=['referencia'=> $row['referencia'],'in.id_usuario'=>$IDU];
			$item=$this->inventario->read($where,false);
			$alm = $this->almacen->read(['where'=>['nombre'=> $row['almacen']]],false);
			if(isset($item)&&$item['id_inventario']!=null){
				$data=[
					'existencia'=>$row['existencia'],
					'descripcion'=>$row['descripcion'],
					'estatus'	=> 1,
					'codigo'=> $row['codigo'],
					'id_inventario_almacen'=>19
				];
				if($row['costo']!=''){ $data['costo']=$row['costo']; }
				$this->db->update('inventario', $data,['id_inventario'=>$item['id_inventario']]);
				$data2=[
					'cantidad'  =>  $row['existencia'],
					'costo'       =>  $row['costo'],
					'observacion'=> 'Actualizacion por Carga Masiva',
					'fecha_in'  => date('Y-m-d'),
					'id_usuario'=> $IDU,
					'referencia'=> 'N/A',
					'emisor'=>$IDU,
					'tipo' => 'A'
				];
				$this->db->insert('inventario_cargar', $data2);
				$models=explode(',', $row['modelos_asociados']);
				foreach($models as $mod){
					$auto=$this->marca_modelo->read(['where'=>['mo.modelo'=>trim($mod)]], false);
					if(isset($auto)&&$auto['id_modelo']!=null){
						$where=['where'=>["id_inventario" => $item['id_inventario'],'marca'=>$auto['id_marca'],'modelo'=>$auto['id_modelo']]];
						$asoc=$this->extra->read(['ina.*'],"inventario_asociados ina" ,null, $where,'result');
						if(count($asoc)==0){
							$this->db->insert('inventario_asociados', ["id_inventario" => $item['id_inventario'],'marca'=>$auto['id_marca'],'modelo'=>$auto['id_modelo'],'fecha_in'=>date('Y-m-d'), 'id_usuario'=>$IDU]);
						}
					}
				}

			}else{
				$data = array(
					'codigo'=>$row['codigo'],
					'referencia'=>$row["referencia"],
					'descripcion'=>$row["descripcion"],
					'fecha_in'=>date('Y-m-d'),
					'id_usuario'=>$IDU,
					'costo'=>$row['costo'],
					'existencia'=>$row['existencia'],
					'avatar'=>'core/assets/images/default_part.jpg',
					'existencia_minima'=>5,
					'estatus'=>1,
					'id_inventario_almacen'=>19
				);
				$this->db->insert('inventario',$data);
				$id=$this->db->insert_id();
				$data2=[
					'cantidad'  =>  $row['existencia'],
					'costo'       =>  $row['costo'],
					'observacion'=> 'Registro Inicial por Carga Masiva',
					'id_inventario'=> $id,
					'fecha_in'  => date('Y-m-d'),
					'hora' =>date('H:i'),
					'id_usuario'=> $this->session->IdUser,
					'referencia'=> $row["referencia"]
				];
				$this->db->insert('inventario_cargar', $data2);
				$models=explode(',', $row['modelos_asociados']);
				foreach($models as $mod){
					$auto=$this->marca_modelo->read(['where'=>['mo.modelo'=>trim($mod)]],false);
					if(isset($auto)&&$auto['id_modelo']!=null){
						$where=['where'=>["id_inventario" => $id,'marca'=>$auto['id_marca'],'modelo'=>$auto['id_modelo']]];
						$asoc=$this->extra->read(['ina.*'],"inventario_asociados ina" ,null, $where,'result');
						if(count($asoc)==0){
							$this->db->insert('inventario_asociados', ["id_inventario" => $id,'marca'=>$auto['id_marca'],'modelo'=>$auto['id_modelo'],'fecha_in'=>date('Y-m-d'), 'id_usuario'=>$IDU]);
						}
					}
				}

			}
		}
		$this->logSys->create(['controller'=>'Inventario', 'action'=>'Realizo Carga Masiva de: <strong>Inventario</strong>']);
		redirect(site_url('inventario/read'));
	}


	public function conteo()
	{
		$IDU=$this->usuario->id_user();
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');
		$i=0;
		$j=0;
		$sc=0;
		$nid=0;
		$ok=0;
		$pa=0;
		$chk=0;
		$post=$this->input->post();
		$hora=$post['hora'];
		$fecha=$post['fecha'];
		foreach($file_data as $row)
		{
			$row['existencia']=number_format($row['existencia'], 2, '.', '');
			if($row['codigo']!=''){
				$row['codigo']=str_replace('-', '', $row['codigo']);
				$where=['referencia'=> $row['referencia'], 'codigo'=>$row['codigo'],'estatus'=>1];
				$item=$this->inventario->read($where,false);
                if($item['costo']==null){$item['costo']=0;}
                if(isset($item['id_inventario'])&&$item['id_inventario']!=null){
                	if($row['existencia']!=$item['existencia']){
                		$check=0;
                		$ins=$this->inventario->read_cargas(['ic.id_inventario'=>$item['id_inventario'], 'ic.fecha_in>='=>$fecha, 'hora>='=>$hora],'result');
                		$Cins=0;
                		$Csum=0;
                		if(count($ins)>0){
                			foreach ($ins as $carga) {
                				$Cins+=$carga['cantidad'];
                			}
                		}
                		$sum=$this->inventario->read_suministro(['s.id_inventario'=>$item['id_inventario'],'s.fecha_out>='=>$fecha, 's.hora_out>='=>$hora],'result');
                		if(count($sum)>0){                			
                			foreach ($sum as $salida) {
                				$Csum+=$salida['despachado'];
                			}                	
                		}
                		$total=$row['existencia']+$Cins-$Csum;
                	
                		if($total==$item['existencia']){
                			$row['existencia']=$total;
                			$check=1;
                		}else{
                			if($row['existencia']>$total){
                				$row['existencia']=$total;
                			}
                		}	
                		$j++;			            
                		$pa++;
		            }else{
		            	$check=1;
		            	$chk++;
		            }
		            $data3=[
	            		'conteo'=>$row['existencia'],
	            		'exist'=>$item['existencia'],
	            		'id_inventario'=>$item['id_inventario'],
	            		'check'=>$check,
	            		'fecha'=>date('Y-m-d'),
	            		'hora'=>date('H:i'),
	            		'id_us'=>$post['id_us'],
	            		'nro'=>1
	            	];
		            $contado=$this->inventario->readCnt(['c.id_inventario'=>$item['id_inventario'],'c.fecha'=>$fecha],false);
		            if($contado['id_conteo']!=null){
		            	if($row['existencia']!=$contado['conteo']){
		            		$this->db->update('conteos', $data3,['id_conteo'=>$contado['id_conteo']]);	
		            	}
		            }else{
		            	$this->db->insert('conteos', $data3);	
		            }
	            	
	            	$data=[
	            		'fecha_cont'=>date('Y-m-d')
	            	];
	            	
	            	$ok++;
	            	$this->db->update('inventario', $data,['id_inventario'=>$item['id_inventario'], 'estatus'=>1]);
	            	$this->logSys->create(['controller'=>'Inventario', 'action'=>'Actualizo por Conteo de <strong>'.$row['referencia'].' '.$row['descripcion'].'</strong>']);	            
	            }
	        }
	        $i++;
	    }
	    $date= date('Y-m-d');
	   
	    redirect(site_url('inventario/listcont/').'v?u=&x=&b=&date='.$date);
	}

	public function conteoUndo($date){
		$IDU=$this->usuario->id_user();
		$this->db->select('*')
		->from('conteos')
		->where('fecha', $date);
		$conteo=$this->db->get()->result_array();
		echo count($conteo).'<br>';
		$i= 1;
		foreach ($conteo as $item) {
			$this->db->select('referencia')
			->from('inventario')
			->where('id_inventario', $item['id_inventario']);
			$parte=$this->db->get()->row_array();
			$data['id_inventario']=$item['id_inventario'];
			$this->db->update('inventario_cargar', $data,['fecha_in'=>$date, 'id_usuario'=>$IDU,'referencia'=>$parte['referencia']]);
			echo $i.' OK<br>'; $i++;
		}
	}


	public function choferes()
		{
			$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');		
			foreach($file_data as $row)
			{
				if($this->session->Parent==0){
					$IDU=$this->session->IdUser;
				}else{
					$IDU=$this->session->Parent;
				}
				$this->db->select('*')
				->from('choferes')
				->where('cedula', $row['cedula'])
				->where('id_usuario', $IDU,false);
				$item = $this->db->get()->row_array();
				if(isset($item)&&$item['id_chofer']!=null){
					$data=[
						'cargo'=>$row['cargo'],
						'codigo'=>$row['codigo'],
						'nombre'=>$row['nombre'],
						'telefono'=>$row['telefono']
					];
					$this->db->update('choferes', $data,['cedula'=>$row['cedula']]);
				}else{
					$data = array(
						'cedula'=>$row['cedula'],
						'cargo'=>$row["cargo"],
						'nombre'=>$row["nombre"],
						'id_usuario'=>$IDU,
						'codigo'=>$row['codigo'],
						'telefono'=>$row['telefono']
					);
					$this->db->insert('choferes',$data);
				}
			}
			$this->logSys->create(['controller'=>'Choferes', 'action'=>'Realizo Carga Masiva de: <strong>Choferes</strong>']);
			redirect(site_url('choferes/read'));
		}

		function vehiculos()
		{
			$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');		
			foreach($file_data as $row)
			{
				if($this->session->Parent==0){
					$IDU=$this->session->IdUser;
				}else{
					$IDU=$this->session->Parent;
				}

				$this->db->select('*')
				->from('autos_tipos')
				->where('tipo', $row['tipo'])
				->where('id_usuario', $IDU);
				$tipo = $this->db->get()->row_array();
				if(isset($tipo)&&$tipo['id_tipo']!=null){
					$id_tipo=$tipo['id_tipo'];
				}else{
					$this->db->insert('autos_tipos',['tipo'=>$row['tipo'], 'id_usuario'=>$IDU]);
					$id_tipo=$this->db->insert_id();
				}

				$this->db->select('*')
				->from('marcas')
				->where('marca', $row['marca'])
				->where('id_usuario', $IDU);
				$marca = $this->db->get()->row_array();
				if(isset($marca)&&$marca['id_marca']!=null){
					$id_marca=$marca['id_marca'];
				}else{
					$this->db->insert('marcas',['marca'=>$row['marca'], 'id_usuario'=>$IDU]);
					$id_marca=$this->db->insert_id();
				}

				$this->db->select('*')
				->from('modelos')
				->where('modelo', $row['modelo'])
				->where('id_usuario', $IDU);
				$modelo = $this->db->get()->row_array();
				if(isset($modelo)&&$modelo['id_modelo']!=null){
					$id_modelo=$modelo['id_modelo'];
				}else{
					$this->db->insert('modelos',['modelo'=>$row['modelo'], 'id_marca'=>$id_marca, 'id_usuario'=>$IDU]);
					$id_modelo=$this->db->insert_id();
				}

				$this->db->select('*')
				->from('autos')
				->where('placa', $row['placa'])
				->where('id_usuario', $IDU,false);
				$item = $this->db->get()->row_array();
				if(isset($item)&&$item['id_auto']!=null){
					$data=[
						'placa'=>$row['placa'],
						'id_usuario'=>$IDU,
						'flota'=>$row["flota"],
						'marca'=>$id_marca,
						'ano'=>$row['anno'],
						'modelo'=>$id_modelo,
						'estatus'=>1,
						'tipo'=>$id_tipo,
						'serial_carroceria'=>$row['serial_carroceria'],
						'serial_motor'=>$row['serial_motor'],
						'color'=>$row['color']
						//'transmision'=>$row['transmision']
						//'observacion'=>$row['observacion']
					];
					$this->db->update('autos', $data,['placa'=>$row['placa']]);
				}else{
					$data = array(
						'placa'=>$row['placa'],
						'flota'=>$row["flota"],
						'marca'=>$id_marca,
						'ano'=>$row['anno'],
						'id_usuario'=>$IDU,
						'estatus'=>1,
						'modelo'=>$id_modelo,
						'tipo'=>$id_tipo,
						'serial_carroceria'=>$row['serial_carroceria'],
						'serial_motor'=>$row['serial_motor'],
						'certif_reg'=>$row['registro'],
						'color'=>$row['color'],
						'capacidad'=>$row['vol'],
						'disp'=> 'n'
						//'transmision'=>$row['transmision']
						//'observacion'=>$row['observacion']
					);
					$this->db->insert('autos',$data);
				}
			}
			$this->logSys->create(['controller'=>'Vehiculos', 'action'=>'Realizo Carga Masiva de: <strong>Vehiculos</strong>']);
			redirect(site_url('vehiculo/read'));
		}

		function modelo_neum()
		{
			$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');		
			foreach($file_data as $row)
			{
				$IDU=$this->usuario->id_user();
				
				$this->db->select('*')
				->from('marcas_n')
				->where('marca', $row['marca']);
				$marca = $this->db->get()->row_array();
				if(isset($marca)&&$marca['id_nmarca']!=null){
					$id_marca=$marca['id_nmarca'];
				}else{
					$this->db->insert('marcas_n',['marca'=>$row['marca']]);
					$id_marca=$this->db->insert_id();
				}

				$this->db->select('*')
				->from('modelos_n')
				->where('modelo', $row['modelo']);
				$modelo = $this->db->get()->row_array();


				$this->db->select('*')
				->from('bandas_n')
				->where('banda', $row['banda']);
				$banda = $this->db->get()->row_array();
				if(isset($banda)&&$banda['id_banda']!=null){
					$id_banda=$banda['id_banda'];
				}else{
					$this->db->insert('bandas_n',['banda'=>$row['banda']]);
					$id_banda=$this->db->insert_id();
				}

				if(isset($modelo)&&$modelo['id_modelo']!=null){
					$id_modelo=$modelo['id_modelo'];
					$data=[
						'banda'=>$id_banda,
						'id_usuario'=>$IDU,
						'id_marca'=>$id_marca,
						'tipo'=>'Radial',
						'uso'=>'Mixto',
						'km_ini'=>$row['km'],
						'km_min'=>$row['km_min'],
						'observacion'=>$row['observacion']
					];
				}else{
					$data=[
						'placa'=>$row['placa'],
						'id_usuario'=>$IDU,
						'marca'=>$id_marca,
						'ano'=>$row['anno'],
						'modelo'=>$id_modelo,
						'estatus'=>1,
						'tipo'=>21,
						'observacion'=>$row['observacion']
					];
					$this->db->insert('modelos',['modelo'=>$row['modelo'], 'id_marca'=>$id_marca]);
					$id_modelo=$this->db->insert_id();
				}
				
			}
			$this->logSys->create(['controller'=>'Neumatico', 'action'=>'Realizo Carga Masiva de: <strong>Neumaticos</strong>']);
			redirect(site_url('neumatico/read'));
		}

		function modif()
		{

			$IDU=$this->usuario->id_user();
			

			$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');	
			//echo count($file_data);	
			//var_dump($file_data);
			foreach($file_data as $row)
			{
				$n=0;
				$item = $this->vehiculo->read(['placa'=> $row['placa'],'au.id_usuario'=>$IDU],false);	
				//$this->db->update('autos', ['estatus'=>2],['placa'=> $row['placa'],'id_usuario'=>$IDU]);
				echo $row['placa'];
				//echo "<br>".$status;
				//echo "-".$item["id_auto"];
				if(!is_null($item)){
					$n++;
					echo $n.' si ';
					/*$data=[
						'id_usuario'=>$IDU,
						'placa'=>$row['Placa'],
						'estatus'=>$status,
						'capacidad'=>$row['vol'],
						'disp'=>1,
						'certif_reg'=>$row['Certificado'],
						'serial_motor'=>$row['serial_motor'],
						'serial_carroceria'=>$row['serial_carroceria']
					];
					$this->db->update('autos', $data,['flota'=>$row['Flota'],'id_usuario'=>$IDU]);
					echo "Actualizado- ";
					//$this->$this->db->update('choferes', ['id_auto'=>$item['id_auto']],['nombre like'=>'%'.$row['Chofer'].'%','id_master'=>$IDU]);
					*/
					/*if($status==2):
						echo "crear orden";
						$now=date('Y-m-d');
						$fecha_in=date("Y-m-d",strtotime($now."- ".$row['Dias']." days"));
						$ordtipo=$this->orden_trabajo->read(['ot.tipo like'=>'%'.$row['Observacion'].'%'],'row');
						$orden=$this->orden_trabajo->read(['ot.id_auto'=>$item['id_auto'],'ot.estatus'=>"ABIERTA"],'row');
						$dateord=count_days($orden['fecha_in'],date('Y-m-d'));
						$neword=count_days($fecha_in,date('Y-m-d'));


						echo $dateord."==".$neword;

						if(($dateord-$neword)>2):
							//$this->db->update('ordenes',['estatus'=>"CERRADA"], ['id_orden'=>$orden['id_orden']]);
							 $last = $this->orden_trabajo->read(null,'row',['nro_orden'=>'desc'],null,[1=>0]);
							$tipo=$row["Observacion"]=="MOTOR"?"MOTOR":"Reparacion General";
							//$taller=$this->taller->read(['nombre like'=>"%".$row['Taller']."%"],false);
						
								$data_ord=[
									'id_auto'=>$item['id_auto'],
									'fecha_in'=>$fecha_in,
									'tipo'=>$tipo,
									'estatus'=>"ABIERTA",
									'descripcion_1'=>$row['Observacion'],
									//'id_taller'=>@$taller['id_taller'],
									'nro_orden' => $last['nro_orden']+1,
									'id_usuario'=>$IDU
								];
								$this->db->insert('ordenes',$data_ord);
								echo "orden creada";
						endif;
					endif;*/

				}
			}
			//redirect(site_url('vehiculo/read'));
		}

		function cargadata()
		{
				$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');		
				
				if($this->session->Parent==0){
					$IDU=$this->session->IdUser;
				}else{
					$IDU=$this->session->Parent;
				}
				$a=0;
				foreach($file_data as $row)
				{
					$this->db->select('*')
					->from('choferes')
					->where('nombre like ', $row['chofer'])
					->where('id_usuario', $IDU,false);
					$item = $this->db->get()->row_array();
					$this->db->select('*')
					->from('choferes')
					->where('nombre like ', $row['chofer'])
					->where('id_usuario', $IDU,false);
					$item = $this->db->get()->row_array();
					if(isset($item)&&$item['id_tienda']!=null){
						$upd=[
							'flota'	=>	$row['horas'],
							'salida'	=>	$row['salida'],
							'id_zona'	=>	$row['Zona']
						];
						$this->db->update('tienda', $upd, ['id_tienda'=>$item['id_tienda']]);
						$this->db->delete('tiendasemana',['id_tienda'=>$item['id_tienda']]);
					}else{
						$ins=['nombre'=>$row['Tienda'],'id_usuario'=>$IDU,'t_viaje'=>$row['horas'],'salida'=>$row['salida'],'id_zona'=>$row['Zona']];
						$this->db->insert('tienda', $ins);
						$item['id_tienda']=$this->db->insert_id();
					}

				}
		}

		function actual_carga_ruta(){
			$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');	
			$IDU=$this->usuario->id_user();
			foreach($file_data as $row)
				{
					var_dump($row);
					$datar=['ruta'=>$row['Rutas'],'km'=>$row['km'],'trailer'=>$row['trailer'],'id_usuario'=>$IDU ];
					$this->db->insert('rutas', $datar);
					$new=$this->db->insert_id();

										
					if($row['Tienda 1']!=''){
						$this->db->update('tienda',['id_ruta'=>$new],['nombre'=>$row['Tienda 1']]);
					}
					if($row['Tienda 2']!=''){
						$this->db->update('tienda',['id_ruta'=>$new],['nombre'=>$row['Tienda 2']]);
					}
					if($row['Tienda 3']!=''){
						$this->db->update('tienda',['id_ruta'=>$new],['nombre'=>$row['Tienda 3']]);
					}
					if($row['Tienda 4']!=''){
						$this->db->update('tienda',['id_ruta'=>$new],['nombre'=>$row['Tienda 4']]);
					}
					if($row['Tienda 5']!=''){
						$this->db->update('tienda',['id_ruta'=>$new],['nombre'=>$row['Tienda 5']]);
					}
					if($row['Tienda 6']!=''){
						$this->db->update('tienda',['id_ruta'=>$new],['nombre'=>$row['Tienda 6']]);
					}
				}
				redirect(site_url('rutas/read'),'refresh');
		}

		function actual_ordenes(){
			$ordenes=$this->orden_trabajo->read(['ot.estatus'=>"ABIERTA"],'result');
			foreach ($ordenes as $orden) {
				$auto=$this->vehiculo->read(['au.id_auto'=>$orden["id_auto"]],false);
				if($auto['estatus']==1){
					$this->db->update('ordenes',["estatus"=>"CERRADA"],['id_orden'=>$orden['id_orden']]);
				}
				
			}
			
		}

		function actual_carga_t()
		{
				$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');		
				
				if($this->session->Parent==0){
					$IDU=$this->session->IdUser;
				}else{
					$IDU=$this->session->Parent;
				}
				$a=0;
				foreach($file_data as $row)
				{
					$this->db->select('*')
					->from('tienda')
					->where('nombre like ', $row['Tienda'])
					->where('id_usuario', $IDU,false);
					$item = $this->db->get()->row_array();
					//print_r($this->db->last_query());
					//var_dump($item);
					if(isset($item)&&$item['id_tienda']!=null){
						$upd=[
							't_viaje'	=>	$row['horas'],
							'salida'	=>	$row['salida'],
							'id_zona'	=>	$row['Zona']
						];
						$this->db->update('tienda', $upd, ['id_tienda'=>$item['id_tienda']]);
						$this->db->delete('tiendasemana',['id_tienda'=>$item['id_tienda']]);
					}else{
						$ins=['nombre'=>$row['Tienda'],'id_usuario'=>$IDU,'t_viaje'=>$row['horas'],'salida'=>$row['salida'],'id_zona'=>$row['Zona']];
						$this->db->insert('tienda', $ins);
						$item['id_tienda']=$this->db->insert_id();
					}
					$data=['id_tienda'=>$item['id_tienda'],'id_usuario'=>$IDU];
					if($row['LUNES']=='s'){
						$data['day']=1;
						$this->db->insert('tiendasemana',$data);
						unset($data['day']);
					}
					if($row['MARTES']=='s'){
							$data['day']=2;
							$this->db->insert('tiendasemana',$data);
							unset($data['day']);
					}
					if($row['MIERCOLES']=='s'){
							$data['day']=3;
							$this->db->insert('tiendasemana',$data);
							unset($data['day']);
					}
					if($row['JUEVES']=='s'){
							$data['day']=4;
							$this->db->insert('tiendasemana',$data);
							unset($data['day']);
					}
					if($row['VIERNES']=='s'){
						$data['day']=5;
						$this->db->insert('tiendasemana',$data);
						unset($data['day']);
					}
					if($row['SABADO']=='s'){
						$data['day']=6;
						$this->db->insert('tiendasemana',$data);
							unset($data['day']);
						}
						$a++;
				}
				//echo "tiendas agregadas: ".$a;
				$this->logSys->create(['controller'=>'Vehiculos', 'action'=>'Realizo Carga Masiva de plan horario tiendas']);
				redirect(site_url('despacho/ola'));
		}

		function actual_carga_v()
		{
			$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');		
			$IDU=$this->usuario->id_user();
			$a=0;
			foreach($file_data as $row)
			{
				
				$auto = $this->vehiculo->read(['flota'=>$row['flota']],false);
				$ruta = $this->ruta->read(['ruta'=>$row['ruta']],false);
				$this->db->delete('planruta',['id_auto'=>$auto['id_auto']]);
				$this->db->delete('plansemana',['id_auto'=>$auto['id_auto']]);

				$data=[ 'id_auto'=> $auto['id_auto'], 'id_ruta'=> $ruta['id_ruta'],	'puerta'=> $row['puerta']] ;
				$datap=[ 'id_auto'=> $auto['id_auto'],'id_usuario'=>$IDU];
				if($row['LUNES']=='s'){
					$data['day']=1;
					$datap['day']=1;
					$this->db->insert('planruta',$data);
					$this->db->insert('plansemana',$datap);
					unset($data['day']);
				}
				if($row['MARTES']=='s'){
					$data['day']=2;
					$datap['day']=2;
					$this->db->insert('planruta',$data);
					$this->db->insert('plansemana',$datap);
					unset($data['day']);
				}
				if($row['MIERCOLES']=='s'){
					$data['day']=3;
					$datap['day']=3;
					$this->db->insert('planruta',$data);
					$this->db->insert('plansemana',$datap);
					unset($data['day']);
				}
				if($row['JUEVES']=='s'){
					$data['day']=4;
					$datap['day']=4;
					$this->db->insert('planruta',$data);
					$this->db->insert('plansemana',$datap);
					unset($data['day']);
				}
				if($row['VIERNES']=='s'){
					$data['day']=5;
					$datap['day']=5;
					$this->db->insert('planruta',$data);
					$this->db->insert('plansemana',$datap);
					unset($data['day']);
				}
				if($row['SABADO']=='s'){
					$data['day']=6;
					$datap['day']=6;
					$this->db->insert('planruta',$data);
					$this->db->insert('plansemana',$datap);
					unset($data['day']);
				}
				$a++;
			}
			
		
			//echo "rutas agregadas: ".$a;
			$this->logSys->create(['controller'=>'Vehiculos', 'action'=>'Realizo Carga Masiva de plan de rutas']);
			redirect(site_url('plansemana/read'));
		}

		function ola()
		{
				$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');		
				$IDU=$this->usuario->id_user();
				$a=0;
				foreach($file_data as $row)
				{
					if($row['puerta']!=null){
						$this->db->delete('ola',['id_usuario'=>$IDU,'day'=>$row['day'],'puerta'=>$row['puerta']]);
						for($i=1;$i<=8;$i++){

							$this->db->select('*')
							->from('tienda')
							->where('nombre ', $row['tienda'.$i])
							->where('id_usuario', $IDU,false);
							$tienda = $this->db->get()->row_array();
							$data=[
								'id_tienda'=> $tienda['id_tienda'],
								'lote'=> $i,
								'day'	=> $row['day'],
								'puerta'=> 'P-'.$row['puerta'],
								'id_usuario'=> $IDU
							];
							$this->db->insert('ola', $data);
						}					
						
						$a++;
					}
				}
					//echo "rutas agregadas: ".$a;
				$this->logSys->create(['controller'=>'Vehiculos', 'action'=>'Realizo Carga Masiva de plan de rutas']);
				redirect(site_url('despacho/ola'));
		}


		function siniestro()
		{

			if($this->session->Parent==0){
				$IDU=$this->session->IdUser;
			}else{
				$IDU=$this->session->Parent;
			}

			$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');		
			foreach($file_data as $row)
			{
				
				$this->db->select('*')
				->from('causas')
				->where('causa', $row['causa']);
				$causa = $this->db->get()->row_array();
				if(isset($causa)&&$causa['id_causas']!=null){
					$id_causa=$causa['id_causas'];
				}else{
					$this->db->insert('causas',['causa'=>$row['causa']]);
					$id_causa=$this->db->insert_id();
				}

				$this->db->select('*')
				->from('choferes')
				->where('nombre', $row['chofer']);
				$chofer = $this->db->get()->row_array();
				if(isset($chofer)&&$chofer['id_chofer']!=null){
					$id_chofer=$chofer['id_chofer'];
				}else{
					$this->db->insert('choferes',['nombre'=>$row['chofer'], 'id_usuario'=>$IDU]);
					$id_chofer=$this->db->insert_id();
				}


				$this->db->select('*')
				->from('autos')
				->where('placa', $row['placa'])
				->where('id_usuario', $IDU);
				$item = $this->db->get()->row_array();

				$report=$this->accidente->read(['YEAR(fecha)='=>date('Y')]);
					//echo count($report).' ';
				$ye=count($report)+1;
				$codigo=date('Y').'-'.$ye;
					//echo $codigo.'<br>';
				$acc=$this->accidente->read(['id_auto'=>$item['id_auto'],'id_usuario'=>$IDU,'fecha'=>nice_date($row['fecha_ocurrencia'], 'Y-m-d')],'row');

				$costo=floatval(str_replace(',', '.', $row['costo']));
				$indem=floatval(str_replace(',', '.', $row['indemnizado']));
				if(isset($item)&&$item['id_auto']!=null){
					if(isset($acc)&&$acc['id_accidente']!=null){
						$data=[
							'id_auto'=>$item['id_auto'],
							'heridos'=>$row['heridos'],
							'grua'=>$row['grua'],
							'taller'=>$row['taller'],
							'fch_not_int'=>nice_date($row['F_N_INTER'], 'Y-m-d'),
							'fch_not_cia'=>nice_date($row['F_N_CIA'], 'Y-m-d'),
							'codigo'=>$row['codigo'],
							'lugar'=>$row['lugar'],
							'causa'=>$id_causa,
							'presupuesto'=>$row['presupuesto'],
							'desc'=>$row['descripcion'],
							'fch_insp'=>nice_date($row['fecha_inspeccion'], 'Y-m-d'),
							'costo'=>$costo,
							'indemnizado'=>$indem,
							'perdida'=>$row['perdida'],
							'fecha_rep'=>nice_date($row['fecha_rep'], 'Y-m-d'),
							'obs'=>$row['observaciones'],
							'status'=>$row['status']
						];
						$this->db->update('accidentes', $data, ['id_accidente'=>$acc['id_accidente']]);
					}else{
						$data=[
							'id_auto'=>$item['id_auto'],
							'id_chofer'=>$id_chofer,
							'id_usuario'=>$IDU,
							'fecha'=>nice_date($row['fecha_ocurrencia'], 'Y-m-d'),
							'heridos'=>$row['heridos'],
							'grua'=>$row['grua'],
							'taller'=>$row['taller'],
							'fch_not_int'=>nice_date($row['F_N_INTER'], 'Y-m-d'),
							'fch_not_cia'=>nice_date($row['F_N_CIA'], 'Y-m-d'),
							'codigo'=>$row['codigo'],
							'lugar'=>$row['lugar'],
							'causa'=>$id_causa,
							'presupuesto'=>$row['presupuesto'],
							'desc'=>$row['descripcion'],
							'fch_insp'=>nice_date($row['fecha_inspeccion'], 'Y-m-d'),
							'costo'=>$costo,
							'indemnizado'=>$indem,
							'perdida'=>$row['perdida'],
							'fecha_rep'=>nice_date($row['fecha_rep'], 'Y-m-d'),
							'obs'=>$row['observaciones'],
							'status'=>$row['status']
						];
						$this->db->insert('accidentes', $data);
					}
				}
			}

				//redirect(site_url('accidente/read'));
		}


		
		function inventario_match()
		{
			$IDU=$this->usuario->id_user();
			$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');
			$match=0;
			$ref=0;
			$n=0;
			$data=[];
			$data['list']=[];
			foreach($file_data as $row)
			{
				$n++;		            
				$row['cambio']='';
		            //$ubc=explode('-', $row['ubicacion']);
				$c='';
				if($row['ubicacion']==null){
					$row['ubicacion']='AAAA';
				}
		            //array_push($data['list'], $row);
		            //$ref++;
				$where=['descripcion'=> $row['descripcion'],'in.id_usuario'=>$IDU];
				$item=$this->inventario->read($where,false);
				$row['cambio']='IGUAL';
				if(isset($item)&&$item['id_inventario']!=null){
					if($item['referencia']!= $row['ubicacion']){ $row['cambio']='UBiCACION'; }
					$ref++;
				}else{
					$row['cambio']='NUEVO';
					$match++;
				}
				array_push($data['list'], $row);

			}
			$this->load->view('ui/head');


		        //$data['list'] = $file_data;
			$data['total']=$n;
			$data['total_match']=$match;
			$data['total_rows']=$ref;
			$this->load->view('Import/match',$data);

			$this->load->view('ui/footer');
			echo 'por descripcion: '.$match;
		        //echo '<br>por Ubicacion: '.$ref;

		}

	function inventario_clean()
	{
		$IDU=$this->usuario->id_user();
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"],false,false,false,';');
		$match=0;
		$ref=0;
		$n=0;
		$data=[];
		$data['list']=[];
		foreach($file_data as $row)
		{
			$n++;
			$row['modelos_asociados']='';
			$c='';
			if($row['ubicacion']==null){
				$row['ubicacion']='AAAA';
			}
			$row['cambio']='IGUAL';
			$where=['descripcion'=> $row['descripcion'],'in.id_usuario'=>$IDU];
			$item=$this->inventario->read($where,false);
			if(isset($item)&&$item['id_inventario']!=null){
				if($item['referencia']!= $row['ubicacion']){ $row['cambio']='UBiCACION'; }
				$ref++;
			}else{
				$row['cambio']='NUEVO';
				$match++;
			}
			$tipo=explode('NEW STRALIS', strtoupper($row['descripcion']));
			if(count($tipo)>1){
				$row['descripcion']=str_replace('NEW STRALIS', '', strtoupper($row['descripcion']));
				$row['modelos_asociados'].='NEW STRALIS';
				$c=',';
			}
			$tipo=explode('STRALIS', strtoupper($row['descripcion']));
			if(count($tipo)>1){
				$row['descripcion']=str_replace('STRALIS', '', strtoupper($row['descripcion']));
				$row['modelos_asociados'].=$c.'STRALIS';
				$c=',';
			}
			$tipo=explode('TECTOR', strtoupper($row['descripcion']));
			if(count($tipo)>1){
				$row['descripcion']=str_replace('TECTOR', '', strtoupper($row['descripcion']));
				$row['modelos_asociados'].=$c.'TECTOR';
				$c=',';
			}
			$tipo=explode('EUROCARGO', strtoupper($row['descripcion']));
			if(count($tipo)>1){
				$row['descripcion']=str_replace('EUROCARGO', '', strtoupper($row['descripcion']));
				$row['modelos_asociados'].=$c.'TECTOR';
				$c=',';
			}
			$tipo=explode('EUROTECH', strtoupper($row['descripcion']));
			if(count($tipo)>1){
				$row['descripcion']=str_replace('EUROTECH', '', strtoupper($row['descripcion']));
				$row['modelos_asociados'].=$c.'EUROTECH';
				$c=',';
			}
			$tipo=explode('TECH', strtoupper($row['descripcion']));
			if(count($tipo)>1){
				$row['descripcion']=str_replace('TECH', '', strtoupper($row['descripcion']));
				$row['modelos_asociados'].=$c.'EUROTECH';
				$c=',';
			}
			$tipo=explode('EURO', strtoupper($row['descripcion']));
			if(count($tipo)>1){
				$row['descripcion']=str_replace('EURO', '', strtoupper($row['descripcion']));
				$row['modelos_asociados'].=$c.'TECTOR';
				$c=',';
			}
			$tipo=explode('DAILY', strtoupper($row['descripcion']));
			if(count($tipo)>1){
				$row['descripcion']=str_replace('DAILY', '', strtoupper($row['descripcion']));
				$row['modelos_asociados'].=$c.'DAILY';
				$c=',';
			}
			$tipo=explode('TRAKKER', strtoupper($row['descripcion']));
			if(count($tipo)>1){
				$row['descripcion']=str_replace('TRAKKER', '', strtoupper($row['descripcion']));
				$row['modelos_asociados'].=$c.'TRAKKER';
				$c=',';
			}
			$tipo=explode('TRAKER', strtoupper($row['descripcion']));
			if(count($tipo)>1){
				$row['descripcion']=str_replace('TRAKER', '', strtoupper($row['descripcion']));
				$row['modelos_asociados'].=$c.'TRAKKER';
				$c=',';
			}
			array_push($data['list'], $row);
			}
		$this->load->view('ui/head');

        //$data['list'] = $file_data;
		$data['total']=$n;
		$data['total_match']=$match;
		$data['total_rows']=$ref;
		$this->load->view('Import/clean',$data);
		$this->load->view('ui/footer');
		echo 'por descripcion: '.$match;
        //echo '<br>por Ubicacion: '.$ref;
		}
	}
