<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->Online==false) {
			redirect('welcome','refresh');
		}
		$this->folder = $this->uri->segment(1).'/'.$this->uri->segment(2);

	}

	// List all your items
	public function index()
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$this->cart->destroy();
		$this->load->view('ui/head');
		$data['list'] = $this->plan->read();
		$this->load->view($this->folder.'/read',$data);
		$this->load->view('ui/footer');
	}
	public function presup()
	{
		
		$this->cart->destroy();
		$this->load->view('ui/head');
		$data['list'] = $this->plan->read();
		$this->load->view('plan/presupuesto',$data);
		$this->load->view('ui/footer');
	}

	// List all your items
	public function create($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
		$this->form_validation->set_rules('titulo', 'Titulo', 'required');
		$this->form_validation->set_rules('descripcion', 'Descripcion', 'required');
		if ($this->form_validation->run() == TRUE) {
			$this->plan->create();
		} else {
			$this->load->view('ui/head');
			$data['edit'] = $this->plan->read(['where'=>['id_plan'=>$id]],false);
			$this->load->view($this->folder,$data);
			$this->load->view('ui/footer');
		}
	}

	// Add a new item
	public function read($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$this->cart->destroy();
		$this->load->view('ui/head');
		$data['list'] = $this->plan->read();
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}


	public function view($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$this->cart->destroy();
		$data['view'] = $this->plan->read(['where'=>['id_plan'=>$id]],false);
		if ($data['view']==null) {
			redirect('plan/read');
		}

		$data['config']=$this->plan->read_config(['where'=>['id_plan'=>$id]]);
		$data['rutinas']=$this->tempario->read_rutina(['tr.id_plan'=>$id],true,['order_by'=>['ord','asc']]);
		$data['rut_sum']=$this->tempario->read_rutina(['tr.id_plan'=>$id,'ts.sumin IS NOT NULL'=>NULL],true,['order_by'=>['ord','asc']]);

		$this->load->library('cart');
		$this->load->view('ui/head');
		$data['list'] = $this->inventario->read();
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}

	//Update one item
	public function edit( $id = NULL,$cancel=null )
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
		$this->load->library('cart');
		$this->cart->product_name_rules = "\/\#\~\ñ\Ñ\.\:\-\_\"\' a-z0-9";
		if($cancel==1){
			$this->cart->destroy();
			redirect('plan/view/'.$id);
		} 
		$this->load->view('ui/head');
		$post=$this->input->post();
		//echo var_dump($post);
		if($post!=null){
			$post['descripcion']= str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>",$post['descripcion']);
			$this->plan->update($id,$post);
			$this->cart->destroy();
			redirect('plan/view/'.$id);

		}else{
			$data['view'] = $this->plan->read(['where'=>['id_plan'=>$id]],false);
			if ($data['view']==null) {
				redirect('plan/read');
			}
			$data['config']=$this->plan->read_config(['where'=>['id_plan'=>$id]]);
			$this->load->view($this->folder,$data);
			$this->load->view('ui/footer');
		}
	}

	public function read_suministro($id=null,$mo=null){
		$item_total = 0;
		$pln=$this->plan->read_config(['where'=>['id_pconfig'=>$id]],false);
		$plan=$this->plan->read(['where'=>['id_plan'=>$pln['id_plan']]],false);
		//echo $pln['id_tempario'];
		$tempario=$this->tempario->read(['where'=>['id_tempario_servicio'=>$pln['id_tempario']]], false);
		//var_dump($tempario);
		$t=explode(".", $tempario['horas']);
		if(is_null(isset($t[1]))){ $t[1]=0;}
		$h=$t[0]*60; $costo=(($h+$t[1])/60)*$tempario['costo_mo'];
		foreach ($this->plan->read_suministro(['id_plan'=>$id]) as $item):
			$item_total += ($item['costo_div']*$item['cantidad']);
			echo '<tr>
			<td class="text-center">1</td>
			<td>'.strtoupper($item['codigo']).'</td>
			<td class="text-right">'.$item['descripcion'].'</td>
			<td class="text-right">'.$item['cantidad'].'</td>
			<td class="text-right">'.money($item['costo_div'],2).'</td>
			<td class="text-right">'.money($item['costo_div']*$item['cantidad'],2).'</td>
			</tr>';
		endforeach;
		echo '<tr>
		<td class="text-center"></td>
		<td></td>
		<td class="text-right">Total Suministros</td>
		<td class="text-right"></td>
		<td class="text-right"></td>
		<td class="text-right">$'.money($item_total,2).'</td>
		</tr>';
		if($mo==1){
			echo '<tr>
			<td class="text-center"></td>
			<td></td>
			<td class="text-right">Mano de Obra '.$plan['short'].'</td>
			<td class="text-right"></td>
			<td class="text-right"></td>
			<td class="text-right">$'.money($costo,2).'</td>
			</tr>';
			echo '<tr>
			<td class="text-center"></td>
			<td></td>
			<td class="text-right font-weight-bold text-dark"><b>TOTAL</b></td>
			<td class="text-right"></td>
			<td class="text-right"></td>
			<td class="text-right font-weight-bold text-dark">$'.money($item_total+$costo,2).'</td>
			</tr>';
		}

	}

	//Delete one item
	public function delete( $id = NULL )
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
	}

	public function read_parte()
	{
		foreach ($this->cart->contents() as $item) {
			$idForm='updPart'.$item['id'];
			$cnt=$item['qty']-1;
			if($item['qty']>$item['limit']){ $item['qty']=$item['limit']; }
			echo "<tr><td>123".$item['cod'];
			echo form_hidden('cantidad[]', $item['qty']);
			echo form_hidden('id_inventario[]', $item['id']);
			echo "</td>
			<td>".$item['name']."</td>
			<td width='50px'>";
			echo form_open("plan/update_parte/",['id'=>$idForm]).form_hidden('id', $item['rowid']);
			echo form_input(array('name'=>'qty','value'=>$item['qty'], 'size'=>5, 'class'=>'form-control maxnum','type'=>'number','max'=>$item['limit'], 'min'=>1, 'onchange'=>'add_form('."'#".$idForm."', '#sumin_fields', '".site_url($this->uri->segment(1)."/read_parte")."')"));
			echo form_close();
			echo "</td>";
			echo "<td width='100px' class='allign-middle p-2'><b>".$item['limit']."</b></td>";
			echo '<td width="50px">';
			$idForm='resPart'.$item['id'];
			echo form_open("plan/update_parte/",['id'=>$idForm]).form_hidden('id', $item['rowid']).form_hidden('qty', $cnt);
			echo '<button class="btn btn-danger" type="button" onclick="add_form('."'#".$idForm."', '#sumin_fields', '".site_url($this->uri->segment(1)."/read_parte")."')".'"><i class="fa fa-minus"></i></button>';
			echo '</td><td width="50px">';	
			echo form_close();
			if($item['qty']<$item['limit']){                       	
				$idForm='sumPart'.$item['id'];
				$cnt=$item['qty']+1;		
				echo form_open("plan/update_parte/",['id'=>$idForm]);
				echo form_hidden('id', $item['rowid']).form_hidden('qty', $cnt);
				echo '<button class="btn btn-success" type="button" onclick="add_form('."'#".$idForm."', '#sumin_fields', '".site_url($this->uri->segment(1)."/read_parte")."')".'"><i class="fa fa-plus"></i></button>';

				echo form_close();
			}
			echo "</td></tr>";                             
		}
	}

	public function add_parte()
	{
		$item=$this->input->post();
			//echo var_dump($item);
		$name=utf8_encode($item['name']);
		$this->cart->product_name_rules = "\/\#\~\ñ\Ñ\.\:\-\_\"\' a-z0-9"; 
		$data = array(
			'id'      => $item['id'],
			'qty'     => 1,
			'price'   => $item['price'],
			'name'    => $name,
			'limit'   => $item['limit'],
			'cod'	  => $item['cod']
		);

		$this->cart->insert($data);
	}


	public function update_parte()
	{
		$item=$this->input->post();

		$data = array(
			'rowid' => $item['id'],
			'qty'   => $item['qty']
		);
			//echo var_dump($this->cart);
		$this->cart->update($data);
	}

	public function borra_lista()
	{
		$this->cart->destroy();

	}

	public function agrega_parte($id, $edit=null)
	{
		$post = $this->input->post();

		if($edit==1){
			$this->plan->deletesum($id);
		}
		$this->plan->agregasum($id, $post);

	}

	public function lista_parte($id){
		$n=1;
		foreach ($this->plan->read_suministro(['id_plan'=>$id]) as $item):
			
			echo'<tr>
			<td class="text-center">'.$n.'</td>
			<td>'.$item['codigo'].'</td>
			<td class="text-right">'.$item['descripcion'].'</td>
			<td class="text-right">'.$item['cantidad'].'</td>
			<td class="text-right">'.money($item['costo_div'],2).'</td>
			<td class="text-right">'.money($item['costo_div']*$item['cantidad'],2).'</td>
			</tr>';
			$n++;
		endforeach;

	}
	public function list_config($id){
	  $config=$this->plan->read_config(['where'=>['id_plan'=>$id]]);
	  foreach($config as $conf):
      echo '<li class="nav-item">'; 
        echo '<a class="nav-link show config" data-id="'.$conf['id_pconfig'].'" data-toggle="tab" >';
        echo '<a class="float-right" style="font-size: 10px"><i class="fa fa-times text-danger btnDelT" data-id="'.$conf['id_pconfig'].'" data-tit="'.$conf['title'].'"></i></a>'; 
          echo $conf['title'];
        echo '</a>';
      echo '</li>';
    endforeach;
    echo '<li class="nav-item">';
    	echo '<a class="nav-link show" data-toggle="modal" data-target="#modal_config">';
    		echo '<span class="hidden-md-down"><i class="fa fa-plus"></i></span>';
      echo '</a> ';
    echo '</li>';
	}


	public function read_flota($id){
		$flota=$this->vehiculo->read(['mo.id_modelo'=>$id, 'au.id_usuario'=>$this->usuario->id_user()],true,['order_by'=>['length(flota)'=>'asc', 'flota'=>'asc']]);
		foreach ($flota as $f):
			echo '<div class="col col-2">
							<input type="checkbox" id="unit-'.$f['id_auto'].'" name="units[]" value="'.$f['id_auto'].'">
							<label style="width: 360px" for="unit-'.$f['id_auto'].'"><i class="fa fa-truck"></i> <small>'.$f['flota'].'</small></label>
						</div>';
		endforeach;

	}

	public function set_config(){
		$this->plan->set_config();
	}

	public function del_config($id){
		$this->plan->del_config($id);
	}

public function list_sumin()
{ 
  $list = '';
  //var_dump($this->cart->contents());
  foreach ($this->cart->contents() as $item) {
    $unidad="";
    if(strpos($item['name'], 'ACEITE')===0){ $unidad="(ltrs)"; }
    if(preg_match('/GRASA\b/', $item['name'])){ $unidad="(lbs)"; }
    if (isset($item['type'])&&$item['type']=='sumin_config') {
      $list .= '<tr>';
      $list .= '<td>'.$item['code'].'</td>';
      $list .= '<td>'.$item['name'].'</td>';
      $list .= '<td>'.$item['qty'].'</td>';
      $list .= '<td>';
      $list .= '<button class="btn btn-sm btn-danger float-right btnDelS" type="button" id="del_s'.$item['rowid'].'" data-id="'.$item['rowid'].'"><i class="fa fa-trash"></i></button>';
      $list .= '</td>';
      $list .= '</tr>';
    }
  }
  echo $list;
  echo "<script>
  $('.btnDelS').on('click', function() {
    var part=$(this).attr('data-id');
    var empty = confirm('Desea eliminar item seleccionado?');
    if (empty==true) {
      $.get('".site_url('inventario/del_parte/')."'+part, {delete: true}, function() {
        $('#sumin_list').load('".site_url('plan/list_sumin')."',function(data) {
          $('#sumin_list').html(data);
          });
          });
        }  
        });
        </script>";
}
}

/* End of file Usuario.php */
/* Location: ./application/controllers/Usuario.php */
