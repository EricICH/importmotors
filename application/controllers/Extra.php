<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Extra extends CI_Controller {

	public function set_404()
	{
		$this->load->view('ui/head');
		$this->load->view('ui/set_404');
		$this->load->view('ui/footer');
	}
	// List all your items
	public function option_modelo()
	{
		$post = $this->input->post();
		$items = $this->db->get_where('modelos',['id_marca'=>$post['id_marca']])->result_array();
		echo '<option value="0">Seleccione</option>';
		if (count($items)>0) {
			foreach ($items as $item) {
				$selected = ($post['selected']==$item['id_modelo'])?'selected':'';
				echo '<option value="'.$item['id_modelo'].'" '.$selected.'>'.$item['modelo'].'</option>';
			}
		}else{
			echo 'error';
		}
	}


	public function option_tienda()
	{
		$post = $this->input->post();
		$items = $this->db->get_where('tienda',['id_ruta'=>$post['id_ruta']])->result_array();
		if (count($items)>0) {
			foreach ($items as $item) {
				$selected = ($post['selected']==$item['id_tienda'])?'selected':'';
				echo '<option value="'.$item['nombre'].'" '.$selected.'>'.$item['nombre'].'</option>';
			}
		}else{
			echo 'error';
		}
	}


	public function option_tienda2($date)
	{
		$post = $this->input->post();
		$day=nice_date($date);
		$items = $this->tienda->read(['id_ruta'=>$post['id_ruta']]);
		if (count($items)>0) {
			foreach ($items as $item) {
				$selected = ($post['selected']==$item['id_tienda'])?'selected':'';
				echo '<option value="'.$item['id_tienda'].'" '.$selected.'>'.$item['nombre'].'</option>';
			}
		}else{
			echo 'error';
		}
	}


	
    public function option_tienda_desp($date=null)
    {
    	$post = $this->input->post();
    	$idu=$this->usuario->id_user();
    	$day=nice_date($date);
    	$campos =['t.id_tienda', 't.nombre'];
        $join=['despachos as d'=>['d.id_tienda = t.id_tienda'=>'inner']];
        $where=['where'=>['t.id_usuario'=>$idu, 'id_ruta'=>$post['id_ruta']], 'ISNULL(d.id_viaje)=TRUE'];
        $order=['t.nombre'=>'asc'];
        $group=['g'=>'t.id_tienda'];
        $items = $this->extra->read($campos, 'tienda as t', $join, $where, 'result', $order,$group);
    	if (count($items)>0) {
    		foreach ($items as $item) {
    			$selected = ($post['selected']==$item['id_tienda'])?'selected':'';
    			echo '<option value="'.$item['id_tienda'].'" '.$selected.'>'.$item['nombre'].'</option>';
    		}
    	}else{
    		echo 'error';
    	}
    }

    public function option_tempario()
    {
        $post = $this->input->post();
        $items = $this->db->get_where('tempario_servicios',['id_tempario_categoria'=>$post['catemp']])->result_array();
        if (count($items)>0) {
            foreach ($items as $item) {
                echo "<option value=\"{$item['id_tempario_servicio']}\">{$item['servicio']}</option>";
            }
        }else{
            echo 'error';
        }
    }

     public function option_tempario_filter()
    {
        $post = $this->input->post();
        $items = $this->db->get_where('tempario_servicios',['id_tempario_categoria'=>$post['catemp']])->result_array();
        echo "<option value=\"\">Todos</option>";
        if (count($items)>0) {
            foreach ($items as $item) {
                echo "<option value=\"{$item['id_tempario_servicio']}\">{$item['servicio']}</option>";
            }
        }else{
            echo 'error';
        }
    }

	public function option_modelo_pre($selected)
	{
		$post = $this->input->post();
		$items = $this->db->get_where('modelos',['id_marca'=>$post['id_marca']])->result_array();
		if (count($items)>0) {
			foreach ($items as $item) {
				$selected = ($post['selected']==$item['id_modelo'])?'selected':'';
				echo '<option value="'.$item['id_modelo'].'" '.$selected.'>'.$item['modelo'].'</option>';
			}
		}else{
			echo 'error';
		}
	}


	public function get_kms()
	{
		$post = $this->input->post();
		$items = $this->vehiculo->read(['au.id_auto'=>$post['id_auto']],false);
		echo $items['kilometraje'];
	}

	public function get_orden()
	{
		$post = $this->input->post();
		$this->db->select('o.*')
			->from('ordenes o')
			->where('o.id_auto', $post['id_auto'])
			->where('o.tipo !=', 'PERDIDA TOTAL')
			->where_not_in('o.estatus', ['CERRADA','ANULADA','FACTURADA'])
			->order_by('fecha_in','desc')
			->limit(1);
		$items = $this->db->get()->row_array();
		if ($items>0) {
			echo '
				<div class="row">
				  <div class="col-md">
				    <b>Nro Orden</b> <span>'.$items["nro_orden"].'</span>
				  </div>
				  <div class="col-md">
				    <b>Tipo</b> <span>'.$items["tipo"].'</span>
				  </div>
				  <div class="col-md">
				    <b>Estatus</b> <span>'.$items["estatus"].'</span>
				  </div>
				  <div class="w-100"></div>
				  <div class="col-md">
				    <b>Fecha Apertura</b> <span>'.$items["fecha_in"].'</span>
				  </div>
				</div>
			';
		}else{
			echo "empty";
		}

	

}
}

/* End of file Usuario.php */
/* Location: ./application/controllers/Usuario.php */
