<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Viaje extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->Online==false) {
			redirect('welcome','refresh');
		}
		$this->folder = $this->uri->segment(1).'/'.$this->uri->segment(2);
	}

	// List all your items
	public function index()
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);

    if($this->input->get()){
      $data['form']=$this->input->get();
      $fecha=$data['form']['fecha'];
    }else{
      $fecha=date('Y-m-d');
    }
    $this->load->view('ui/head');
    $data['viajes'] = $this->viaje->read(['fecha'=>$fecha]);
		$this->load->view($this->folder.'index', $data);
		$this->load->view('ui/footer');
	}

	// List all your items
	public function create($id=null,$info=null)
  {
    permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
    $data = [];
    $get = $this->input->get();
      if(isset($get['f'])) {
        if (@get['f'] != "") {
          $fecha=$get['f'];
        }
      }else{
        $fecha=date('Y-m-d');
      }
    $data['viajes'] = $this->viaje->read(['v.fecha'=>$fecha]);
    $data['fecha']=$fecha;
    $this->load->view('ui/head');
    $this->load->view($this->folder,$data);
    $this->load->view('ui/footer');
  }

	// Add a new item
	public function read($val=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
    $fecha=date('Y-m-d');
    
    if($this->input->get()){
      $data['form']=$this->input->get();
      if(isset($data['form']['c'])){
        $filter['v.chofer']=$data['form']['c'];
      }
      if(isset($data['form']['fecha'])){
        $filter['fecha']=$data['form']['fecha'];
        $fecha=$data['form']['fecha'];
     }else{
        $data['form']['fecha']=date('Y-m-d');  
     }
    }
    
    

    if($val==1){
      $id_user=$this->usuario->id_user();
      $this->db->update('viajes',['status'=>1],['fecha'=>$fecha,'id_usuario'=>$id_user]);
    }
		$this->load->view('ui/head');
		$data['viajes'] = $this->viaje->read($filter);
    $data['plan'] = $this->viaje->read(['fecha'=>$fecha]);
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}



  public function plantilla($id=null)
  {
    permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);

    if($this->input->get()){
      $data['form']=$this->input->get();
      $fecha=$data['form']['fecha'];
    }else{
      $fecha=date('Y-m-d');
    }
    $this->load->view('ui/head');
    $data['viajes'] = $this->viaje->read(['fecha'=>$fecha]);
    $data['plan'] = $this->viaje->read(['fecha'=>$fecha]);
    $this->load->view($this->folder,$data);
    $this->load->view('ui/footer');
    //redirect('viaje/read?fecha='.$fecha,'refresh');
  }

	public function view($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$data['view'] = $this->viaje->view(['fecha'=>$id]);
		if ($data['view']==null) {
			redirect('viaje/read');
		}
		$this->load->view('ui/head');
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}

	//Update one item
	public function update( $id = NULL )
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
	}

  public function get_viaje( $date = null )
  {
    if($date==null){
      $date=date('Y-m-d');  
    }
    $viajes=$this->viaje->read(['fecha'=>$date],true,['order_by'=>['hora','asc']]);
  
    foreach ($viajes as $viaje) {
      $trailer=$viaje['trailer']!=null?$viaje['trailer']:'N/A';
      $ayudante=$viaje['ayudante']!=null?$viaje['ayudante']:'N/A';
      $auto=$this->vehiculo->read(['flota'=>$viaje['flota']],false);
      $trail=$this->vehiculo->read(['flota'=>$viaje['trailer']],false);
      $cap=$auto['vol']+$trail['vol'];
      $list = '';
      $list.='<tr>';
      $list.='<td><small>'.nice_date($viaje['hora'], 'H:i a').'</small></td>';
      $list.='<td>'.$viaje['flota'].'</td>';
      $list.='<td>'.$trailer.'</td>';
      $list.='<td>'.$viaje['chofer'].'</td>';
      $list.='<td>'.$ayudante.'</td>';
      $list.='<td>';
      $despachos=$this->viaje->read_despachos(['id_viaje'=>$viaje['id_viaje']],'result',['tienda'=>'asc']);
      $vol=0;
      foreach ($despachos as $desp) {
        $list.='<span class="badge badge-pill badge-info text-left">'.$desp['tienda'].'<br>'.$desp['cesta'].' - '.$desp['bulto'].' - '.$desp['paleta'].' - '.$desp['carga'].'</span>';
        $vol+=$desp['carga'];
      }              
      $list.='</td>';
      $list.='<td>'.$vol.'/'.$cap.'</td>';
      $list.='<td>'.$viaje['puerta'].'</td>';
      $list.='<td><i class="fa fa-times text-danger btnDelV" style="cursor:pointer" data-id="'.$viaje['id_viaje'].'" data-name="'.$viaje['flota'].'"></i></td>';
      $list.='</tr>';
      echo $list;
    }
    echo "<script>
              $('.btnDelV').on('click', function() {
                var trab=$(this).attr('data-id');
                var nombre=$(this).attr('data-name');
                var empty = confirm('Desea eliminar la Ruta de la flota '+nombre+' de la lista?');
                if (empty==true) {
                  $.get('".site_url('viaje/del_viaje/')."'+trab, {delete: true}, function() {
                    $('#real_plan').load('".site_url('viaje/get_viaje/').$date."',function(data) {
                      $('#real_plan').html(data);
                    });
                  });
                }  
              });
          </script>";
  }


  public function get_tienda( $id = NULL )
  {
    foreach ($this->cart->contents() as $item) {
      $list = '';
      if (isset($item['type'])&&$item['type']=='tienda') {
        $list .= '<span class="badge badge-pill badge-info text-center">';
        $list .= $item['name'].'<br>';
        $list .= $item['cesta'].' -'; 
        $list .= $item['bulto'].' -';
        $list .= $item['paleta'].' -';
        $list .= $item['price'].' ';
        $list .='<i class="fas fa-times text-danger btnDelT" style="cursor:pointer" data-id="'.$item['rowid'].'" data-name="'.$item['name'].'"></i>';
        $list .='</span>';
      }
      echo $list;          
    //$this->cart->destroy();
    }
    echo "<script>
              $('.btnDelT').on('click', function() {
                var trab=$(this).attr('data-id');
                var nombre=$(this).attr('data-name');
                var empty = confirm('Desea eliminar la tienda '+nombre+' de la lista?');
                if (empty==true) {
                  $.get('".site_url('viaje/del_tienda/')."'+trab, {delete: true}, function() {
                    $('#despachos').load('".site_url('viaje/get_tienda')."',function(data) {
                      $('#desapchos').html(data);
                    });
                  });
                }  
              });
          </script>";
  }

  public function set_tienda( $id = NULL )
  {
    $form = $this->input->post();
    $this->cart->product_name_rules = "(\)\/\#\~\ñ\Ñ\.\:\-\_\"\' a-z0-9";
    $tienda = $this->tienda->read(['id_tienda'=>$form['tienda']],false);
    $data = [
      'id'      => $form['tienda'],
      'code'    => $tienda['id_zona'],
      'type'    => $form['type'],
      'qty'     => 1,
      'price'   => $form['vol'],
      'name'    => $tienda['nombre'],
      'bulto'   => $form['bulto'],
      'cesta'   => $form['cesta'],
      'paleta'  => $form['paleta']
    ];
    
    $this->cart->insert($data);
  }

   public function set_viaje( $id = NULL )
  {
    $new=$this->viaje->create();
    $this->cart->destroy();
  }

  public function del_tienda( $id = NULL )
  {
    if($id==null){
      foreach ($this->cart->contents() as $item) {
        $data = array(
          'rowid' => $item['rowid'],
          'qty'   => 0
        );
        $this->cart->update($data);
      }
    }else{
      $data = array(
          'rowid' => $id,
          'qty'   => 0
        );
      $this->cart->update($data);
    }
  }

    public function del_viaje( $id = NULL )
    {
        $this->viaje->delete($id);
    }

    public function editaPlan($id, $fecha){

        
      $day=nice_date($fecha,'w');
      $campos=['o.*','t.nombre','t.id_ruta','t.salida','a.flota','a.id_auto','a.estatus','a.capacidad','ch.id_chofer'];
      $where=['where'=>['o.id_ola'=>$id]];

      $join=['tienda t'=>['o.id_tienda=t.id_tienda'=>'inner'],'planruta pr'=>['t.id_ruta=pr.id_ruta'=>'inner'],'autos a'=>['a.id_auto=pr.id_auto'=>'left'],'choferes ch' => ['a.id_auto=ch.id_auto'=>'left']];
      $ola=$this->extra->read($campos,'ola as o', $join,$where,'row');
      //var_dump($ola);

      $cmps=['o.*','t.nombre','t.id_ruta','t.salida','t.t_viaje','t.id_tienda'];
      $whr=['where'=>['day'=>$day,'o.puerta'=>$ola['puerta'],'t.id_ruta'=>$ola['id_ruta']]];
      $jn=['tienda t'=>['o.id_tienda=t.id_tienda'=>'inner']];
      $dspch='';
      $crg='';
      $ctot=0;
      $dd=[];
      $tnds=$this->extra->read($cmps,'ola as o', $jn,$whr,'result', ['t.salida'=>'desc','o.lote'=>'desc'],['g'=>'t.nombre']);
      foreach ($tnds as $desp):
        $despachos=$this->despacho->read(['d.id_viaje'=>null,'d.fecha'=>$fecha,'d.id_tienda'=>$desp['id_tienda']],false);
        if(!empty($despachos)): $crg='<br>'.$despachos['cesta'].'-'.$despachos['bulto'].'-'.$despachos['paleta'].'-'.$despachos['carga']; $ctot+=$despachos['carga']; endif;
        //$salida=strtotime($desp['salida'])>strtotime($salida)?$desp['salida']:$salida;
        $dspch.='<span class="badge badge-pill badge-info text-center" >'.$desp['nombre'].$crg.'</span>  ';
        $dd[$desp['nombre']]=$crg;
        //echo $desp['nombre'].'['.$crg.']    ';
      endforeach;
      ?> 
      <?=form_open('viaje/set_ola', ["id"=>"set_ola", "after-send"=>"#".$ola['id_ola']], ['fecha'=>$fecha,'id_ruta'=>$ola['id_ruta'],'id_ola'=>$id]); ?>
      <table>
        <tbody>
          <tr>
            <td width="30%">
            <span>
              <label>Hora de Salida</label>
              <input type="text" name="salida" class="form-control" value="<?=$ola['salida']?>" required>
            </span>  
            </td>
            <td width="40%">
              <?= form_label('Vehiculo', ''); ?>
              <select name="id_auto" class="form-control form-control-sm s2"  required>
                <?php foreach ($this->vehiculo->read(['estatus ='=>1, 'trailer'=>'n', 'disp'=>'s'],true, ['order_by'=>['length(flota)'=>'asc','flota'=>'asc']]) as $item): ?>
                  <option value="<?= $item['id_auto'] ?>" data-carga="<?=$item['carga_max']!=null?$item['carga_max']:'0'?>" <?=$item['id_auto']==$ola['id_auto']?'selected':''?>>
                    Flota <?= $item['flota'] ?> / <?= $item['marca'] ?> <?= $item['modelo'] ?> [<?=$item['capacidad']!=null?$item['capacidad']:'0'?>]
                  </option>
                <?php endforeach ?>    
              </select>
            </td>
            <td width="30%">
              <?= form_label('Trailer', ''); ?>
              <select name="id_trailer" id="id_trailer" class="form-control form-control-sm s2">
                <option value="">Ninguno</option>
                <?php foreach ($this->vehiculo->read(['estatus ='=>1, 'trailer'=>'s', 'disp'=>'s']) as $item): ?>
                  <option value="<?= $item['flota'] ?>">
                   <?= $item['flota'] ?>  [<?=$item['capacidad']>0?$item['capacidad']:$item['vol']?>]
                 </option>
               <?php endforeach ?>    
             </select>
           </td>
         </tr>
         <tr>
          <td>
            <span>
              <label>Chofer</label>
              <select name="id_chofer" id="id_chofer" class="form-control form-control-sm s2" required  style="width: 100%" >
                <?php foreach ($this->chofer->read(['ch.cargo'=>"CHOFER"]) as $item): ?>
                  <option value="<?= $item['nombre'] ?>" <?=$item['id_chofer']==$ola['id_chofer']?'selected':''?>><?= $item['nombre'] ?></option>
                <?php endforeach ?>
              </select>
            </span>
          </td>
          <td class="align-middle">
            <span>
              <label>Ayudante</label>
              <select name="ayudante" id="ayudante" class="form-control form-control-sm s2 l3" style="width: 100%" >
                <option value="">Ninguno</option>
                <?php foreach ($this->chofer->read(['ch.cargo'=>'AYUDANTE DE CHOFER']) as $item): ?>
                  <option value="<?= $item['nombre'] ?>"><?= $item['nombre'] ?></option>
                <?php endforeach ?>
              </select>
            </span>
          </td>
          <td class="align-middle">
            <span>
              <label>Puerta</label>
              <input type="text" name="puerta" class="form-control" value="<?=$ola['puerta']?>" required>
            </span>
          </td>
        </tr>
        <tr>
          <td colspan="3" id="tnds">Tiendas:
            <?php foreach ($dd as $key => $value): ?>
              <span class="badge badge-pill badge-info text-center" id="<?=$key?>"><div class="float-left"><?=$key?><?=$value?></div><div class="float-right" style="height: 100%; vertical-align: top;"><i class="fa fa-times text-danger delT" data-id="<?=str_replace(' ', '_', $key)?>" style="font-size: 15px; cursor: pointer;"></i></div></span> 
            <?php  endforeach;?>
            <input type="hidden" id="despachos" name="despachos" value='<?=json_encode($dd)?>'>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td><button class="btn btn-success" id="set_btn" data-id="<?=$id?>" type="submit" form="#set_ola">Guardar</button></td>
        </tr>
      </tfoot>
    </table>
    <?=form_close();?>
    <script>

      var content = $('#<?=$id?>').html();
      str= content.split('</td>');
      trail=str[2].replace('<td>','').trim();
      $("#id_trailer option[value='"+trail+"'").attr("selected",true);
      
    $('#set_btn').on('click', function() {
      $('#set_ola').submit();
      $('#changeAuto').find('.close').click();
    });
    $('#set_ola').on('submit', function (e) {
       e.preventDefault();
       $.post('<?=site_url('viaje/set_ola')?>', $('#set_ola').serialize(), function (data, textStatus) {
          $('#<?=$id?>').html(data);
        });
    });
    $(".delT").on('click', function(){
      id=$(this).data('id');
      array=$('#despachos').val();
      $('#tnds').load('<?=site_url('viaje/delT/')?>'+id,{array:array}, function(data) {
          $(this).html(data);
        });
    });
     
  </script>
  <?php
    }

    public function delT($id){
      $get=$this->input->post();
      $array=json_decode($get['array'],true);
      
      unset($array[str_replace('_', ' ', $id)]);
        echo "Tiendas:";
        foreach ($array as $key => $value): 
          echo '<span class="badge badge-pill badge-info text-center">';
            echo '<div class="float-left">'.$key.$value.'</div>';
            echo '<div class="float-right" style="height: 100%; vertical-align: top;">';
            echo '<i class="fa fa-times text-danger delT" data-id="'.$key.'" style="font-size: 15px; cursor: pointer;"></i></div></span> ';

        endforeach;
        echo "<input type='hidden' id='despachos' name='despachos' value='".json_encode($array)."'>";
    
    }

    public function set_ola(){
      $p=$this->input->post();
      if($p['fecha']!=null){ $fecha=$p['fecha']; $day=nice_date($p['fecha'],'w'); }else{ $p=date('Y-m-d'); $day=date('W');} 
      $a=$this->vehiculo->read(['au.id_auto'=>$p['id_auto']],false);
      $ch=$this->chofer->read(['ch.nombre'=>$p['id_chofer']],false);
      $ay=$this->chofer->read(['ch.nombre'=>$p['ayudante']],false);
      $tr=$this->vehiculo->read(['flota'=>$p['id_trailer']],false);
      $cmps=['o.*','t.nombre','t.id_ruta','t.salida','t.t_viaje','t.id_tienda'];
      $whr=['where'=>['day'=>$day,'o.puerta'=>$p['puerta'],'t.id_ruta'=>$p['id_ruta']]];
      $jn=['tienda t'=>['o.id_tienda=t.id_tienda'=>'inner']];
      $tnds=$this->extra->read($cmps,'ola as o', $jn,$whr,'result', ['t.salida'=>'desc','o.lote'=>'desc'],['g'=>'t.nombre']);
      $crg='';
      $ctot=0;
      $css="";
      $ico="";
      $dspch='';
      $salida='00:00';
      $dd=[];
      $despch=json_decode($p['despachos']);
      foreach (@$despch as $key => $value):
        $dspch.='<span class="badge badge-pill badge-info text-center" >'.@$key.@$value.'</span>  ';
        $datd=explode('-',@$value);
        $ctot+=@$datd[3];
      
      endforeach;
      ?>
      <td><small><?=nice_date($p['salida'],'H:i')?></small></td>
      <td><?=$a['flota']?></td>
      <td><?=$tr['id_auto']!=null?$tr['flota']:'N/A'?></td>
      <td><?=$p['id_chofer']?></td>
      <td><?=$p['ayudante']?></td>
      <td><?=$dspch?></td>
      <td><?=$ctot?></td>
      <td class='<?=$css?>'><?=$tr['capacidad']>0?$tr['capacidad']:$a['capacidad']?></td>
      <td><?=$p['puerta']?></td>
      <td>
        <input type="hidden" name="id_auto[]" value="<?=$a['id_auto']?>">
        <input type="hidden" name="id_trailer[]" value="<?=$tr['id_auto']?>">
        <input type="hidden" name="salida[]" value="<?=$p['salida']?>">
        <input type="hidden" name="id_chofer[]" value="<?=$ch['id_chofer']?>">
        <input type="hidden" name="ayudante[]" value="<?=$ay['id_chofer']?>">
        <input type="hidden" name="puerta[]" value="<?=$p['puerta']?>">
        <input type="hidden" name="despachos[]" value='<?=$p['despachos']?>'>
        <button type="button" class="btn btn-sm" data-toggle="modal"  data-target="#changeAuto"><i class="fa fa-edit data-pass" data-id="<?=$p['id_ola']?>" ></i></button>
      </td><?php 
    }
    public function process_ola(){
      $p=$this->input->post();
     //var_dump($p);
      $i=0;
      $idUser=$this->usuario->id_user();
      foreach ($p['despachos'] as $desp) {
        //var_dump($desp);
        $data=[
          'hora'=>$p['salida'][$i],
          'fecha'=>$p['fecha'],
          'id_auto'=>$p['id_auto'][$i],
          'trailer'=>$p['id_trailer'][$i],
          'chofer'=>$p['id_chofer'][$i],
          'ayudante'=>$p['ayudante'][$i],
          'puerta'=>$p['puerta'][$i],
          'id_usuario'=>$idUser
        ];
        $viaje=$this->viaje->read(['hora'=>$p['salida'][$i],'fecha'=>$p['fecha'],'v.id_auto'=>$p['id_auto'][$i],'v.id_usuario'=>$idUser],false);
        if($viaje['id_viaje']==null){
          $insert = $this->db->insert('viajes', $data);
          $id_viaje = $this->db->insert_id();
        }else{
          $id_viaje=$viaje['id_viaje'];
        }
        if ($id_viaje) {
          $tienda=json_decode($desp,true);
          //var_dump($tienda);
          foreach ($tienda as $key=>$value) { 
              $tnd=$this->tienda->read(['nombre'=>$key],false);
              $despacho=$this->despacho->read(['d.id_tienda'=>$tnd['id_tienda'],'ISNULL(d.id_viaje)'=>TRUE], false);
              //var_dump($despacho);
              $this->db->update('despachos',['id_viaje'=>$id_viaje],['id_despacho'=>$despacho['id_despacho']]);
            
          }
        }

        $i++;
        
      }
      redirect('viaje/read?fecha='.$p['fecha'],'refresh');
    }

    public function backhaull(){
      if($this->input->get()){
        $data['form']=$this->input->get();
        $fecha=$data['form']['fecha'];
      }else{
        $fecha=date('Y-m-d');
      }
      if($this->input->post()){
        $post=$this->input->post();
        $this->viaje->update($post['id_viaje']);
      }
      $this->load->view('ui/head');
      $data['viajes'] = $this->viaje->read(['fecha'=>$fecha]);
      $this->load->view($this->folder,$data);
      $this->load->view('ui/footer');
      //redirect('viaje/read?fecha='.$fecha,'refresh');
    }

    public function back_check($id){
      $viaje = $this->viaje->read(['id_viaje'=>$id],false);
      $prov = $this->proveedor->read();
      foreach ($prov as $value) {
        $options[$value['id_proveedor']]=$value['nombre'];
      }
      ?>

      <?=form_open('viaje/backhaull/'.$viaje['fecha'], ["id"=>"set_ola", "after-send"=>""], ['id_viaje'=>$viaje['id_viaje'],'backhaull'=>1]); ?>
        <table>
          <tbody>
            <tr class="align-middle">
              <td class="w-50" width="50%">
                  <label>Proveedores</label>
                  <?= form_dropdown('id_proveedor', $options,null,['class'=>'form-control s2']) ?>
              </td>
            </tr>
            <tr>
              <td>
                <label>
                  Carga Finalizada Correctamente
                </label>
                <?=form_textarea('obs_back',null,['class'=>'form-control','rows'=>3])?>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="2"><button class="btn btn-success" id="set_btn" data-id="<?=$id?>" type="submit">Guardar</button></td>
            </tr>
          </tfoot>
        </table>
      <?=form_close();?>
      <?php
    }
}
/* End of file Usuario.php */
/* Location: ./application/controllers/Usuario.php */