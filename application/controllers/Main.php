<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->folder = $this->uri->segment(1).'/'.$this->uri->segment(2);

	}

	// List all your items
	public function index()
	{
		$data['list']= $this->auto->read();
			$this->load->view('ui/head');
			$this->load->view('main/index',$data);
		$this->load->view('ui/footer');
	}

	// List all your items
	public function create()
	{
		
	}

	// Add a new item
	public function read($id=null)
	{
		$this->load->view('ui/head');
		$this->load->view($this->folder);
		$this->load->view('ui/footer');
	}

	public function view($id=null)
	{
		$data['view'] = $this->user->read(['id_user'=>$id]);
		if ($data['view']==null) {
			redirect('user/read');
		}
		$this->load->view('ui/head');
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}

	//Update one item
	public function update( $id = NULL )
	{

	}

	//Delete one item
	public function delete( $id = NULL )
	{

	}

	public function login()
	{}
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
