<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->folder = $this->uri->segment(1).'/'.$this->uri->segment(2);

	}

	// List all your items
	public function index()
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		redirect($this->uri->segment(1).'/read','refresh');
	}

	// List all your items
	public function create($id=null)
	{
    $this->form_validation->set_rules('nombre', 'Nombre', 'required');
    $this->form_validation->set_rules('apellido', 'Apellido', 'required');
///    $this->form_validation->set_rules('ci', 'Cedula', 'required');
    
    if ($this->form_validation->run() == TRUE) {
      $form = $this->input->post();
      if ($form['id_taller_personal']!==''){
        $this->personal->update();
      }else{
        $this->personal->create();
        $id_person = $this->db->insert_id();
        $person=$this->personal->read(['where'=>['id_taller_personal'=>$id_person]],false);
        $taller=$this->taller->read(['id_taller'=>$person['id_taller']],'row');
        $this->logSys->create(['controller'=>'Personal', 'action'=>'Registro nuevo personal: '.$person['nombre'].' '.$person['apellido'].' en taller '.$taller['nombre']]);
      }
      $url='taller/read';
      if(isset($form['id_taller'])&&($form['id_taller']!='')){ $url='taller/view/'.$form['id_taller'].'#personalT'; }
      redirect($url,'refresh');
    } else {
      $get = $this->input->get();

      $this->load->view('ui/head');
      $data['edit'] = $this->personal->read(['where'=>['id_taller_personal'=>$id]],false);
      if (isset($get['taller'])){
        $data['dependencia'] = $this->personal->read(['where'=>['id_taller'=>$get['taller']]]);
        $data['taller'] = $this->taller->read(['id_taller'=>$get['taller']],'row');
      }else{
        $data['dependencia'] = $this->personal->read(['where'=>['id_taller'=>$data['edit']['id_taller']]]);
        $data['taller'] = $this->taller->read(['id_taller'=>$data['edit']['id_taller']],'row');
      }
      $this->load->view($this->folder,$data);
      $this->load->view('ui/footer');
    }
	}

	// Add a new item
	public function read($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$this->load->view('ui/head');
		$this->load->view($this->folder);
		$this->load->view('ui/footer');
	}

	public function almacen_horario($id=null)
	{
		//permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$data['turnos']=$this->personal->turnos([],true,['order_by'=>['turno','asc'],'group_by'=>['hora_in']]);
		$data['personal']=$this->personal->personal_almacen();
		$data['day']=['Dia','Lunes', 'Martes', 'Miercoles', 'Jueves','Viernes','Sabado','Domingo'];
		$data['d']=['Dia','Lu', 'Ma', 'Mi', 'Ju','Vi','Sa','Do'];
		$this->load->view('ui/head');
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}

	public function admin_horario($id=null)
	{
		//permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$data['turnos']=$this->personal->turnos(null, true,['order_by'=>['turno','asc'], 'group_by'=>['hora_in']]);
		$data['turn']=$this->personal->turnos();
		$data['day']=['Dia','Lunes', 'Martes', 'Miercoles', 'Jueves','Viernes','Sabado','Domingo'];
		$this->load->view('ui/head');
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}

	public function view($id=null,$upd=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		if($upd=='p'){
			permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
			$post=$this->input->post();
			//var_dump($post);
			$this->usuario->updt_permisos($id, $post);
		}
		if($upd=='u'){
			permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
			$post=$this->input->post();
			$this->form_validation->set_rules('usuario', 'Usuario', 'required!is_unique[usuarios.usuario]');
			
			if($post['clave_1']!=""){
				$this->form_validation->set_rules('clavecnf', 'Password Confirmation', 'required|matches[clave_1]');
			}else{
				unset($post['clave_1']);
				unset($post['clavecnf']);
			}
			
			$this->form_validation->set_rules('email', 'Email', 'required');
			
			$this->usuario->update($id,$post);

		}
		if($upd=='u'){
			permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
			$post=$this->input->post();
			$this->usuario->update($id,$post);
		}

		$data['view'] = $this->personal->view(['id_usuario'=>$id]);
		if ($data['view']==null) {
			redirect('usuario/read');
		}
		$this->load->view('ui/head');
		//$this->load->view('ui/nav');
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}

	//Update one item
	public function update( $id = NULL )
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
		$pers=$this->personal->read(['id_taller_personal'=>$id],false);
		$this->personal->delete($id);
		redirect('taller/view/'.$pers['id_taller'].'#personalT','refresh');
	}

	public function login()
	{
		$post = $this->input->post();
		//var_dump($post);
		$this->form_validation->set_rules('usuario', 'Usuario', 'required');
		$this->form_validation->set_rules('clave', 'Clave', 'required');

		if ($this->form_validation->run() == TRUE) {
			$user = $this->usuario->login(['usuario'=>$post['usuario'],'clave_1'=>$post['clave']]);
			//var_dump($user);
			if ($user == null) {
				show_error('El usuario suministrado no existe.',404,'Datos Invalidos');
			}
			$campos=['acc.read','md.action'];
			$join=['modulo-data md' => 'acc.id_modulo = md.id_modulo-data'];
			$where=['where'=>['acc.id_usuario' => $user['id_usuario']]];
			$permisos=$this->extra->read($campos,'accesos AS acc',$join,$where);
			
			$root = $user['superadmin']==1?true:false;
			$array = array(
				'IdUser' => $user['id_usuario'],
				'Name' => $user['nombre'],
				'User' => $user['usuario'],
        		'Pic' => $user['picture'],
				'Online' => true,
				'Root' => $root,
				'Parent'=>$user['id_usuario_master'],
				'Permisos'=> $permisos
			);
			
			$this->session->set_userdata( $array );
			redirect('cliente/index');
		} else {
			
		}
		$this->load->view($this->uri->segment(1).'/'.$this->uri->segment(2));
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('welcome','refresh');
	}

	public function option_mecanico()
    {
        $post = $this->input->post();
        $items = $this->personal->read(['where'=>['id_taller'=>$post['id']]]);
        if (count($items)>0) {
            foreach ($items as $item) {
                echo "<option value=\"{$item['id_taller_personal']}\">{$item['nombre']} {$item['apellido']}</option>";
            }
        }else{
            echo 'error';
        }
    }

    public function option_jefe()
    {
        $post = $this->input->post();
        $items = $this->personal->read(['where'=>['id_taller'=>$post['id'],'jefe_taller'=>1]]);
        if (count($items)>0) {
            foreach ($items as $item) {
                echo "<option value=\"{$item['id_taller_personal']}\">{$item['nombre']} {$item['apellido']}</option>";
            }
        }else{
            echo 'error';
        }
    }

	public function option_turno()
	{
		$day=['Dia','Lunes', 'Martes', 'Miercoles', 'Jueves','Viernes','Sabado','Domingo'];
		$post = $this->input->post();
		$item = $this->personal->turnos(['where'=>['id_turno'=>$post['turno']]],false);
		echo '<option value="">N/A</option>';
		for($i=1; $i<=7; $i++){
                echo '<option value="'.$i.'" ';
                if ($item['dia_in']==$i){ echo 'selected'; }
                echo '>'.$day[$i].'</option>';
         } 
	}

	public function option_hora()
	{
		$post = $this->input->post();
		$item = $this->personal->turnos(['where'=>['id_turno'=>$post['turno']]],false);
		echo '<option value="">N/A</option>';
		for($i=0; $i<=23; $i++){ 
            $hora=zerofill($i,2).':00';
			echo '<option value="'.nice_date($hora,'H:i').'" ';
			if (nice_date($item['hora_in'],'H:i')==$hora){ echo 'selected'; }
			echo '>'.nice_date($hora,'h:i a').'</option>';
         }
	}

	public function edit_turno()
	{
		$this->personal->edit_turno();
	}

	public function read_turnos()
	{
		$turnos=$this->personal->turnos(null,true,['group_by'=>['hora_in']]);
		$turn=$this->personal->turnos();
		$day=['Dia','Lunes', 'Martes', 'Miercoles', 'Jueves','Viernes','Sabado','Domingo'];

		foreach($turnos as $t){ 
		  $hora_in=new DateTime(nice_date($t['hora_in'], 'H:i')); 
		  $int='PT'.($t['jornada']+$t['alm']).'H';
		  echo '<tr>';
		  echo '<th class="text-center "><strong>'; echo $hora_in->format('h:i a'); echo '</strong> <strong>';
		  $hora_in->add(new DateInterval($int));
		  echo $hora_in->format('h:i a');
		  echo '</strong></th>';
		  for($i=1; $i<=7; $i++){
		      echo '<td class="border">';
		      foreach($turn as $tp){
		        $dia_fin=$tp['dia_in']+$tp['duracion']>7?$tp['dia_in']+$tp['duracion']-7:$tp['dia_in']+$tp['duracion'];
		        $lab='n';
		        if($dia_fin<$tp['dia_in']){
		          if(($i>=$tp['dia_in'])||($i<$dia_fin)){ $lab='s'; }  
		        }else{
		          if(($i>=$tp['dia_in'])&&($i<$dia_fin)){ $lab='s'; }  
		        }
		        $turno=$tp['turno'];
		        if($i>5){ $turno=2; }
		          if ($t['turno']==$turno){ 
		            if ($lab=='s'){ 
		              echo '<div id="list_"><span class="badge">'.$tp['turno'].'-'.$tp['tabla'].'</span><br></div>';
		      		}
		          }
		        }
		      	echo '</td>';
		      }
		  echo '</tr>';
		}
	}


}

/* End of file Usuario.php */
/* Location: ./application/controllers/Usuario.php */
