<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_model extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->Online==false) {
			redirect('welcome','refresh');
		}
		$this->folder = $this->uri->segment(1).'/'.$this->uri->segment(2);
	}

	// KPI
	public function index()
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
        $this->load->view('ui/head');
        $data['brand'] = $this->model_model->read(null,true,['group_by'=>['b.brand']]);
        $this->load->view($this->folder.'/read',$data);
        $this->load->view('ui/footer');
	}

	// Crear
	public function create($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
	  $post = $this->input->get();
	  if (@$post['s']=='brand'){
        $this->form_validation->set_rules('brand', 'brand', 'required');
      }
      if (@$post['s']=='model'){
        $this->form_validation->set_rules('model', 'model', 'required');
      }

	if ($this->form_validation->run() == TRUE) {
      if ($post['s']=='brand'){
        $this->model_model->create_brand();
      }
      if ($post['s']=='model'){
        $this->model_model->create_model();
      }
  }
			$this->load->view('ui/head');
			$this->load->view($this->folder);
			$this->load->view('ui/footer');
	}

	// Editar
	public function edit($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
		$this->form_validation->set_rules('plate', 'Plate', 'required');
		if ($this->form_validation->run() == TRUE) {
			$this->auto->update();
		} else {
			$this->load->view('ui/head');
			$data['edit'] = $this->auto->read(['au.id_auto'=>$id],false);
			$this->load->view($this->folder,$data);
			$this->load->view('ui/footer');
		}
	}

	// Lista
	public function read($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$this->load->view('ui/head');
		$data['brand'] = $this->model_model->read(null,true,['group_by'=>['b.brand']]);
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}

	// Lista
	public function view($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$data['list'] = $this->auto->read(['id_model'=>$id]);
		$this->load->view('ui/head');
		$this->load->view('auto/read',$data);
		$this->load->view('ui/footer');
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
	}
}

/* End of file Usuario.php */
/* Location: ./application/controllers/Usuario.php */
