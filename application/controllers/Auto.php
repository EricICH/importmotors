<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auto extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->Online==false) {
			redirect('welcome','refresh');
		}
		$this->folder = $this->uri->segment(1).'/'.$this->uri->segment(2);
	}

	// KPI
	public function index()
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);

		$data[]=null;
		$load='/index';
		if($this->session->Acc==0){
			$chfr=$this->chofer->read(['ch.id_user'=>$this->session->IdUser],false);
			if(!empty($chfr)){ 
				$data['view'] = $this->vehiculo->read(['au.id_auto'=>$chfr['id_auto']],false,['order_by'=>['flota','asc']]);
				$load='/view';
			}
		}
		$this->load->view('ui/head');
		$this->load->view($this->uri->segment(1).$load,$data);
		$this->load->view('ui/footer');
	}

	// Crear
	public function create($id=null)
	{
		//permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
		if ($this->input->post()) {
			if($id!=null){
				$last=$this->auto->update($id);	
			}else{
      			$last=$this->auto->create();
			}
		  	redirect('auto/view/'.$last);
		} else {

			$data['brands']=$this->model->read(null,true,['group_by'=>['m.id_brand']]);
			if($id!=null){ $data['edit']=$this->auto->read(['id_auto'=>$id],false); }
			$this->load->view('ui/head');
			$this->load->view($this->folder, $data);
			$this->load->view('ui/footer');
		}
	}

	// Editar
	public function edit($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
		//$this->form_validation->set_rules('placa', 'Placa', 'required');
		if ($this->input->post()) {
			$this->vehiculo->update();
			redirect('vehiculo/view/'.$id);
		} 
			$this->load->view('ui/head');
			$data['edit'] = $this->vehiculo->read(['au.id_auto'=>$id],false);
			$this->load->view($this->folder,$data);
			$this->load->view('ui/footer');
		
	}


	// Lista
	public function read($f="all")
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		if ($f=="all") {
			$filter = null;
		}else{
			$search = explode("-", $f);
			if ($search[0]=="e") {
				$filter['au.estatus'] = $search[1]; 
			}
			if ($search[0]=="t") {
				$filter['au.tipo'] = $search[1]; 
			}
		}
	
		$this->load->view('ui/head');

		$data['list'] = $this->vehiculo->read($filter,true,['order_by'=>['length(flota)'=> 'asc', 'flota'=>'asc'],'group_by'=>'placa']);
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}


	
	// Lista
	public function view($id=null)
	{
		$data['view'] = $this->auto->read(['au.id_auto'=>$id],false);
		if ($data['view']==null) {
			redirect('main/index');
		}

		$this->load->view('ui/head');

		  $this->load->view($this->folder,$data);
		
		
		$this->load->view('ui/footer');
	}

	//Delete one item
	public function delete( $id )
	{
		permite($this->session->Permisos,'vehiculo',2);
		$auto=$this->vehiculo->read(['au.id_auto'=>$id],false);
		$this->logSys->create(['controller'=>'vehiculo', 'action'=>'elimino Vehiculo: '.$auto['flota']]);
	  	$this->vehiculo->delete($id);
	  	redirect('vehiculo/view/'.$id);
	}

	public function option_model($id){
		
		$models=$this->model->read(['where'=>['m.id_brand'=>$id]],true);		
		foreach ($models as $model): ?>
            <option value="<?=$model['id_model']?>"><?=$model['model']?></option>
        <?php endforeach ?>
       	<option value="0" <?=count($models)==0?'selected':""?>>Nuevo</option>
       	<?php

	}

	
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
