<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->folder = $this->uri->segment(1).'/'.$this->uri->segment(2);

	}

	// List all your items
	public function index()
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		redirect($this->uri->segment(1).'/read','refresh');
	}

	// List all your items
	public function create()
	{
		if($this->input->post()){
		    $post=$this->input->post();
			$new=$this->user->create($post);
			$user=$this->user->read(['id_user'=>$new],'row');
			$this->logSys->create(['controller'=>'user', 'action'=>'Creo user: '.$user['user']]);
			redirect($this->uri->segment(1).'/view/'.$new,'refresh');		
		}
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
		$this->load->view('ui/head');
		//$this->load->view('ui/nav');
		$this->load->view($this->folder);
		$this->load->view('ui/footer');
	}

	// Add a new item
	public function read($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$this->load->view('ui/head');
		//$this->load->view('ui/nav');
		$this->load->view($this->folder);
		$this->load->view('ui/footer');
	}

    public function view($id=null,$upd=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$user=$this->user->read(['id_user'=>$id],'row');
		if(($id==$this->session->IdUser)||($user['id_parent']==$this->session->IdUser)){	
			if($upd=='p'){
				$post=$this->input->post();
				$this->user->updt_permisos($id, $post);
				$user=$this->user->read(['id_user'=>$id],'row');
				//$this->logSys->create(['controller'=>'user', 'action'=>'Modifico permisos de user: '.$user['user']]);
			}
			if($upd=='u'){
				$post=$this->input->post();
				$this->form_validation->set_rules('user', 'user', 'required!is_unique[users.user]');
				
				if($post['pass_1']!=""){
					$this->form_validation->set_rules('passcnf', 'Password Confirmation', 'required|matches[pass_1]');
				}else{
					unset($post['pass']);
					unset($post['passcnf']);
				}
				
				$this->form_validation->set_rules('email', 'Email', 'required');
					
				$this->user->update($id,$post);
				$us=$this->user->view(['id_user'=>$id],'row');
				$this->logSys->create(['controller'=>'user', 'action'=>'Modifico datos de user: '.$us['user']]);

			}
			if($upd=='x'){
				permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
				$post=$this->input->post();
				$this->user->update($id,$post);
			}
		}
		$data['view'] = $this->user->view(['id_user'=>$id]);
		if ($data['view']==null) {
			redirect('user/read');
		}
		$this->load->view('ui/head');
		//$this->load->view('ui/nav');
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}

	//Update one item
	public function update( $id = NULL )
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
        if($id!=null) {
        	$user=$this->user->read(['id_user'=>$id],'row');
        	$this->logSys->create(['controller'=>'user', 'action'=>'elimino user: '.$user['user']]);
            $this->user->delete($id);
        }
        redirect('user/read');
    }

	public function login()
	{
		$post = $this->input->post();
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('pass', 'pass', 'required');

		if ($this->form_validation->run() == TRUE) {
			$user = $this->user->login(['username'=>$post['username'],'pass'=>$post['pass']]);
			if ($user == null) {
				show_error('El Usuario suministrado no existe.',404,'Datos Invalidos');
			}
			$IDU=$user['id_parent']==0?$user['id_user']:$user['id_parent'];
			
			$array = array(
				'IdUser' => $user['id_user'],
				'Name' => $user['name'],
				'User' => $user['username'],
        		'Pic' => base_url().$user['picture'],
        		'Cargo' => $user['cargo'],
				'Online' => true,
				'Parent'=>$user['id_parent']
			);
			
			$this->session->set_userdata( $array );
			//$this->logSys->create(['controller'=>'user', 'action'=>'inicio Sesion']);
			redirect('main/index');
		} else {
			
		}
		$this->load->view($this->uri->segment(1).'/'.$this->uri->segment(2));
	}

	public function logout()
	{
		//$this->logSys->create(['controller'=>'user', 'action'=>'Fin Sesion']);
		$this->session->sess_destroy();
		redirect('welcome','refresh');
	}
}
/* End of file user.php */
/* Location: ./application/controllers/user.php */