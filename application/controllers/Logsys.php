<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LogSys extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->Online==false) {
			redirect('welcome','refresh');
    }
		$this->folder = $this->uri->segment(1).'/'.$this->uri->segment(2);
	}

	// KPI
	public function index()
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		redirect('logsys/read');
	}

	// Crear
	public function create($id=null){}

	// Editar
	public function edit($id=null)
	{}

	// Lista
	public function read($d="now",$f="all")
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		if ($f=="all") {
			$filter = null;
		}else{
			$search = explode("-", $f);

			if ($search[0]=="t") {
				$filter['t.tipo'] = $search[1];
			}
		}
		if ($d=="now") {
			$filter['DATE(date)'] =date('Y-m-d');
		}else{
			$filter['DATE(date)'] =$d;
		}
		
		$data['total_rows'] = count($this->logSys->read($filter));
		$this->load->view('ui/head');
		$data['list'] = $this->logSys->read($filter,'result');
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}

	// Lista
	public function view($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),1);
		$data['view'] = $this->logSys->read(['id_log'=>$id],'row');
		if ($data['view']==null) {
			redirect('logsys/read');
		}
		$this->load->view('ui/head');
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
	}
}

/* End of file Usuario.php */
/* Location: ./application/controllers/Usuario.php */
