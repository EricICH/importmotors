<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->Online==false) {
			redirect('welcome','refresh');
		}
		$this->folder = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$this->load->model('usuario_model');
		//$this->load->model('admin_model');
		$this->load->helper('count_helper');
		//$this->load->model('herramienta_model');
				
	}

	// KPI
	public function index()
	{
		$this->load->view('ui/head');
		//$data['list'] = $this->admin->read();
		$page = $this->uri->segment(3);
		$offset = !$page?0:$page;
		$this->load->view('admin',$data);
		$this->load->view('ui/footer');
	}

	

	// Editar
	public function edit($id=null)
	{

	}

	// Lista
	public function read_mods($id=null)
	{
		$this->load->view('ui/head');
		$data['list'] = $this->modulo->read(['where'=>['id_parent'=>0]]);
		$this->load->view('modulo/read',$data);
		$this->load->view('ui/footer');
	}

	public function create_mod($id=null)
	{
		permite($this->session->Permisos,strtolower($this->uri->segment(1)),2);
	    $get = $this->input->get();
	    $id=null;
	    $post=$this->input->post();
	    if(!is_null($post)){	       
	        $id=@$post['id_modulo-data'];
	        
	        if($id!=null){
	            $func= "update_modulo";
	        }else{
	            $func= "create_modulo";
	        }
	        $success=$this->modulo->$func($id);
	        verif_form($success,$success['data'],'admin/read_mods');
	    }
	    var_dump($post);
	}
	
	public function read_user($id=null)
	{
		$this->load->view('ui/head');
		$data['list'] = $this->usuarios->read(['where'=>['id_usuario_master'=>0]]);
		$page = $this->uri->segment(3);
		$offset = !$page?0:$page;
		$this->load->view($this->folder,$data);
		$this->load->view('ui/footer');
	}

	// Lista
	public function view($id=null)
	{
		
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		$this->almacen->delete($id);
		redirect('almacen/read');
	}
}

/* End of file almacen.php */
/* Location: ./application/controllers/almacen.php */
