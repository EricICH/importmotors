<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = [
  'form_validation',
  'database',
  'email',
  'session',
  'user_agent',
  'pagination',
  'cart',
  'pdf',
  'csvimport'
];

$autoload['drivers'] = array();

$autoload['helper'] = [
  'form',
  'url',
  'html',
  'date',
  'count',
  'text',
  'inflector',
  'fill',
  'format',
  'access',
  'esquema'
];

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = [
  'user_model'          => 'user',
  'auto_model'         => 'auto',
  'model_model'     => 'model',
  'extra_model'     => 'extra'
];
